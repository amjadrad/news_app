package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemNewsBinding;
import ir.tildaweb.news.databinding.ItemUserNewsBookmarkBinding;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.listeners.UserNewsBookmarkClickListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.user_news_favs.model.UserNewsFavoritesIndexResponse;
import ir.tildaweb.news.utils.DateUtils;
import ir.tildaweb.news.utils.TildaTextView;


public class AdapterUserNewsFavs extends RecyclerView.Adapter<AdapterUserNewsFavs.ViewHolder> {

    private String TAG = getClass().getName();
    private List<UserNewsFavoritesIndexResponse.UserNewsFavoritesData.UserNewsFavorite> list;
    private Context context;

    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;
    private UserNewsBookmarkClickListener onUserNewsBookmarkClickListener;


    public AdapterUserNewsFavs(Context context, RecyclerView recyclerView, List<UserNewsFavoritesIndexResponse.UserNewsFavoritesData.UserNewsFavorite> list, OnLoadMoreListener onLoadMoreListener, UserNewsBookmarkClickListener userNewsBookmarkClickListener) {
        this.list = list;
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        this.onUserNewsBookmarkClickListener = userNewsBookmarkClickListener;
        paginate(recyclerView);
    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserNewsBookmarkBinding itemBinding = ItemUserNewsBookmarkBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        UserNewsFavoritesIndexResponse.UserNewsFavoritesData.UserNewsFavorite news = list.get(position);
        holder.tvTitle.setText(String.format("%s", news.getNews().getTitle()));
        holder.tvDate.setText(String.format("%s", DateUtils.getDateTimeParsed(news.getNews().getCreatedAt())));
        holder.tvCategory.setText(String.format("#%s", news.getNews().getCategory().getTitle()));
        holder.linearNewsShow.setOnClickListener(view -> onUserNewsBookmarkClickListener.onShow(news.getNews().getId()));
        holder.imageViewFavorite.setOnClickListener(view -> onUserNewsBookmarkClickListener.onDeleteBookmark(news.getUserId(), news.getNews().getId()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TildaTextView tvTitle;
        TildaTextView tvDate;
        TildaTextView tvCategory;
        LinearLayout linearNewsShow;
        AppCompatImageView imageViewFavorite;

        public ViewHolder(@NonNull ItemUserNewsBookmarkBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvDate = binding.tvDate;
            tvCategory = binding.tvCategory;
            imageViewFavorite = binding.imageViewFavorite;
            linearNewsShow = binding.linearNewsShow;
        }
    }

    public void addItems(ArrayList<UserNewsFavoritesIndexResponse.UserNewsFavoritesData.UserNewsFavorite> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(UserNewsFavoritesIndexResponse.UserNewsFavoritesData.UserNewsFavorite item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void deleteItem(int id) {
        int i = 0;
        for (UserNewsFavoritesIndexResponse.UserNewsFavoritesData.UserNewsFavorite favorite : list) {
            if (favorite.getId() == id) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }


    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
