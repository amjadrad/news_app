package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemDropdownBinding;

public class AdapterDropDown extends RecyclerView.Adapter<AdapterDropDown.ViewHolder> {

    public static class DropdownItem {
        private String title;
        private boolean isSelected;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }

    private String TAG = getClass().getName();
    private List<DropdownItem> list;
    private Context context;
    private String tag;
    private OnDropDownSelectListener onDropDownSelectListener;

    public interface OnDropDownSelectListener {
        void onDropDownSelect(String tag, int position);
    }

    public AdapterDropDown(Context context, List<DropdownItem> list, String tag, OnDropDownSelectListener onDropDownSelectListener) {
        this.list = list;
        this.context = context;
        this.tag = tag;
        this.onDropDownSelectListener = onDropDownSelectListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDropdownBinding binding = ItemDropdownBinding.inflate(LayoutInflater.from(context), parent, false);
        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        DropdownItem dropdownItem = list.get(position);
        holder.tvTitle.setText(String.format("%s", dropdownItem.getTitle()));
        if (dropdownItem.isSelected()) {
            holder.viewCheckBox.setVisibility(View.VISIBLE);
        } else {
            holder.viewCheckBox.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelection(position);
                onDropDownSelectListener.onDropDownSelect(tag, position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        View viewCheckBox;

        public ViewHolder(@NonNull ItemDropdownBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            viewCheckBox = binding.viewCheckBox;
        }

    }


    public void setSelection(int position) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setSelected(i == position);
        }
        notifyDataSetChanged();
    }


    public void addItems(ArrayList<DropdownItem> items) {
        this.list.addAll(items);
        notifyDataSetChanged();
    }

    public void addItem(DropdownItem item) {
        this.list.add(item);
        notifyDataSetChanged();
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }

}
