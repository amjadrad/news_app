package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemAdminContactsBinding;
import ir.tildaweb.news.listeners.OnContactItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.contact_us_list.model.ContactUsListResponse;


public class AdapterAdminContacts extends RecyclerView.Adapter<AdapterAdminContacts.ViewHolder> {

    private String TAG = getClass().getName();
    private List<ContactUsListResponse.ContactsData.Contact> list;
    private Context context;
    private OnContactItemClickListener onContactItemClickListener;
    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;


    public AdapterAdminContacts(Context context, RecyclerView recyclerView, List<ContactUsListResponse.ContactsData.Contact> list, OnLoadMoreListener onLoadMoreListener, OnContactItemClickListener onContactItemClickListener) {
        this.list = list;
        this.onContactItemClickListener = onContactItemClickListener;
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        paginate(recyclerView);

    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminContactsBinding itemBinding = ItemAdminContactsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ContactUsListResponse.ContactsData.Contact contact = list.get(position);
        holder.tvTitle.setText(String.format("%s", contact.getTitle()));
        holder.tvDescription.setText(String.format("%s", contact.getDescription()));
        if (contact.getUser().getFullName() == null) {
            holder.tvUser.setText(String.format("%s", "ناشناس"));
        } else {
            holder.tvUser.setText(String.format("%s", contact.getUser().getFullName()));
        }
        if (contact.getSeen() == 0) {
            holder.tvNew.setVisibility(View.VISIBLE);
        } else {
            holder.tvNew.setVisibility(View.GONE);
        }
        holder.linearDelete.setOnClickListener(view -> onContactItemClickListener.onDelete(contact.getId(), contact.getTitle()));
        holder.linearShow.setOnClickListener(view -> onContactItemClickListener.onShow(contact.getSeen(), contact.getId(), contact.getTitle(), contact.getDescription()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDescription;
        TextView tvNew;
        TextView tvUser;
        LinearLayout linearDelete;
        LinearLayout linearShow;

        public ViewHolder(@NonNull ItemAdminContactsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvDescription = binding.tvDescription;
            tvNew = binding.tvNew;
            tvUser = binding.tvUser;
            linearDelete = binding.linearDelete;
            linearShow = binding.linearShow;
        }
    }

    public void addItems(ArrayList<ContactUsListResponse.ContactsData.Contact> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(ContactUsListResponse.ContactsData.Contact item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void deleteItem(int id) {
        int i = 0;
        for (ContactUsListResponse.ContactsData.Contact item : list) {
            if (item.getId() == id) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public void updateSeen(int id) {
        int i = 0;
        for (ContactUsListResponse.ContactsData.Contact item : list) {
            if (item.getId() == id) {
                item.setSeen(1);
                notifyItemChanged(i);
                break;
            }
            i++;
        }
    }


    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }
}
