package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.databinding.ItemAdminCategoryBinding;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;


public class AdapterAdminCategories extends RecyclerView.Adapter<AdapterAdminCategories.ViewHolder> {

    private String TAG = getClass().getName();
    private List<CategoriesResponse.Category> list;
    private Context context;
    private ItemClickListener itemClickListener;

    public AdapterAdminCategories(Context context, List<CategoriesResponse.Category> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminCategoryBinding itemBinding = ItemAdminCategoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CategoriesResponse.Category category = list.get(position);
        holder.tvTitle.setText(String.format("%s", category.getTitle()));
        Glide.with(context).load(BuildConfig.FILE_URL + category.getPicture()).into(holder.imageView);

        holder.linearDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onDelete(category.getId(), category.getTitle());
            }
        });
        holder.linearEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onEdit(category.getId());
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        AppCompatImageView imageView;
        LinearLayout linearDelete;
        LinearLayout linearEdit;

        public ViewHolder(@NonNull ItemAdminCategoryBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            imageView = binding.imageView;
            linearDelete = binding.linearDelete;
            linearEdit = binding.linearEdit;
        }

    }

    public void addItems(ArrayList<CategoriesResponse.Category> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(CategoriesResponse.Category item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }

    public void deleteItem(int id) {
        int i = 0;
        for (CategoriesResponse.Category category : list) {
            if (category.getId() == id) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

}
