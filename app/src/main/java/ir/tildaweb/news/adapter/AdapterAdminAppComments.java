package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemAdminAppCommentsBinding;
import ir.tildaweb.news.databinding.ItemAdminContactsBinding;
import ir.tildaweb.news.listeners.OnAppCommentItemClickListener;
import ir.tildaweb.news.listeners.OnContactItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsListResponse;
import ir.tildaweb.news.ui.contact_us_list.model.ContactUsListResponse;


public class AdapterAdminAppComments extends RecyclerView.Adapter<AdapterAdminAppComments.ViewHolder> {

    private String TAG = getClass().getName();
    private List<AppCommentsListResponse.AppCommentsData.AppComment> list;
    private Context context;
    private OnAppCommentItemClickListener onAppCommentItemClickListener;
    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;


    public AdapterAdminAppComments(Context context, RecyclerView recyclerView, List<AppCommentsListResponse.AppCommentsData.AppComment> list, OnLoadMoreListener onLoadMoreListener, OnAppCommentItemClickListener onAppCommentItemClickListener) {
        this.list = list;
        this.onAppCommentItemClickListener = onAppCommentItemClickListener;
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        paginate(recyclerView);

    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminAppCommentsBinding itemBinding = ItemAdminAppCommentsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AppCommentsListResponse.AppCommentsData.AppComment appComment = list.get(position);
        holder.tvComment.setText(String.format("%s", appComment.getComment()));
        holder.tvRating.setText(String.format("%s", Math.round(appComment.getRate())));
        if (appComment.getUser().getFullName() == null) {
            holder.tvUser.setText(String.format("%s", "ناشناس"));
        } else {
            holder.tvUser.setText(String.format("%s", appComment.getUser().getFullName()));
        }
        holder.linearDelete.setOnClickListener(view -> onAppCommentItemClickListener.onDelete(appComment.getId()));
        holder.linearShow.setOnClickListener(view -> onAppCommentItemClickListener.onShow(appComment.getComment()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvComment;
        TextView tvRating;
        TextView tvUser;
        LinearLayout linearDelete;
        LinearLayout linearShow;

        public ViewHolder(@NonNull ItemAdminAppCommentsBinding binding) {
            super(binding.getRoot());
            tvComment = binding.tvComment;
            tvRating = binding.tvRating;
            tvUser = binding.tvUser;
            linearDelete = binding.linearDelete;
            linearShow = binding.linearShow;
        }
    }

    public void addItems(ArrayList<AppCommentsListResponse.AppCommentsData.AppComment> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(AppCommentsListResponse.AppCommentsData.AppComment item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void deleteItem(int id) {
        int i = 0;
        for (AppCommentsListResponse.AppCommentsData.AppComment item : list) {
            if (item.getId() == id) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }
}
