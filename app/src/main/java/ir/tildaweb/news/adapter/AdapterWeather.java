
package ir.tildaweb.news.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import ir.tildaweb.news.databinding.ItemWeatherBinding;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.WeatherResponse;
import ir.tildaweb.news.utils.DateUtils;


public class AdapterWeather extends RecyclerView.Adapter<AdapterWeather.ViewHolder> {

    private String TAG = getClass().getName();
    private List<WeatherResponse.Weather> list;
    private Context context;

    public AdapterWeather(Context context, List<WeatherResponse.Weather> list) {
        this.list = list;
        this.context = context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemWeatherBinding itemBinding = ItemWeatherBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        WeatherResponse.Weather item = list.get(position);
        holder.tvMinTemp.setText(String.format(" \u2103 %d", Math.round(item.getMainInfo().getTempMin())));
        holder.tvMaxTemp.setText(String.format(" \u2103 %d", Math.round(item.getMainInfo().getTempMax())));
        holder.tvHumidity.setText(String.format("%s%d", "%", item.getMainInfo().getHumidity()));
        String date = new DateUtils().showJalali(item.getDate());
        holder.tvTime.setText(String.format("%s", date.substring(10)));

        PersianCalendar calendar = new PersianCalendar();
        calendar.setPersianDate(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10)));
        holder.tvDate.setText(String.format("%s", calendar.getPersianLongDate()));

//        holder.tvWeatherStatus.setText(item.getWeatherInfo().get(0).getDescription());
        Glide.with(context).load("https://openweathermap.org/img/wn/" + item.getWeatherInfo().get(0).getIcon() + "@2x.png").into(holder.imageViewWeatherStatus);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvMaxTemp;
        TextView tvMinTemp;
        TextView tvWeatherStatus;
        TextView tvHumidity;
        TextView tvDate;
        TextView tvTime;
        AppCompatImageView imageViewWeatherStatus;

        public ViewHolder(@NonNull ItemWeatherBinding binding) {
            super(binding.getRoot());
            tvMaxTemp = binding.tvMaxTemp;
            tvMinTemp = binding.tvMinTemp;
//            tvWeatherStatus = binding.tvWeatherStatus;
            tvHumidity = binding.tvHumidity;
            tvDate = binding.tvDate;
            tvTime = binding.tvTime;
            imageViewWeatherStatus = binding.imageViewWeatherStatus;
        }

    }

    public void addItems(ArrayList<WeatherResponse.Weather> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(WeatherResponse.Weather item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }
}
