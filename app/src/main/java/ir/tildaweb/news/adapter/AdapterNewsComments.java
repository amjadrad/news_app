package ir.tildaweb.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asura.library.posters.Poster;
import com.asura.library.posters.RemoteImage;
import com.asura.library.posters.RemoteVideo;
import com.asura.library.views.PosterSlider;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ItemNewsBinding;
import ir.tildaweb.news.databinding.ItemNewsCommentBinding;
import ir.tildaweb.news.listeners.NewsClickListener;
import ir.tildaweb.news.listeners.NewsCommentsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;
import ir.tildaweb.news.utils.DateUtils;
import ir.tildaweb.news.utils.IntentHelper;
import ir.tildaweb.news.utils.TextUtils;


public class AdapterNewsComments extends RecyclerView.Adapter<AdapterNewsComments.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsCommentsResponse.CommentData.NewsComment> list;
    private Context context;

    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;
    private NewsCommentsClickListener newsCommentsClickListener;

    public AdapterNewsComments(Context context, RecyclerView recyclerView, List<NewsCommentsResponse.CommentData.NewsComment> list, OnLoadMoreListener onLoadMoreListener, NewsCommentsClickListener newsCommentsClickListener) {
        this.list = list;
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        this.newsCommentsClickListener = newsCommentsClickListener;
        paginate(recyclerView);
    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNewsCommentBinding itemBinding = ItemNewsCommentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NewsCommentsResponse.CommentData.NewsComment newsComment = list.get(position);
        String name = newsComment.getUser().getFullName();
        if (name == null) {
            name = "ناشناس";
        }
        holder.tvTime.setText(String.format("%s", newsComment.getCreatedAt()));
        holder.tvUserFullname.setText(String.format("%s", name));
        holder.tvComment.setText(String.format("%s", newsComment.getComment()));
        holder.tvCreateComment.setOnClickListener(view -> newsCommentsClickListener.onReplyComment(newsComment.getNewsId(), newsComment.getId()));
        holder.recyclerView.setLayoutManager(new

                LinearLayoutManager(context));
        AdapterNewsCommentsReplied adapterNewsComments = new AdapterNewsCommentsReplied(context, newsComment.getComments());
        holder.recyclerView.setAdapter(adapterNewsComments);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserFullname;
        TextView tvComment;
        TextView tvCreateComment;
        TextView tvTime;
        RecyclerView recyclerView;

        public ViewHolder(@NonNull ItemNewsCommentBinding binding) {
            super(binding.getRoot());
            tvComment = binding.tvComment;
            tvCreateComment = binding.tvCreateComment;
            tvUserFullname = binding.tvFullname;
            tvTime = binding.tvTime;
            recyclerView = binding.recyclerView;
        }

    }

    public void addItems(ArrayList<NewsCommentsResponse.CommentData.NewsComment> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsCommentsResponse.CommentData.NewsComment item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
