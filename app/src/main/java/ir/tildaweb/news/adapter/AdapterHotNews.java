package ir.tildaweb.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asura.library.posters.Poster;
import com.asura.library.posters.RemoteImage;
import com.asura.library.posters.RemoteVideo;
import com.asura.library.views.PosterSlider;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ItemBeforeNewsBinding;
import ir.tildaweb.news.databinding.ItemHotNewsBinding;
import ir.tildaweb.news.databinding.ItemNewsBinding;
import ir.tildaweb.news.listeners.NewsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.news_by_category.view.NewsByCategoryActivity;
import ir.tildaweb.news.utils.IntentHelper;
import ir.tildaweb.news.utils.TextUtils;


public class AdapterHotNews extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsResponse.NewsData.News> list;
    private Context context;
    private NewsClickListener onNewsClickListener;
    private PersianCalendar calendar;

    public AdapterHotNews(Context context, List<NewsResponse.NewsData.News> list, NewsClickListener onNewsClickListener) {
        this.list = list;
        this.calendar = new PersianCalendar();
        this.context = context;
        this.onNewsClickListener = onNewsClickListener;
    }


    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        NewsResponse.NewsData.News news = list.get(holder.getAdapterPosition());
        if (news != null && news.getUserVisit() != null && !news.getUserVisit()) {
            onNewsClickListener.onVisitNews(news.getId());
            news.setUserVisit(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? 0 : 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            ItemBeforeNewsBinding itemBinding = ItemBeforeNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderBeforeItem(itemBinding);
        } else {
            ItemHotNewsBinding itemBinding = ItemHotNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderNews(itemBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ViewHolderNews holderItem = (ViewHolderNews) holder;
        NewsResponse.NewsData.News news = list.get(position);
        holderItem.tvTitle.setText(String.format("%s", news.getTitle()));
        holderItem.tvDescription.setText(String.format("%s", news.getDescription()));
        holderItem.tvCategory.setText(String.format("#%s", news.getCategory().getTitle()));
        calendar.setPersianDate(Integer.parseInt(news.getCreatedAt().substring(0, 4)), Integer.parseInt(news.getCreatedAt().substring(5, 7)), Integer.parseInt(news.getCreatedAt().substring(8, 10)));
        holderItem.tvDate.setText(String.format("%s", calendar.getPersianLongDate()));
        if (news.getNewsMedia() != null && news.getNewsMedia().size() > 0) {
            holderItem.cardViewImage.setVisibility(View.VISIBLE);
            Glide.with(context).load(BuildConfig.FILE_URL + news.getNewsMedia().get(0).getPath()).into(holderItem.imageView);
        } else {
            holderItem.cardViewImage.setVisibility(View.GONE);
        }
        holderItem.tvCategory.setOnClickListener(view -> {
            Intent intent = new Intent(context, NewsByCategoryActivity.class);
            intent.putExtra("category_id", news.getCategory().getId());
            intent.putExtra("category_title", news.getCategory().getTitle());
            context.startActivity(intent);
        });
        holder.itemView.setOnClickListener(view -> onNewsClickListener.onShowNews(news.getId()));

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderBeforeItem extends RecyclerView.ViewHolder {

        TextView tvTitle;
        AppCompatImageView imageView;

        public ViewHolderBeforeItem(@NonNull ItemBeforeNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            imageView = binding.imageView;
        }

    }

    public class ViewHolderNews extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDescription;
        TextView tvCategory;
        TextView tvDate;
        AppCompatImageView imageView;
        CardView cardViewImage;

        public ViewHolderNews(@NonNull ItemHotNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvDescription = binding.tvDescription;
            tvCategory = binding.tvCategory;
            tvDate = binding.tvDate;
            imageView = binding.imageView;
            cardViewImage = binding.cardViewImage;
        }

    }

    public void addItems(ArrayList<NewsResponse.NewsData.News> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsResponse.NewsData.News item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
