package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemAdminNewsBinding;
import ir.tildaweb.news.databinding.ItemAdminUserNewsBinding;
import ir.tildaweb.news.listeners.OnAdminNewsItemClickListener;
import ir.tildaweb.news.listeners.OnAdminUserNewsItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.utils.DateUtils;
import ir.tildaweb.news.utils.TildaTextView;


public class AdapterAdminUserNews extends RecyclerView.Adapter<AdapterAdminUserNews.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsResponse.NewsData.News> list;
    private Context context;
    private OnAdminUserNewsItemClickListener onAdminUserNewsItemClickListener;
    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;


    public AdapterAdminUserNews(Context context, RecyclerView recyclerView, List<NewsResponse.NewsData.News> list, OnLoadMoreListener onLoadMoreListener, OnAdminUserNewsItemClickListener onAdminUserNewsItemClickListener) {
        this.list = list;
        this.onAdminUserNewsItemClickListener = onAdminUserNewsItemClickListener;
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        paginate(recyclerView);

    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminUserNewsBinding itemBinding = ItemAdminUserNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        NewsResponse.NewsData.News news = list.get(position);
        holder.tvTitle.setText(String.format("%s", news.getTitle()));
        holder.tvDate.setText(String.format("%s", DateUtils.getDateTimeParsed(news.getCreatedAt())));
        holder.tvCategory.setText(String.format("#%s", news.getCategory().getTitle()));
        holder.linearDelete.setOnClickListener(view -> onAdminUserNewsItemClickListener.onDelete(news.getId(), news.getTitle()));
        holder.linearEdit.setOnClickListener(view -> onAdminUserNewsItemClickListener.onEdit(news.getId()));
        holder.linearReject.setOnClickListener(view -> onAdminUserNewsItemClickListener.onReject(news.getId()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TildaTextView tvTitle;
        TildaTextView tvDate;
        TildaTextView tvCategory;
        LinearLayout linearDelete;
        LinearLayout linearEdit;
        LinearLayout linearReject;

        public ViewHolder(@NonNull ItemAdminUserNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvCategory = binding.tvCategory;
            tvDate = binding.tvDate;
            linearDelete = binding.linearDelete;
            linearEdit = binding.linearEdit;
            linearReject = binding.linearReject;
        }
    }

    public void addItems(ArrayList<NewsResponse.NewsData.News> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsResponse.NewsData.News item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void deleteItem(int newsId) {
        int i = 0;
        for (NewsResponse.NewsData.News news : list) {
            if (news.getId() == newsId) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public void rejectItem(int newsId) {
        int i = 0;
        for (NewsResponse.NewsData.News news : list) {
            if (news.getId() == newsId) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }
}
