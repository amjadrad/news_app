package ir.tildaweb.news.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ItemAdminAdminBinding;
import ir.tildaweb.news.databinding.ItemAdminCategoryBinding;
import ir.tildaweb.news.listeners.AdminItemClickListener;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;


public class AdapterAdminAdmins extends RecyclerView.Adapter<AdapterAdminAdmins.ViewHolder> {

    private String TAG = getClass().getName();
    private List<AdminsResponse.Admin> list;
    private Context context;
    private AdminItemClickListener itemClickListener;

    public AdapterAdminAdmins(Context context, List<AdminsResponse.Admin> list, AdminItemClickListener itemClickListener) {
        this.list = list;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminAdminBinding itemBinding = ItemAdminAdminBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AdminsResponse.Admin admin = list.get(position);
        holder.tvName.setText(String.format("%s", admin.getName()));
        holder.tvPhone.setText(String.format("%s", admin.getPhone()));
        holder.linearAccess.setVisibility(View.VISIBLE);
        if (admin.getType().equals("owner")) {
            holder.linearAccess.setVisibility(View.GONE);
            holder.tvType.setText(String.format("%s", "مدیر اصلی"));
            holder.tvType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.tvType.setBackground(context.getResources().getDrawable(R.drawable.bg_primary_rounded_soft));
        } else {
            holder.tvType.setText(String.format("%s", "مدیر عادی"));
            holder.tvType.setTextColor(context.getResources().getColor(R.color.colorDark2));
            holder.tvType.setBackground(context.getResources().getDrawable(R.drawable.bg_info_rounded_soft));
        }

        holder.linearDelete.setOnClickListener(view -> itemClickListener.onDelete(admin.getId(), admin.getName()));
        holder.linearEdit.setOnClickListener(view -> itemClickListener.onEdit(admin.getId()));
        holder.linearAccess.setOnClickListener(view -> itemClickListener.onAccess(admin.getId(), admin.getName()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvPhone;
        TextView tvType;
        LinearLayout linearDelete;
        LinearLayout linearEdit;
        LinearLayout linearAccess;


        public ViewHolder(@NonNull ItemAdminAdminBinding binding) {
            super(binding.getRoot());
            tvName = binding.tvName;
            tvPhone = binding.tvPhone;
            linearEdit = binding.linearEdit;
            linearDelete = binding.linearDelete;
            linearAccess = binding.linearAccess;
            tvType = binding.tvType;
        }

    }

    public void addItems(ArrayList<AdminsResponse.Admin> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }

    public void deleteItem(int id) {
        int i = 0;
        for (AdminsResponse.Admin category : list) {
            if (category.getId() == id) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

}
