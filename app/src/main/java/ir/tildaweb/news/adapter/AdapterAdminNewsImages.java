package ir.tildaweb.news.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ItemAdminAdminBinding;
import ir.tildaweb.news.databinding.ItemAdminNewsImagesBinding;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminsResponse;
import ir.tildaweb.tilda_filepicker.models.FileModel;


public class AdapterAdminNewsImages extends RecyclerView.Adapter<AdapterAdminNewsImages.ViewHolder> {

    private String TAG = getClass().getName();
    private List<FileModel> list;
    private Context context;
    private ItemClickListener itemClickListener;

    public AdapterAdminNewsImages(Context context, List<FileModel> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.itemClickListener = itemClickListener;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminNewsImagesBinding itemBinding = ItemAdminNewsImagesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        FileModel model = list.get(position);
        if (model.getId() != null && model.getId() > 0) {
            Glide.with(context).load(BuildConfig.FILE_URL + model.getPath()).into(holder.imageView);
        } else {
            Glide.with(context).load(new File(model.getPath())).into(holder.imageView);
        }
        holder.linearLayoutDelete.setOnClickListener(view -> {
            if (model.getId() != null && model.getId() > 0) {
                itemClickListener.onDelete(model.getId(), "تصویر");
            } else {
                delete(model);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView imageView;
        LinearLayout linearLayoutDelete;


        public ViewHolder(@NonNull ItemAdminNewsImagesBinding binding) {
            super(binding.getRoot());
            imageView = binding.imageView;
            linearLayoutDelete = binding.linearDelete;
        }

    }

    public void addItem(FileModel item) {
        this.list.add(0, item);
        notifyItemInserted(0);

    }

    public void addItems(ArrayList<FileModel> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }

    public void delete(FileModel model) {
        int i = 0;
        for (FileModel item : list) {
            if (item == model) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public void delete(int mediaId) {
        int i = 0;
        for (FileModel item : list) {
            if (item.getId() == mediaId) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public List<FileModel> getList() {
        return this.list;
    }
}
