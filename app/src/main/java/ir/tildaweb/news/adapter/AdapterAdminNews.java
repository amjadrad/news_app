package ir.tildaweb.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemAdminNewsBinding;
import ir.tildaweb.news.listeners.OnAdminNewsItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.utils.DateUtils;
import ir.tildaweb.news.utils.TildaTextView;


public class AdapterAdminNews extends RecyclerView.Adapter<AdapterAdminNews.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsResponse.NewsData.News> list;
    private Context context;
    private OnAdminNewsItemClickListener onAdminNewsItemClickListener;
    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;


    public AdapterAdminNews(Context context, RecyclerView recyclerView, List<NewsResponse.NewsData.News> list, OnLoadMoreListener onLoadMoreListener, OnAdminNewsItemClickListener onAdminNewsItemClickListener) {
        this.list = list;
        this.onAdminNewsItemClickListener = onAdminNewsItemClickListener;
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        paginate(recyclerView);

    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminNewsBinding itemBinding = ItemAdminNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        NewsResponse.NewsData.News news = list.get(position);
        holder.tvTitle.setText(String.format("%s", news.getTitle()));
        holder.tvDate.setText(String.format("%s", DateUtils.getDateTimeParsed(news.getCreatedAt())));
        holder.tvCategory.setText(String.format("#%s", news.getCategory().getTitle()));
        int countUnSeen=0;
        if (news.getCommentsCountUnSeen() != null) {
            countUnSeen = news.getCommentsCountUnSeen();
        }
        if (countUnSeen == 0) {
            holder.tvCommentsCountUnSeen.setVisibility(View.GONE);
        } else if (countUnSeen > 10) {
            holder.tvCommentsCountUnSeen.setVisibility(View.VISIBLE);
            holder.tvCommentsCountUnSeen.setText(String.format("%s", "10+"));
        } else {
            holder.tvCommentsCountUnSeen.setVisibility(View.VISIBLE);
            holder.tvCommentsCountUnSeen.setText(String.format("%d", countUnSeen));
        }
        holder.linearDelete.setOnClickListener(view -> onAdminNewsItemClickListener.onDelete(news.getId(), news.getTitle()));
        holder.linearEdit.setOnClickListener(view -> onAdminNewsItemClickListener.onEdit(news.getId()));
        holder.linearComments.setOnClickListener(view -> onAdminNewsItemClickListener.onComments(news.getId()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TildaTextView tvTitle;
        TildaTextView tvDate;
        TildaTextView tvCommentsCountUnSeen;
        TildaTextView tvCategory;
        LinearLayout linearDelete;
        LinearLayout linearEdit;
        LinearLayout linearComments;

        public ViewHolder(@NonNull ItemAdminNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvCategory = binding.tvCategory;
            tvCommentsCountUnSeen = binding.tvCommentsCountUnSeen;
            tvDate = binding.tvDate;
            linearDelete = binding.linearDelete;
            linearEdit = binding.linearEdit;
            linearComments = binding.linearComments;
        }
    }

    public void addItems(ArrayList<NewsResponse.NewsData.News> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsResponse.NewsData.News item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void deleteItem(int newsId) {
        int i = 0;
        for (NewsResponse.NewsData.News news : list) {
            if (news.getId() == newsId) {
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
            i++;
        }
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }
}
