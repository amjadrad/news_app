package ir.tildaweb.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.databinding.ItemCategoryBinding;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.news_by_category.view.NewsByCategoryActivity;
import ir.tildaweb.news.utils.TildaTextView;


public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.ViewHolder> {

    private String TAG = getClass().getName();
    private List<CategoriesResponse.Category> list;
    private Context context;

    public AdapterCategories(Context context, List<CategoriesResponse.Category> list) {
        this.list = list;
        this.context = context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCategoryBinding itemBinding = ItemCategoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CategoriesResponse.Category category = list.get(position);
        holder.tvTitle.setText(String.format("%s", category.getTitle()));
        if (category.getNewsCountUnSeen() == 0) {
            holder.tvNewsCountUnSeen.setVisibility(View.GONE);
        } else {
            holder.tvNewsCountUnSeen.setVisibility(View.VISIBLE);
            holder.tvNewsCountUnSeen.setText(String.format("%s", category.getNewsCountUnSeen()));
        }
        Glide.with(context).load(BuildConfig.FILE_URL + category.getPicture()).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewsByCategoryActivity.class);
                intent.putExtra("category_id", category.getId());
                intent.putExtra("category_title", category.getTitle());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TildaTextView tvTitle;
        TildaTextView tvNewsCountUnSeen;
        AppCompatImageView imageView;

        public ViewHolder(@NonNull ItemCategoryBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvNewsCountUnSeen = binding.tvNewsCountUnSeen;
            imageView = binding.imageView;
        }

    }

    public void addItems(ArrayList<CategoriesResponse.Category> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(CategoriesResponse.Category item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }
}
