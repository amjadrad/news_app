package ir.tildaweb.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ir.tildaweb.news.databinding.ItemNewsCommentRepliedBinding;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;
import ir.tildaweb.news.utils.DateUtils;


public class AdapterNewsCommentsReplied extends RecyclerView.Adapter<AdapterNewsCommentsReplied.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsCommentsResponse.CommentData.NewsComment> list;
    private Context context;

    public AdapterNewsCommentsReplied(Context context, List<NewsCommentsResponse.CommentData.NewsComment> list) {
        this.list = list;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNewsCommentRepliedBinding itemBinding = ItemNewsCommentRepliedBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NewsCommentsResponse.CommentData.NewsComment newsComment = list.get(position);
        String name = newsComment.getUser().getFullName();
        if (name == null) {
            name = "ناشناس";
        }
        holder.tvTime.setText(String.format("%s", newsComment.getCreatedAt()));
        holder.tvUserFullname.setText(String.format("%s", name));
        holder.tvComment.setText(String.format("%s", newsComment.getComment()));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserFullname;
        TextView tvComment;
        TextView tvTime;

        public ViewHolder(@NonNull ItemNewsCommentRepliedBinding binding) {
            super(binding.getRoot());
            tvComment = binding.tvComment;
            tvUserFullname = binding.tvFullname;
            tvTime = binding.tvTime;
        }
    }

    public void addItems(ArrayList<NewsCommentsResponse.CommentData.NewsComment> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsCommentsResponse.CommentData.NewsComment item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
