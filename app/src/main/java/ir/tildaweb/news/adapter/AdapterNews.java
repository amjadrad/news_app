package ir.tildaweb.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asura.library.posters.Poster;
import com.asura.library.posters.RemoteImage;
import com.asura.library.posters.RemoteVideo;
import com.asura.library.views.PosterSlider;

import java.util.ArrayList;
import java.util.List;

import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ItemNewsBinding;
import ir.tildaweb.news.listeners.NewsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.news_by_category.view.NewsByCategoryActivity;
import ir.tildaweb.news.utils.IntentHelper;


public class AdapterNews extends RecyclerView.Adapter<AdapterNews.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsResponse.NewsData.News> list;
    private Context context;

    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;
    private NewsClickListener onNewsClickListener;
    private PersianCalendar calendar;


    public AdapterNews(Context context, RecyclerView recyclerView, List<NewsResponse.NewsData.News> list, OnLoadMoreListener onLoadMoreListener, NewsClickListener newsClickListener) {
        this.list = list;
        this.calendar = new PersianCalendar();
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        this.onNewsClickListener = newsClickListener;
        paginate(recyclerView);
    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        NewsResponse.NewsData.News news = list.get(holder.getAdapterPosition());
        if (news != null && news.getUserVisit() != null && !news.getUserVisit()) {
            onNewsClickListener.onVisitNews(news.getId());
            news.setUserVisit(true);
        }
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNewsBinding itemBinding = ItemNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        NewsResponse.NewsData.News news = list.get(position);
        ColorStateList cslMutedText = AppCompatResources.getColorStateList(context, R.color.colorMutedText);
        ColorStateList cslPrimary = AppCompatResources.getColorStateList(context, R.color.colorPrimary);
        ColorStateList cslDanger = AppCompatResources.getColorStateList(context, R.color.colorDanger);


        holder.tvTitle.setText(String.format("%s", news.getTitle()));
        holder.tvDescription.setText(String.format("%s", news.getDescription()));
        holder.tvDisLikesCount.setText(String.format("%s", news.getDislikesCount()));
        holder.tvLikesCount.setText(String.format("%s", news.getLikesCount()));
        holder.tvCategory.setText(String.format("#%s", news.getCategory().getTitle()));
        holder.tvViews.setText(String.format("%s بازدید", news.getVisitsCount()));
        if (news.getCommentsCount() == 0) {
            holder.tvViewComments.setVisibility(View.GONE);
        } else {
            holder.tvViewComments.setVisibility(View.VISIBLE);
            holder.tvViewComments.setText(String.format("مشاهده %s نظر", news.getCommentsCount()));
        }
        calendar.setPersianDate(Integer.parseInt(news.getCreatedAt().substring(0, 4)), Integer.parseInt(news.getCreatedAt().substring(5, 7)), Integer.parseInt(news.getCreatedAt().substring(8, 10)));
        holder.tvDate.setText(String.format("%s   %s", calendar.getPersianLongDate(), news.getCreatedAt().substring(11)));

        holder.itemView.setOnClickListener(view -> onNewsClickListener.onShowNews(news.getId()));
        if (news.getUserLike() == null) {
            ImageViewCompat.setImageTintList(holder.imageViewLike, cslMutedText);
            ImageViewCompat.setImageTintList(holder.imageViewDislike, cslMutedText);
        } else {
            if (news.getUserLike() == 1) {
                ImageViewCompat.setImageTintList(holder.imageViewLike, cslPrimary);
                ImageViewCompat.setImageTintList(holder.imageViewDislike, cslMutedText);
            } else {
                ImageViewCompat.setImageTintList(holder.imageViewLike, cslMutedText);
                ImageViewCompat.setImageTintList(holder.imageViewDislike, cslDanger);
            }
        }
        if (news.getFavorite() == null) {
            ImageViewCompat.setImageTintList(holder.imageViewFavorite, cslMutedText);
        } else {
            ImageViewCompat.setImageTintList(holder.imageViewFavorite, cslPrimary);
        }
        holder.posterSlider.setHideIndicators(false);
        holder.posterSlider.setInterval(600000);
        holder.posterSlider.setLoopSlides(false);

        List<Poster> posters = new ArrayList<>();
        List<NewsResponse.NewsData.News.NewsMedia> newsMedia = news.getNewsMedia();

        if (newsMedia == null || newsMedia.size() == 0) {
            holder.posterSlider.setVisibility(View.GONE);
        } else {
            holder.posterSlider.setVisibility(View.VISIBLE);
            for (NewsResponse.NewsData.News.NewsMedia media : newsMedia) {
                if (media.getType().name().equals(NewsResponse.NewsType.picture.name())) {
                    posters.add(new RemoteImage(BuildConfig.FILE_URL + media.getPath()));
                } else {
                    posters.add(new RemoteVideo(Uri.parse(BuildConfig.FILE_URL + media.getPath())));
                }
            }
            holder.posterSlider.setPosters(posters);
        }
        holder.imageViewLike.setOnClickListener(view -> {
//            if (news.getUserLike() == null || news.getUserLike() == 1) {
            onNewsClickListener.onLike(news.getId());
//            }
        });
        holder.imageViewDislike.setOnClickListener(view -> {
//            if (news.getUserLike() == null || news.getUserLike() == 0) {
            onNewsClickListener.onDisLike(news.getId());
//            }
        });
        holder.imageViewShare.setOnClickListener(view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    String.format("%s \n %s", news.getTitle(), news.getDescription()));
            sendIntent.setType("text/plain");
            context.startActivity(sendIntent);
        });
        holder.tvLink.setOnClickListener(view -> {
            if (news.getLink() != null) {
                IntentHelper intentHelper = new IntentHelper(context);
                intentHelper.openSite(news.getLink());
                holder.tvLink.setVisibility(View.VISIBLE);
            } else {
                holder.tvLink.setVisibility(View.GONE);
            }
        });
        if (news.getLink() != null) {
            holder.tvLink.setVisibility(View.VISIBLE);
        } else {
            holder.tvLink.setVisibility(View.GONE);
        }

        holder.tvCategory.setOnClickListener(view -> {
            Intent intent = new Intent(context, NewsByCategoryActivity.class);
            intent.putExtra("category_id", news.getCategory().getId());
            intent.putExtra("category_title", news.getCategory().getTitle());
            context.startActivity(intent);
        });

        holder.tvCreateComment.setOnClickListener(view -> onNewsClickListener.onCreateComment(news.getId()));
        holder.tvViewComments.setOnClickListener(view -> onNewsClickListener.onCommentsList(news.getId()));
        holder.imageViewFavorite.setOnClickListener(view -> onNewsClickListener.onFavorite(news.getId()));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDescription;
        TextView tvLikesCount;
        TextView tvDisLikesCount;
        TextView tvLink;
        TextView tvViews;
        TextView tvCreateComment;
        TextView tvViewComments;
        TextView tvCategory;
        TextView tvDate;
        AppCompatImageView imageViewLike, imageViewDislike, imageViewShare, imageViewFavorite;
        PosterSlider posterSlider;


        public ViewHolder(@NonNull ItemNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvDescription = binding.tvDescription;
            tvLikesCount = binding.tvLikesCount;
            tvDisLikesCount = binding.tvDisLikesCount;
            tvLink = binding.tvLink;
            tvViews = binding.tvViews;
            tvDate = binding.tvDate;
            tvCreateComment = binding.tvCreateComment;
            tvViewComments = binding.tvViewComments;
            tvCategory = binding.tvCategory;
            posterSlider = binding.slider;
            imageViewLike = binding.imageViewLike;
            imageViewDislike = binding.imageViewDislike;
            imageViewShare = binding.imageViewShare;
            imageViewFavorite = binding.imageViewFavorite;
        }

    }

    public void addItems(ArrayList<NewsResponse.NewsData.News> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsResponse.NewsData.News item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void updateLike(NewsLikeResponse newsLikeResponse) {
        int i = 0;
        for (NewsResponse.NewsData.News news : list) {
            if (news.getId().intValue() == newsLikeResponse.getNewsId().intValue()) {
                if (newsLikeResponse.getStatus() == 201) {
                    news.setLikesCount(newsLikeResponse.getLikesCount());
                    news.setDislikesCount(newsLikeResponse.getDislikesCount());
                    news.setUserLike(newsLikeResponse.getUserLike());
                }
                notifyItemChanged(i);
                break;
            }
            i++;
        }
    }

    public void updateFavorite(UserNewsFavoriteResponse userNewsFavoriteResponse) {
        int i = 0;
        for (NewsResponse.NewsData.News news : list) {
            if (news.getId().intValue() == userNewsFavoriteResponse.getNewsId().intValue()) {
                if (userNewsFavoriteResponse.getDeleted()) {
                    news.setFavorite(null);
                } else {
                    news.setFavorite(userNewsFavoriteResponse.getFavorite());
                }
                notifyItemChanged(i);
                break;
            }
            i++;
        }
    }


    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
