package ir.tildaweb.news.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zcw.togglebutton.ToggleButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ir.tildaweb.news.databinding.ItemAdminNewsCommentBinding;
import ir.tildaweb.news.listeners.OnAdminNewsCommentItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;
import ir.tildaweb.news.utils.DateUtils;
import ir.tildaweb.news.utils.TildaTextView;


public class AdapterAdminNewsComments extends RecyclerView.Adapter<AdapterAdminNewsComments.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsCommentsResponse.CommentData.NewsComment> list;
    private Context context;

    //Paginate
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isSearch = false;
    private OnAdminNewsCommentItemClickListener newsCommentsClickListener;
    private DateUtils dateUtils;

    public AdapterAdminNewsComments(Context context, RecyclerView recyclerView, List<NewsCommentsResponse.CommentData.NewsComment> list, OnLoadMoreListener onLoadMoreListener, OnAdminNewsCommentItemClickListener onAdminNewsCommentItemClickListener) {
        this.list = list;
        this.dateUtils = new DateUtils();
        this.context = context;
        this.onLoadMoreListener = onLoadMoreListener;
        this.newsCommentsClickListener = onAdminNewsCommentItemClickListener;
        paginate(recyclerView);
    }

    private void paginate(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager && !isSearch) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull AdapterAdminNewsComments.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        NewsCommentsResponse.CommentData.NewsComment comment = list.get(holder.getAdapterPosition());
        if (comment.getSeen() == 0) {
            newsCommentsClickListener.onCommentSeen(comment.getId());
            comment.setSeen(1);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAdminNewsCommentBinding itemBinding = ItemAdminNewsCommentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NewsCommentsResponse.CommentData.NewsComment newsComment = list.get(position);
        String name = newsComment.getUser().getFullName();
        if (name == null) {
            name = "ناشناس";
        }
        holder.tvTime.setText(String.format("%s",(newsComment.getCreatedAt())));
        holder.tvUserFullname.setText(String.format("%s", name));
        holder.tvComment.setText(String.format("%s", newsComment.getComment()));
        if (newsComment.getActive() == 0) {
            holder.toggle.setToggleOff();
            holder.tvActive.setText(String.format("%s", "عدم نمایش"));
        } else {
            holder.toggle.setToggleOn();
            holder.tvActive.setText(String.format("%s", "نمایش"));
        }

        holder.toggle.setOnToggleChanged(on -> newsCommentsClickListener.onActiveChange(newsComment.getId(), on));

        if (newsComment.getSeen() == 1) {
            holder.tvStatus.setVisibility(View.GONE);
        } else {
            holder.tvStatus.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TildaTextView tvUserFullname;
        TildaTextView tvComment;
        TildaTextView tvActive;
        TildaTextView tvStatus;
        TildaTextView tvTime;
        ToggleButton toggle;

        public ViewHolder(@NonNull ItemAdminNewsCommentBinding binding) {
            super(binding.getRoot());
            tvComment = binding.tvComment;
            tvUserFullname = binding.tvFullname;
            tvTime = binding.tvTime;
            tvActive = binding.tvActive;
            toggle = binding.toggle;
            tvStatus = binding.tvStatus;
        }
    }

    public void addItems(ArrayList<NewsCommentsResponse.CommentData.NewsComment> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsCommentsResponse.CommentData.NewsComment item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void changeActive(int id, int active) {
        int i = 0;
        for (NewsCommentsResponse.CommentData.NewsComment item : list) {
            if (item.getId() == id) {
                item.setActive(active);
                notifyItemChanged(i);
            }
            i++;
        }
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
