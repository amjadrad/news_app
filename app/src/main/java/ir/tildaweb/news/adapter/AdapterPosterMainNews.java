package ir.tildaweb.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.tildaweb.news.databinding.ItemBeforeNewsBinding;
import ir.tildaweb.news.databinding.ItemHotNewsBinding;
import ir.tildaweb.news.listeners.NewsClickListener;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsPosterResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.news_by_category.view.NewsByCategoryActivity;


public class AdapterPosterMainNews extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = getClass().getName();
    private List<NewsPosterResponse.PosterNews> list;
    private Context context;
    private NewsClickListener onNewsClickListener;

    public AdapterPosterMainNews(Context context, List<NewsPosterResponse.PosterNews> list, NewsClickListener onNewsClickListener) {
        this.list = list;
        this.context = context;
        this.onNewsClickListener = onNewsClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            ItemBeforeNewsBinding itemBinding = ItemBeforeNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderBeforeItem(itemBinding);
        } else {
            ItemHotNewsBinding itemBinding = ItemHotNewsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderNews(itemBinding);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (position != 0 && holder instanceof ViewHolderNews) {
            ViewHolderNews holderItem = (ViewHolderNews) holder;
            NewsResponse.NewsData.News news = list.get(position).getNews();
            holderItem.tvTitle.setText(String.format("%s", news.getTitle()));
            holderItem.tvDescription.setText(String.format("%s", news.getDescription()));
            holderItem.tvCategory.setText(String.format("#%s", news.getCategory().getTitle()));
            holderItem.tvCategory.setOnClickListener(view -> {
                Intent intent = new Intent(context, NewsByCategoryActivity.class);
                intent.putExtra("category_id", news.getCategory().getId());
                intent.putExtra("category_title", news.getCategory().getTitle());
                context.startActivity(intent);
            });
            holder.itemView.setOnClickListener(view -> onNewsClickListener.onShowNews(news.getId()));
        } else {
            ViewHolderBeforeItem holderItem = (ViewHolderBeforeItem) holder;
            holderItem.tvTitle.setText("پر بازدیدترین ها");
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderBeforeItem extends RecyclerView.ViewHolder {

        TextView tvTitle;
        AppCompatImageView imageView;

        public ViewHolderBeforeItem(@NonNull ItemBeforeNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            imageView = binding.imageView;
        }

    }

    public class ViewHolderNews extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDescription;
        TextView tvCategory;

        public ViewHolderNews(@NonNull ItemHotNewsBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            tvDescription = binding.tvDescription;
            tvCategory = binding.tvCategory;
        }

    }

    public void addItems(ArrayList<NewsPosterResponse.PosterNews> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(NewsPosterResponse.PosterNews item) {
        this.list.add(0, item);
        notifyItemInserted(0);
    }

    public void clearAll() {
        this.list.clear();
        notifyDataSetChanged();
    }


}
