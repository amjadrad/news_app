//package ir.tildaweb.news.adapter;
//
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import java.util.List;
//
//
//public class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//    private <T> List<Class<T>> list;
//    private View view;
//
//    public <T> BaseAdapter(List<Class<T>> list, View view) {
//        this.list = list;
//        this.view = view;
//    }
//
//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return null;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//    public void add(Class<?> item) {
//        this.list.add(item);
//        notifyItemInserted(list.size() - 1);
//    }
//
//    public void add(Class<?> item, int index) {
//        this.list.add(index, item);
//        notifyItemInserted(index);
//    }
//
//    public void addFirst(Class<?> item) {
//        add(item, 0);
//    }
//
//    public void add(List<Class<?>> list) {
//        this.list.addAll(list);
//        notifyDataSetChanged();
//    }
//
//    public void clear() {
//        this.list.clear();
//        notifyDataSetChanged();
//    }
//
//    public void delete(int index) {
//        list.remove(index);
//        notifyItemRemoved(index);
//    }
//
//    public void delete(Class<?> aClass) {
//        int i = 0;
//        for (Class<?> item : list) {
//            if (item == aClass) {
//                delete(i);
//                break;
//            }
//            i++;
//        }
//    }
//
//    public void delete(Integer itemId) {
//        int i = 0;
//        for (Class<?> item : list) {
//            try {
//                if (item.getField("id").equals(itemId)) {
//                    delete(i);
//                    break;
//                }
//            } catch (NoSuchFieldException e) {
//                e.printStackTrace();
//            }
//            i++;
//        }
//    }
//
//}
