package ir.tildaweb.news.listeners;

public interface OnAdminUserNewsItemClickListener {

    void onEdit(int id);

    void onReject(int id);

    void onDelete(int id, String title);
}
