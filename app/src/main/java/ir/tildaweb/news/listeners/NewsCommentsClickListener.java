package ir.tildaweb.news.listeners;

public interface NewsCommentsClickListener {
    void onReplyComment(int newsId, int replyId);
}