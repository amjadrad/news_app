package ir.tildaweb.news.listeners;

public interface ItemClickListener {
    void onEdit(int id);
    void onDelete(int id , String title);
}