package ir.tildaweb.news.listeners;

public interface AdminItemClickListener {
    void onEdit(int id);
    void onAccess(int id , String name);
    void onDelete(int id , String title);
}