package ir.tildaweb.news.listeners;

public interface OnAppCommentItemClickListener {

    void onDelete(int id);
    void onShow(String text);
}