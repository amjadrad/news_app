package ir.tildaweb.news.listeners;

public interface NewsClickListener {
    void onLike(int id);
    void onDisLike(int id);
    void onFavorite(int id);
    void onCreateComment(int id);
    void onCommentsList(int id);
    void onShowNews(int id);
    void onVisitNews(int id);
}