package ir.tildaweb.news.listeners;

public interface OnAdminNewsCommentItemClickListener {
    void onActiveChange(int newsCommentId, boolean isActive);
    void onCommentSeen(int newsCommentId);
}
