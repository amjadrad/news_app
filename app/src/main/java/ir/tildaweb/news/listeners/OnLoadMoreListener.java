package ir.tildaweb.news.listeners;

public interface OnLoadMoreListener {
    void onLoadMore();
}