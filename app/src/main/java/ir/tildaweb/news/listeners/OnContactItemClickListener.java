package ir.tildaweb.news.listeners;

public interface OnContactItemClickListener {
    void onShow(int seen, int id, String title, String description);

    void onDelete(int id, String title);
}