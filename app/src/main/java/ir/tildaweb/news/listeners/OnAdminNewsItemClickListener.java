package ir.tildaweb.news.listeners;

public interface OnAdminNewsItemClickListener {

    void onEdit(int id);

    void onComments(int id);

    void onDelete(int id, String title);
}
