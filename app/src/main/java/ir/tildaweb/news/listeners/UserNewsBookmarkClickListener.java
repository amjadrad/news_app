package ir.tildaweb.news.listeners;

public interface UserNewsBookmarkClickListener {
    void onShow(int newId);

    void onDeleteBookmark(int userId, int newsId);
}