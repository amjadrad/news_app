package ir.tildaweb.news.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ir.hamsaa.persiandatepicker.util.PersianCalendar;

public class DateUtils {

    private String TAG = this.getClass().getName();

    public Calendar instance(String dateTime) {
        DateObject dateObject = parseDateTime(dateTime);
        Calendar calendar = Calendar.getInstance();
        calendar.set(dateObject.year, dateObject.month - 1, dateObject.day, dateObject.hour, dateObject.minute, dateObject.second);
        return calendar;
    }

    public String showJalali(String dateTime) {
        DateObject dateObject = parseDateTime(dateTime);
        String dateJalali = "";
        PersianCalendar persianCalendar = new PersianCalendar();
        persianCalendar.set(dateObject.year, dateObject.month - 1, dateObject.day);
        dateJalali = (persianCalendar.getPersianYear()) + "-" + getWithZeroDate(persianCalendar.getPersianMonth()) + "-" + getWithZeroDate(persianCalendar.getPersianDay());
        if (dateTime.length() > 11) {
            dateJalali += " " + dateTime.substring(11, dateTime.length() - 3);
        }
        return dateJalali;
    }

    private DateObject parseDateTime(String dateTime) {
        DateObject dateObject = new DateObject();
        dateObject.year = Integer.parseInt(dateTime.substring(0, 4));
        dateObject.month = Integer.parseInt(dateTime.substring(5, 7));
        dateObject.day = Integer.parseInt(dateTime.substring(8, 10));
        if (dateTime.length() > 11) {
            dateObject.hour = Integer.parseInt(dateTime.substring(11, 13));
            dateObject.minute = Integer.parseInt(dateTime.substring(14, 16));
            if (dateTime.length() > 17) {
                dateObject.second = Integer.parseInt(dateTime.substring(17));
            }
        }
        return dateObject;
    }


    //Old

    public interface DateHelperPresenter {
        void onDateSelected(Date date, String datePersian, String dateAD, String justDatePersian, String justDateAd, String justTime, int requestCode);
    }

    public static String getTodayDateAD() {
        String date = "", time = "";
        Date d = new Date();
        date = d.getYear() + 1900 + "-" + getWithZeroDate(d.getMonth() + 1) + "-" + getWithZeroDate(d.getDate());
        time = getWithZeroDate(d.getHours()) + ":" + getWithZeroDate(d.getMinutes()) + ":" + getWithZeroDate(d.getSeconds());

        return date + " " + time;
    }


    public static String getWithZeroDate(int i) {
        if (i < 10) {
            return "0" + i;
        } else {
            return i + "";
        }
    }

    public static String getWithFormatTimeStamp(int y, int m, int d) {
        return (y + 1900) + "-" + getWithZeroDate(m + 1) + "-" + getWithZeroDate(d);
    }

    public static int getHourMinutes(int h, int m) {
        return (h * 60) + m;
    }

    public static int getHourMinutes(String time) {
        try {
            String[] arr = time.split(":");
            int h = Integer.parseInt(arr[0]);
            int m = Integer.parseInt(arr[1]);
            return (h * 60) + m;
        } catch (Exception e) {
            return 0;
        }
    }


    public static String getTimeWithZero(int h, int m) {
        String result = "";
        if (h < 10) {
            result += "0" + h;
        } else {
            result += h;
        }

        result += ":";
        if (m < 10) {
            result += "0" + m;
        } else {
            result += m;
        }

        return result;
    }


    public static Integer diffDateDays(String dateAD) {
        Integer days = null;
        try {
            Calendar calendar = Calendar.getInstance();
            int y = Integer.parseInt(dateAD.substring(0, 4));
            int m = Integer.parseInt(dateAD.substring(5, 7)) - 1;
            int d = Integer.parseInt(dateAD.substring(8, 10));
            calendar.set(y, m, d);
            Calendar calendarToday = Calendar.getInstance();
            long timeDef = (8 * 60 * 60 * 1000) - ((60 * 1000) + (50 * 1000));
            long diff = calendarToday.getTimeInMillis() - ((calendar.getTimeInMillis() - timeDef));
            days = Math.round(diff / (1000 * 60 * 60 * 24));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return days;
    }

    public static Integer diffDateDays(String dateADFirst, String dateAdSecond, boolean withTime) {
        Integer days = null;
        try {
            Calendar calendar1 = Calendar.getInstance();
            int y1 = Integer.parseInt(dateADFirst.substring(0, 4));
            int m1 = Integer.parseInt(dateADFirst.substring(5, 7)) - 1;
            int d1 = Integer.parseInt(dateADFirst.substring(8, 10));
            int h1 = 0;
            int min1 = 0;
            if (withTime) {
                h1 = Integer.parseInt(dateADFirst.substring(11, 13));
                min1 = Integer.parseInt(dateADFirst.substring(14, 16));
            }
            calendar1.set(y1, m1, d1, h1, min1);
            Calendar calendar2 = Calendar.getInstance();
            int y2 = Integer.parseInt(dateAdSecond.substring(0, 4));
            int m2 = Integer.parseInt(dateAdSecond.substring(5, 7)) - 1;
            int d2 = Integer.parseInt(dateAdSecond.substring(8, 10));
            int h2 = 0;
            int min2 = 0;
            if (withTime) {
                h2 = Integer.parseInt(dateAdSecond.substring(11, 13));
                min2 = Integer.parseInt(dateAdSecond.substring(14, 16));
            }
            calendar2.set(y2, m2, d2, h2, min2);

            long timeDef = (8 * 60 * 60 * 1000) - ((60 * 1000) + (50 * 1000));
            long diff = calendar2.getTimeInMillis() - ((calendar1.getTimeInMillis() - timeDef));
            days = Math.round(diff / (1000 * 60 * 60 * 24));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return days;
    }


    public static DateClass diffDateDays(int dateFirst, int dateSecond) {
        DateClass dateClass = new DateClass();
        try {

            int diff = dateSecond - dateFirst;
            int days = diff / (60 * 60 * 24);
            int hours = diff - (days * ((60 * 60 * 24)));
            int minutes = (diff / (60));
            int seconds = diff;

            dateClass.setDays(days);
            dateClass.setHours(hours);
            dateClass.setMinutes(minutes);
            dateClass.setSeconds(seconds);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateClass;
    }


    public static class DateClass {
        private int days, hours, minutes, seconds;

        public int getDays() {
            return days;
        }

        public void setDays(int days) {
            this.days = days;
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }

        public int getSeconds() {
            return seconds;
        }

        public void setSeconds(int seconds) {
            this.seconds = seconds;
        }
    }

    public static String getTodayNameAD() {
        Calendar calendar = Calendar.getInstance();
        int x = calendar.getTime().getDay();
        switch (x) {
            case 0: {
                return "sunday";
            }
            case 1: {
                return "monday";
            }
            case 2: {
                return "tuesday";
            }
            case 3: {
                return "wednesday";
            }
            case 4: {
                return "thursday";
            }
            case 5: {
                return "friday";
            }
            case 6: {
                return "saturday";
            }
            default: {
                return "saturday";
            }
        }

    }


    public long getParsedDateMillis(String dateTime) {

        DateObject dateObject = parseDateTime(dateTime);
        Calendar calendar = Calendar.getInstance();
        calendar.set(dateObject.year, dateObject.month - 1, dateObject.day, dateObject.hour, dateObject.minute, dateObject.second);

        return calendar.getTimeInMillis();
    }

    public class DateObject {
        public int year;
        public int month;
        public int day;
        public int hour;
        public int minute;
        public int second;

        @Override
        public String toString() {
            return "DateObject{" +
                    "year=" + year +
                    ", month=" + month +
                    ", day=" + day +
                    ", hour=" + hour +
                    ", minute=" + minute +
                    ", second=" + second +
                    '}';
        }
    }

    public static String getTodayDateTime(boolean startDay) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        today.setTime(today.getTime());
        if (startDay) {
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
        }
        return dateFormat.format(today);
    }

    public DateObject getParsedRemindTime(long timeMillis) {

        long sec = 1000;
        long min = 60 * sec;
        long hour = 60 * min;
        long day = 24 * hour;

        long days = timeMillis / day;
        long hours = (timeMillis - (day * days)) / hour;
        long minutes = (timeMillis - ((day * days) + (hour * hours))) / min;
        long seconds = (timeMillis - ((day * days) + (hour * hours) + (min * minutes))) / sec;

        DateObject dateObject = new DateObject();
        dateObject.day = (int) days;
        dateObject.hour = (int) hours;
        dateObject.minute = (int) minutes;
        dateObject.second = (int) seconds;
        return dateObject;
    }

    public String getParsedRemindTimePersianText(DateObject dateObject) {
        StringBuilder stringBuilder = new StringBuilder();
        if (dateObject.day > 0) {
            stringBuilder.append(dateObject.day);
            stringBuilder.append(" روز");
        }
        if (dateObject.hour > 0) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(" و ");
            }
            stringBuilder.append(dateObject.hour);
            stringBuilder.append(" ساعت");
        }
        if (dateObject.minute > 0) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(" و ");
            }
            stringBuilder.append(dateObject.minute);
            stringBuilder.append(" دقیقه");
        }
        if (dateObject.second > 0) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(" و ");
            }
            stringBuilder.append(dateObject.second);
            stringBuilder.append(" ثانیه");
        }

        return stringBuilder.toString();

    }

    public String getParsedRemindTimeText(DateObject dateObject) {
        StringBuilder stringBuilder = new StringBuilder();
        if (dateObject.day > 0) {
            if (dateObject.day < 10) {
                stringBuilder.append("0");
            }
            stringBuilder.append(dateObject.day);
            stringBuilder.append(":");
        }
        if (dateObject.hour > 0) {
            if (dateObject.hour < 10) {
                stringBuilder.append("0");
            }
            stringBuilder.append(dateObject.hour);
            stringBuilder.append(":");
        }
        if (dateObject.minute > 0) {
            if (dateObject.minute < 10) {
                stringBuilder.append("0");
            }
            stringBuilder.append(dateObject.minute);
            stringBuilder.append(":");
        }
        if (dateObject.second > 0) {
            if (dateObject.second < 10) {
                stringBuilder.append("0");
            }
            stringBuilder.append(dateObject.second);
        }

        return stringBuilder.toString();

    }

    public static String getDateTimeParsed(String datetime) {
        return datetime.replace("T", " ").replace(".000000Z", "");

    }


}
