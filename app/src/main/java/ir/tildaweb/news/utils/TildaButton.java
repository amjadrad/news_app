package ir.tildaweb.news.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import ir.tildaweb.news.data.pref.AppPreferencesHelper;

public class TildaButton extends AppCompatButton {

    private String TAG = this.getClass().getName();

    public TildaButton(@NonNull Context context) {
        super(context);
        setTexSize(context);
    }

    public TildaButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setTexSize(context);
    }

    public TildaButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTexSize(context);
    }


    private void setTexSize(Context context) {
        AppPreferencesHelper appPreferencesHelper = new AppPreferencesHelper(context);
        float textSizeFactor = appPreferencesHelper.getTextSizePref();
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * textSizeFactor);
    }
}
