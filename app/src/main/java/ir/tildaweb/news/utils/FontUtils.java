package ir.tildaweb.news.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;

public class FontUtils {

    public static int spToPx(Context context, float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static Typeface getFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/iran_yekan_fa_regular.ttf");
    }

    public static Typeface getFontGhasem(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/ghasem.ttf");
    }

    public static Typeface getFontCasablanca(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/casablanca.ttf");
    }


    public static Typeface getFontKalameh(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/kalameh.ttf");
    }


}
