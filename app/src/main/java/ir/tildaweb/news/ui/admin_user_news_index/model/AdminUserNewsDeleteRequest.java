package ir.tildaweb.news.ui.admin_user_news_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminUserNewsDeleteRequest {

    @SerializedName("user_news_id")
    private Integer userNewsId;
    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public Integer getUserNewsId() {
        return userNewsId;
    }

    public void setUserNewsId(Integer userNewsId) {
        this.userNewsId = userNewsId;
    }
}
