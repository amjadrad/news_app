package ir.tildaweb.news.ui.app_comments_list.view;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.adapter.AdapterAdminAppComments;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityAppCommentsListBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.listeners.OnAppCommentItemClickListener;
import ir.tildaweb.news.listeners.OnContactItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsAverageRatingResponse;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsDeleteResponse;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsListResponse;
import ir.tildaweb.news.ui.app_comments_list.presenter.AppCommentsListActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AppCommentsListActivity extends BaseActivity implements View.OnClickListener, AppCommentsListActivityPresenter.View, OnLoadMoreListener, OnAppCommentItemClickListener {

    private ActivityAppCommentsListBinding binding;
    private AppCommentsListActivityPresenter presenter;
    private AdapterAdminAppComments adapterAdminAppComments;
    private int nextPage = 1;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAppCommentsListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tvToolbarTitle.setText(String.format("%s", "نظرات کاربران در مورد برنامه"));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        presenter = new AppCommentsListActivityPresenter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterAdminAppComments = new AdapterAdminAppComments(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterAdminAppComments);

        showLoadingFullPage();
        presenter.requestAppCommentsList(nextPage);
        presenter.requestAppCommentsAverageRating();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        }
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestAppCommentsList(nextPage);
        }
    }


    @Override
    public void onDelete(int id) {
        DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(AppCommentsListActivity.this, "آیا میخواهید این نظر را حذف کنید؟", "");
        dialogConfirmMessage.setOnConfirmClickListener(view -> {
            dialogConfirmMessage.dismiss();
            showLoadingFullPage();
            presenter.requestAppCommentDelete(id);
        });
        dialogConfirmMessage.setDanger();
        dialogConfirmMessage.setConfirmButtonText("حذف");
        dialogConfirmMessage.show();
    }

    @Override
    public void onShow(String text) {
        DialogMessage dialogMessage = new DialogMessage(AppCommentsListActivity.this, "متن کامل نظر", String.valueOf(text));
        dialogMessage.setOnConfirmClickListener(view -> {
            dialogMessage.dismiss();
        });
        dialogMessage.show();
    }

    @Override
    public void onResponseAppCommentsList(AppCommentsListResponse response) {
        dismissLoading();
        if (response.getData().getAppComments().size() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
        adapterAdminAppComments.addItems(response.getData().getAppComments());
        nextPage = VolleyRequestController.getNextPage(response.getData().getNextPageUrl());
    }

    @Override
    public void onErrorResponseAppCommentsUsList(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت لیست نظرها وجود ندارد.");
    }

    @Override
    public void onResponseAppCommentsAverageRating(AppCommentsAverageRatingResponse response) {
        binding.tvAverageRating.setText(String.format("%s", Math.round(response.getAverageRating())));
    }

    @Override
    public void onErrorResponseAppCommentsAverageRating(VolleyError volleyError) {

    }

    @Override
    public void onResponseAppCommentsDelete(AppCommentsDeleteResponse response) {
        dismissLoading();
        toast("با موفقیت حذف شد.");
        presenter.requestAppCommentsAverageRating();
        adapterAdminAppComments.deleteItem(response.getAppCommentId());
    }

    @Override
    public void onErrorResponseAppCommentsDelete(VolleyError volleyError) {
        dismissLoading();
        toast("امکان حذف نظر وجود ندارد. مجدد امتحان کنید.");
    }
}