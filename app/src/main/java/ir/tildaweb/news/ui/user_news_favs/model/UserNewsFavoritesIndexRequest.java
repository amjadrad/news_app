package ir.tildaweb.news.ui.user_news_favs.model;

import com.google.gson.annotations.SerializedName;

public class UserNewsFavoritesIndexRequest {

    @SerializedName("user_id")
    private Integer userId;
    @SerializedName("page")
    private Integer page;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
