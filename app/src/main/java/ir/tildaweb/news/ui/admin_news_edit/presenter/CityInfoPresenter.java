package ir.tildaweb.news.ui.admin_news_edit.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.profile_edit.model.CitiesRequest;
import ir.tildaweb.news.ui.profile_edit.model.CitiesResponse;
import ir.tildaweb.news.ui.profile_edit.model.ProvincesResponse;

public class CityInfoPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public CityInfoPresenter(View view) {
        this.view = view;
    }

    public interface View {
        void onResponseProvinces(ProvincesResponse provincesResponse);
        void onErrorResponseProvinces(VolleyError volleyError);
        void onResponseCities(CitiesResponse citiesResponse);
        void onErrorResponseCities(VolleyError volleyError);
    }

    public void requestProvinces() {
        String url = Endpoints.BASE_URL_PROVINCES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            ProvincesResponse provincesResponse = DataParser.fromJson(response, ProvincesResponse.class);
            view.onResponseProvinces(provincesResponse);
        }
                , error -> {
            view.onErrorResponseProvinces(error);
        });

        volleyRequestController.start();
    }

    public void requestCities(int provinceId) {
        String url = Endpoints.BASE_URL_CITIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CitiesResponse citiesResponse = DataParser.fromJson(response, CitiesResponse.class);
            view.onResponseCities(citiesResponse);
        }
                , error -> {
            view.onErrorResponseCities(error);
        });

        CitiesRequest citiesRequest = new CitiesRequest();
        citiesRequest.setProvinceId(provinceId);
        volleyRequestController.setParameters(citiesRequest);
        volleyRequestController.start();
    }
}