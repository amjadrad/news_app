package ir.tildaweb.news.ui.user_news_favs.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.user_news_favs.model.UserNewsFavoritesIndexRequest;
import ir.tildaweb.news.ui.user_news_favs.model.UserNewsFavoritesIndexResponse;

public class UserNewsBookmarksActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public UserNewsBookmarksActivityPresenter(View view) {
        this.view = view;
    }

    public void requestUserNewsFavorites(int page, int userId) {

        String url = Endpoints.BASE_URL_USER_NEWS_FAVORITES_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            UserNewsFavoritesIndexResponse resp = DataParser.fromJson(response, UserNewsFavoritesIndexResponse.class);
            view.onResponseUserNewsFavoritesIndex(resp);
        }
                , error -> {
            view.onErrorResponseUserNewsFavoritesIndex(error);
        });

        UserNewsFavoritesIndexRequest request = new UserNewsFavoritesIndexRequest();
        request.setUserId(userId);
        request.setPage(page);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsFavorite(int newsId, int userId) {
        String url = Endpoints.BASE_URL_USER_NEWS_FAVORITES_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            UserNewsFavoriteResponse userNewsFavoriteResponse = DataParser.fromJson(response, UserNewsFavoriteResponse.class);
            view.onResponseUserNewsFavorite(userNewsFavoriteResponse);
        }
                , error -> {
            view.onErrorResponseUserNewsFavorite(error);
        });

        UserNewsFavoriteRequest request = new UserNewsFavoriteRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public interface View {

        void onResponseUserNewsFavoritesIndex(UserNewsFavoritesIndexResponse response);

        void onErrorResponseUserNewsFavoritesIndex(VolleyError volleyError);

        void onResponseUserNewsFavorite(UserNewsFavoriteResponse response);

        void onErrorResponseUserNewsFavorite(VolleyError volleyError);

    }
}