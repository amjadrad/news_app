package ir.tildaweb.news.ui.user_news_favs.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterUserNewsFavs;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityUserNewsBookmarksBinding;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.listeners.UserNewsBookmarkClickListener;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.news_show.view.NewsShowActivity;
import ir.tildaweb.news.ui.user_news_favs.model.UserNewsFavoritesIndexResponse;
import ir.tildaweb.news.ui.user_news_favs.presenter.UserNewsBookmarksActivityPresenter;

public class UserNewsBookmarksActivity extends BaseActivity implements View.OnClickListener, UserNewsBookmarksActivityPresenter.View, OnLoadMoreListener, UserNewsBookmarkClickListener {

    private String TAG = this.getClass().getName();
    private ActivityUserNewsBookmarksBinding binding;
    private UserNewsBookmarksActivityPresenter presenter;
    private AdapterUserNewsFavs adapterUserNewsFavs;
    private int nextPage = 1;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserNewsBookmarksBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new UserNewsBookmarksActivityPresenter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterUserNewsFavs = new AdapterUserNewsFavs(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterUserNewsFavs);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("ذخیره شده ها");
        showLoadingFullPage();
        presenter.requestUserNewsFavorites(nextPage, getAppPreferencesHelper().getUserId());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imageViewBack) {
            onBackPressed();
        }
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestUserNewsFavorites(nextPage, getAppPreferencesHelper().getUserId());
        }
    }

    @Override
    public void onResponseUserNewsFavoritesIndex(UserNewsFavoritesIndexResponse response) {
        dismissLoading();
        if (response.getUserNewsFavoritesData() != null && response.getUserNewsFavoritesData().getUserNewsFavorites() != null) {
            adapterUserNewsFavs.addItems(response.getUserNewsFavoritesData().getUserNewsFavorites());
            nextPage = VolleyRequestController.getNextPage(response.getUserNewsFavoritesData().getNextPageUrl());
            binding.linearNoItem.setVisibility(View.GONE);
        }
        if (nextPage == -1 && adapterUserNewsFavs.getItemCount() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorResponseUserNewsFavoritesIndex(VolleyError volleyError) {
        dismissLoading();
    }

    @Override
    public void onResponseUserNewsFavorite(UserNewsFavoriteResponse response) {
        adapterUserNewsFavs.deleteItem(response.getFavorite().getId());
    }

    @Override
    public void onErrorResponseUserNewsFavorite(VolleyError volleyError) {
        toast("مشکلی در حذف این خبر از لیست علاقه مندی ها به وجود آمد.");
    }


    @Override
    public void onShow(int newId) {
        Intent intent = new Intent(UserNewsBookmarksActivity.this, NewsShowActivity.class);
        intent.putExtra("news_id", newId);
        startActivity(intent);
    }

    @Override
    public void onDeleteBookmark(int userId, int newsId) {
        presenter.requestNewsFavorite(newsId, userId);
    }
}