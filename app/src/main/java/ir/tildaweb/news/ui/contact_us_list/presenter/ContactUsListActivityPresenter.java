package ir.tildaweb.news.ui.contact_us_list.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.contact_us_list.model.ContactUsListRequest;
import ir.tildaweb.news.ui.contact_us_list.model.ContactUsListResponse;
import ir.tildaweb.news.ui.contact_us_list.model.ContactsSeenRequest;
import ir.tildaweb.news.ui.contact_us_list.model.ContactsSeenResponse;


public class ContactUsListActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public ContactUsListActivityPresenter(View view) {
        this.view = view;
    }


    public void requestContactUsList(int page) {
        String url = Endpoints.BASE_URL_ADMIN_CONTACT_US_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            ContactUsListResponse contactUsListResponse = DataParser.fromJson(response, ContactUsListResponse.class);
            view.onResponseContactUsList(contactUsListResponse);
        }
                , error -> {
            view.onErrorResponseContactUsList(error);
        });

        ContactUsListRequest request = new ContactUsListRequest();
        request.setPage(page);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestContactsSeen(int id) {
        String url = Endpoints.BASE_URL_ADMIN_CONTACT_US_SEEN;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            ContactsSeenResponse contactsSeenResponse = DataParser.fromJson(response, ContactsSeenResponse.class);
            view.onResponseContactsSeen(contactsSeenResponse);
        }
                , error -> {
            view.onErrorResponseContactsSeen(error);
        });

        ContactsSeenRequest request = new ContactsSeenRequest();
        request.setContactId(id);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestContactsDelete(int id) {
        String url = Endpoints.BASE_URL_ADMIN_CONTACT_US_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            ContactsSeenResponse contactsSeenResponse = DataParser.fromJson(response, ContactsSeenResponse.class);
            view.onResponseContactsDelete(contactsSeenResponse);
        }
                , error -> {
            view.onErrorResponseContactsDelete(error);
        });

        ContactsSeenRequest request = new ContactsSeenRequest();
        request.setContactId(id);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseContactUsList(ContactUsListResponse response);

        void onErrorResponseContactUsList(VolleyError volleyError);

        void onResponseContactsSeen(ContactsSeenResponse response);

        void onErrorResponseContactsSeen(VolleyError volleyError);

        void onResponseContactsDelete(ContactsSeenResponse response);

        void onErrorResponseContactsDelete(VolleyError volleyError);
    }


}