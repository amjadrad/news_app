package ir.tildaweb.news.ui.news_show.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;

public class NewsShowResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("news")
    private NewsResponse.NewsData.News news;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public NewsResponse.NewsData.News getNews() {
        return news;
    }

    public void setNews(NewsResponse.NewsData.News news) {
        this.news = news;
    }
}
