package ir.tildaweb.news.ui.admin_admins_edit.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.admin_admins_index.model.AdminsResponse;

public class AdminAdminsShowResponse {

    @SerializedName("admin")
    private AdminsResponse.Admin admin;

    public AdminsResponse.Admin getAdmin() {
        return admin;
    }

    public void setAdmin(AdminsResponse.Admin admin) {
        this.admin = admin;
    }
}
