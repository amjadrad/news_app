package ir.tildaweb.news.ui.admin_admins_create.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_admins_create.model.AdminAdminsStoreRequest;
import ir.tildaweb.news.ui.admin_admins_create.model.AdminAdminsStoreResponse;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateRequest;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateResponse;


public class AdminAdminsCreateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminAdminsCreateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestAdminStore(AdminAdminsStoreRequest adminAdminsStoreRequest) {
        String url = Endpoints.BASE_URL_ADMIN_ADMINS_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminAdminsStoreResponse adminAdminsStoreResponse = DataParser.fromJson(response, AdminAdminsStoreResponse.class);
            view.onResponseAdminAdminsStore(adminAdminsStoreResponse);
        }
                , error -> {
            view.onErrorResponseAdminAdminsStore(error);
        });
        volleyRequestController.setBody(adminAdminsStoreRequest);
        volleyRequestController.start();
    }

    public interface View {
        void onResponseAdminAdminsStore(AdminAdminsStoreResponse adminAdminsStoreResponse);

        void onErrorResponseAdminAdminsStore(VolleyError volleyError);
    }

}