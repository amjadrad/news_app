package ir.tildaweb.news.ui.admin_admins_edit.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsShowRequest;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsShowResponse;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsUpdateRequest;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsUpdateResponse;


public class AdminAdminsEditActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminAdminsEditActivityPresenter(View view) {
        this.view = view;
    }

    public void requestAdminShow(Integer adminId) {
        String url = Endpoints.BASE_URL_ADMIN_ADMINS_SHOW;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminAdminsShowResponse adminAdminsShowResponse = DataParser.fromJson(response, AdminAdminsShowResponse.class);
            view.onResponseAdminAdminsShow(adminAdminsShowResponse);
        }
                , error -> {
            view.onErrorResponseAdminAdminsShow(error);
        });
        AdminAdminsShowRequest adminAdminsShowRequest = new AdminAdminsShowRequest();
        adminAdminsShowRequest.setAdminId(adminId);
        volleyRequestController.setParameters(adminAdminsShowRequest);
        volleyRequestController.start();
    }

    public void requestAdminUpdate(AdminAdminsUpdateRequest adminAdminsUpdateRequest) {
        String url = Endpoints.BASE_URL_ADMIN_ADMINS_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminAdminsUpdateResponse adminAdminsUpdateResponse = DataParser.fromJson(response, AdminAdminsUpdateResponse.class);
            view.onResponseAdminAdminsUpdate(adminAdminsUpdateResponse);
        }
                , error -> {
            view.onErrorResponseAdminAdminsUpdate(error);
        });
        volleyRequestController.setBody(adminAdminsUpdateRequest);
        volleyRequestController.start();
    }

    public interface View {
        void onResponseAdminAdminsUpdate(AdminAdminsUpdateResponse adminAdminsUpdateResponse);

        void onErrorResponseAdminAdminsUpdate(VolleyError volleyError);

        void onResponseAdminAdminsShow(AdminAdminsShowResponse adminAdminsShowResponse);

        void onErrorResponseAdminAdminsShow(VolleyError volleyError);
    }

}