package ir.tildaweb.news.ui.sigin_verification.view;


import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;

import ir.tildaweb.news.R;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.databinding.ActivitySignVerificationBinding;
import ir.tildaweb.news.receivers.AppSMSBroadcastReceiver;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditResponse;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.main.view.MainActivity;
import ir.tildaweb.news.ui.sigin.model.SigninResponse;
import ir.tildaweb.news.ui.sigin.presenter.SignActivityPresenter;
import ir.tildaweb.news.ui.sigin.view.SignActivity;
import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationResponse;
import ir.tildaweb.news.ui.sigin_verification.presenter.SigninVerificationActivityPresenter;
import ir.tildaweb.news.ui.splash.model.AdminAccessRequest;


public class SigninVerificationActivity extends BaseActivity implements View.OnClickListener, SigninVerificationActivityPresenter.View, SignActivityPresenter.View {

    private ActivitySignVerificationBinding binding;
    private String TAG = this.getClass().getName();
    private SigninVerificationActivityPresenter presenter;
    private SignActivityPresenter signinActivityPresenter;
    private String phone;
    private boolean isAdmin;
    private AppSMSBroadcastReceiver appSMSBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignVerificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setFullScreen();

        presenter = new SigninVerificationActivityPresenter(this);
        signinActivityPresenter = new SignActivityPresenter(this);
        phone = getIntent().getExtras().getString("phone");
        isAdmin = getIntent().getExtras().getBoolean("is_admin");
        binding.btnNext.setOnClickListener(this);
        binding.tvResendSMS.setOnClickListener(this);
        startTimer();
        binding.etCode1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 1) {
                    binding.etCode2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCode2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 1) {
                    binding.etCode3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCode3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 1) {
                    binding.etCode4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCode4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 1) {
                    hideKeyboard(SigninVerificationActivity.this, binding.etCode4);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsRetriever();
        IntentFilter intentFilter = new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");
        appSMSBroadcastReceiver = new AppSMSBroadcastReceiver();
        appSMSBroadcastReceiver.setOnSmsReceiveListener(code -> {
            String verificationCode = code.replaceAll("[^0-9]+", "");
            binding.etCode1.setText(String.valueOf(verificationCode.charAt(0)));
            binding.etCode2.setText(String.valueOf(verificationCode.charAt(1)));
            binding.etCode3.setText(String.valueOf(verificationCode.charAt(2)));
            binding.etCode4.setText(String.valueOf(verificationCode.charAt(3)));
            binding.btnNext.callOnClick();
        });
        registerReceiver(appSMSBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(appSMSBroadcastReceiver);
    }

    private void startTimer() {
        binding.tvResendSMS.setEnabled(false);
        binding.tvResendSMS.setAlpha(0.3f);
        new CountDownTimer(120000, 1000) {

            @Override
            public void onTick(long l) {
                long minutes = (l / 1000) / 60;
                long seconds = (l / 1000) - (minutes * 60);

                String min = "";
                String sec = "";
                if (minutes < 10) {
                    min = "0" + minutes;
                } else {
                    min = "" + minutes;
                }

                if (seconds < 10) {
                    sec = "0" + seconds;
                } else {
                    sec = "" + seconds;
                }

                binding.tvTimer.setText(min + " : " + sec);
            }

            @Override
            public void onFinish() {
                binding.tvResendSMS.setEnabled(true);
                binding.tvResendSMS.setAlpha(1);
            }
        }.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext: {
                String code = binding.etCode1.getText().toString().trim();
                code += binding.etCode2.getText().toString().trim();
                code += binding.etCode3.getText().toString().trim();
                code += binding.etCode4.getText().toString().trim();
                if (code.length() == 4) {
                    showLoadingFullPage();
                    hideKeyboard(SigninVerificationActivity.this, binding.etCode4);
                    presenter.requestSigninVerification(phone, code);
                } else {
                    toast("کد 4 رقمی پیامک شده را وارد کنید.");
                }
                break;
            }
            case R.id.tvResendSMS: {
                showLoadingFullPage();
                signinActivityPresenter.requestSignin(phone);
                break;
            }
        }
    }

    @Override
    public void onResponseSignInVerification(SigninVerificationResponse response) {
        dismissLoading();
        getAppPreferencesHelper().setUserIsAdminPref(isAdmin);
        if (isNotNull(response.getAdmin()) && isNotNull(response.getAdmin().getType()))
            getAppPreferencesHelper().setUserAdminTypePref(response.getAdmin().getType());
        getAppPreferencesHelper().setUserId(response.getUser().getId());
        if (isNotNull(response.getAdmin())) {
            getAppPreferencesHelper().setAdminId(response.getAdmin().getId());
        } else {
            getAppPreferencesHelper().setAdminId(-1);
        }
        AppPreferencesHelper appPreferencesHelper = getAppPreferencesHelper();
        if (isNotNull(response.getUser().getFullName()))
            appPreferencesHelper.setUserFullNamePref(response.getUser().getFullName());
        if (isNotNull(response.getUser().getApiToken()))
            appPreferencesHelper.setUserApiTokenPref(response.getUser().getApiToken());
        if (isNotNull(response.getUser().getPhone()))
            appPreferencesHelper.setUserPhonePref(response.getUser().getPhone());
        if (isNotNull(response.getUser().getCity()))
            appPreferencesHelper.setUserCityIdPref(response.getUser().getCity().getId());
        if (isNotNull(response.getUser().getCity()))
            getAppPreferencesHelper().setUserCityTitlePref(response.getUser().getCity().getTitle());
        if (isNotNull(response.getUser().getCity()))
            getAppPreferencesHelper().setUserCenterCityTitlePref(response.getUser().getCity().getProvince().getCenterCity().getTitle());

        appPreferencesHelper.setLoginPref(true);

        if (getAppPreferencesHelper().getLoginPref() && getAppPreferencesHelper().getUserIsAdminPref() && getAppPreferencesHelper().getAdminId() > 0) {
            AdminAccessRequest request = new AdminAccessRequest();
            request.setAdminId(getAppPreferencesHelper().getAdminId());
            presenter.requestAdminAccess(request);
        } else {
            gotoNextPage();
        }
    }

    private void gotoNextPage() {
        startActivity(new Intent(SigninVerificationActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onErrorResponseSignInVerification(VolleyError volleyError) {
        dismissLoading();
        if (volleyError.networkResponse.statusCode == 401) {
            toast("کد وارد شده صحیح نمی باشد.");
        } else {
            toast("مشکلی رخ داد. لطفا مجددا امتحان کنید.");
        }

    }

    @Override
    public void onResponseUserInfo(AdminAccessEditResponse response) {

        AppPreferencesHelper appPreferencesHelper = getAppPreferencesHelper();
        for (AdminAccessEditResponse.AdminAccess adminAccess : response.getAdminAccess()) {
            if (adminAccess.getActions() != null) {
                String[] actions = adminAccess.getActions().split(",");
                switch (adminAccess.getType()) {
                    case "news":
                        for (String action : actions) {
                            switch (action) {
                                case "insert":
                                    appPreferencesHelper.setAdminAccessNewsInsert(true);
                                    break;
                                case "update":
                                    appPreferencesHelper.setAdminAccessNewsUpdate(true);
                                    break;
                                case "delete":
                                    appPreferencesHelper.setAdminAccessNewsDelete(true);
                                    break;
                                case "reviews":
                                    appPreferencesHelper.setAdminAccessNewsComments(true);
                                    break;
                            }
                        }
                        break;
                    case "categories":
                        for (String action : actions) {
                            switch (action) {
                                case "insert":
                                    appPreferencesHelper.setAdminAccessCategoriesInsert(true);
                                    break;
                                case "update":
                                    appPreferencesHelper.setAdminAccessCategoriesUpdate(true);
                                    break;
                                case "delete":
                                    appPreferencesHelper.setAdminAccessCategoriesDelete(true);
                                    break;
                            }
                        }
                        break;
                    case "user_news":
                        for (String action : actions) {
                            switch (action) {
                                case "update":
                                    appPreferencesHelper.setAdminAccessUserNewsUpdate(true);
                                    break;
                                case "delete":
                                    appPreferencesHelper.setAdminAccessUserNewsDelete(true);
                                    break;
                            }
                        }
                        break;
                    case "app_comments":
                        for (String action : actions) {
                            if ("all".equals(action)) {
                                appPreferencesHelper.setAdminAccessAppComments(true);
                            }
                        }
                        break;
                    case "user_contacts":
                        for (String action : actions) {
                            if ("all".equals(action)) {
                                appPreferencesHelper.setAdminAccessUserContacts(true);
                            }
                        }
                        break;
                }
            }
        }

        gotoNextPage();
    }

    @Override
    public void onErrorResponseUserInfo(VolleyError volleyError) {
        gotoNextPage();
    }


    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(SigninVerificationActivity.this, SignActivity.class));
    }

    @Override
    public void onResponseSignIn(SigninResponse signinResponse) {
        dismissLoading();
        toast("کدتایید مجددا پیامک شد.");
        startTimer();
        binding.etCode1.setText("");
        binding.etCode2.setText("");
        binding.etCode3.setText("");
        binding.etCode4.setText("");
    }

    @Override
    public void onErrorResponseSignIn(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ارسال پیامک رخ داد.");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == SMS_CONSENT_REQUEST) {
//            if (resultCode == Activity.RESULT_OK && data != null) {
//                // Get SMS message content
//                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
//                Log.d(TAG, "onActivityResult: " + message);
//            }
//        }
    }
}