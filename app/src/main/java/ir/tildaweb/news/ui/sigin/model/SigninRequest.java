package ir.tildaweb.news.ui.sigin.model;

import com.google.gson.annotations.SerializedName;

public class SigninRequest {

    @SerializedName("phone")
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
