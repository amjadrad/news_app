package ir.tildaweb.news.ui.app_comments_list.model;

import com.google.gson.annotations.SerializedName;

public class AppCommentsDeleteRequest {

    @SerializedName("app_comment_id")
    private Integer appCommentId;

    public Integer getAppCommentId() {
        return appCommentId;
    }

    public void setAppCommentId(Integer appCommentId) {
        this.appCommentId = appCommentId;
    }
}
