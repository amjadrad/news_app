package ir.tildaweb.news.ui.contact_us.model;

import com.google.gson.annotations.SerializedName;

public class ContactUsStoreRequest {

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("user_id")
    private Integer userId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
