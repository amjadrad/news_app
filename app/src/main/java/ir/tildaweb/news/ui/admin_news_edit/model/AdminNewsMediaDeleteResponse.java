package ir.tildaweb.news.ui.admin_news_edit.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsMediaDeleteResponse {

    @SerializedName("media_id")
    private Integer mediaId;

    public Integer getMediaId() {
        return mediaId;
    }

    public void setMediaId(Integer mediaId) {
        this.mediaId = mediaId;
    }
}