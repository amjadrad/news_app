package ir.tildaweb.news.ui.news_by_category.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.adapter.AdapterNews;
import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.cache.SearchHistoryModel;
import ir.tildaweb.news.databinding.ActivitySearchBinding;
import ir.tildaweb.news.dialogs.DialogNewsCommentCreate;
import ir.tildaweb.news.listeners.NewsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.AppCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsHotResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsMostVisitResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsPosterResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UsersCountResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.WeatherResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.presenter.NewsFragmentPresenter;
import ir.tildaweb.news.ui.news_comments.view.NewsCommentsIndexActivity;
import ir.tildaweb.news.ui.news_show.view.NewsShowActivity;

public class NewsByCategoryActivity extends BaseActivity implements View.OnClickListener, NewsFragmentPresenter.View, OnLoadMoreListener, NewsClickListener {

    private ActivitySearchBinding binding;
    private NewsFragmentPresenter presenter;
    private AdapterNews adapterNews;
    private int nextPage = 1;
    private int page = 1;
    private String search = null;
    private Integer categoryId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySearchBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        String categoryTitle = getIntent().getStringExtra("category_title");
        binding.toolbar.tvToolbarTitle.setText(String.format("جستجو خبر %s", categoryTitle));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        presenter = new NewsFragmentPresenter(this);
        categoryId = getIntent().getIntExtra("category_id", -1);
        binding.etSearch.setText(search);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterNews = new AdapterNews(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterNews);
        showLoadingFullPage();
        presenter.requestNewsSearch(nextPage, search, categoryId, getAppPreferencesHelper().getUserId(), getAppPreferencesHelper().getIsShowNewsGalleryPref());
        binding.btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onResponseNews(NewsResponse newsResponse) {
        dismissLoading();
        if (newsResponse.getData() != null && newsResponse.getData().getNews() != null) {
            adapterNews.addItems(newsResponse.getData().getNews());
            nextPage = VolleyRequestController.getNextPage(newsResponse.getData().getNextPageUrl());
        }
    }

    @Override
    public void onErrorResponseNews(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در دریافت اخبار به وجود آمد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseNewsHot(NewsHotResponse response) {

    }

    @Override
    public void onErrorResponseNewsHot(VolleyError volleyError) {

    }

    @Override
    public void onResponseNewsMostVisit(NewsMostVisitResponse response) {

    }

    @Override
    public void onErrorResponseNewsMostVisit(VolleyError volleyError) {

    }

    @Override
    public void onResponseNewsPostersMain(NewsPosterResponse response) {

    }

    @Override
    public void onErrorResponseNewsPostersMain(VolleyError volleyError) {

    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestNewsSearch(nextPage, search, null, getAppPreferencesHelper().getUserId(), getAppPreferencesHelper().getIsShowNewsGalleryPref());
        }
    }

    @Override
    public void onLike(int id) {
        dismissLoading();
        presenter.requestNewsLike(id, getAppPreferencesHelper().getUserId(), true);
    }

    @Override
    public void onDisLike(int id) {
        dismissLoading();
        presenter.requestNewsLike(id, getAppPreferencesHelper().getUserId(), false);
    }

    @Override
    public void onFavorite(int id) {

    }

    @Override
    public void onCreateComment(int id) {
        DialogNewsCommentCreate dialogNewsCommentCreate = new DialogNewsCommentCreate(NewsByCategoryActivity.this);
        dialogNewsCommentCreate.setOnConfirmClickListener(view -> {
            String comment = dialogNewsCommentCreate.getDescription();
            hideKeyboard(NewsByCategoryActivity.this, dialogNewsCommentCreate.getEtDescription());
            dialogNewsCommentCreate.dismiss();
            showLoadingFullPage();
            presenter.requestNewsCommentCreate(id, getAppPreferencesHelper().getUserId(), comment);
        });
        dialogNewsCommentCreate.show();
    }

    @Override
    public void onCommentsList(int id) {
        Intent intent = new Intent(NewsByCategoryActivity.this, NewsCommentsIndexActivity.class);
        intent.putExtra("news_id", id);
        startActivity(intent);
    }

    @Override
    public void onResponseNewsCommentsStore(NewsCommentStoreResponse newsCommentStoreResponse) {
        dismissLoading();
        toast("نظر شما با موفقیت ذخیره شد و پس از تایید منتظر خواهد شد.");
    }

    @Override
    public void onErrorResponseNewsCommentsStore(VolleyError volleyError) {
        dismissLoading();
        toast("خطایی هنگام ذخیره نظر رخ داد. مجدد امتحان کنید.");
    }

    @Override
    public void onResponseNewsLike(NewsLikeResponse newsLikeResponse) {
        dismissLoading();
        adapterNews.updateLike(newsLikeResponse);
    }

    @Override
    public void onErrorResponseNewsLike(VolleyError volleyError) {
        dismissLoading();
    }

    @Override
    public void onResponseUserNewsFavorite(UserNewsFavoriteResponse response) {

    }

    @Override
    public void onErrorResponseUserNewsFavorite(VolleyError volleyError) {

    }

    @Override
    public void onResponseNewsVisit(NewsLikeResponse response) {

    }

    @Override
    public void onErrorResponseNewsVisit(VolleyError volleyError) {

    }

    @Override
    public void onResponseWeathers(WeatherResponse response) {

    }

    @Override
    public void onErrorResponseWeathers(VolleyError volleyError) {

    }

    @Override
    public void onResponseUsersCount(UsersCountResponse response) {

    }

    @Override
    public void onErrorResponseUsersCount(VolleyError volleyError) {

    }

    @Override
    public void onResponseAppCommentStore(AppCommentStoreResponse response) {

    }

    @Override
    public void onErrorResponseAppCommentStore(VolleyError volleyError) {

    }


    @Override
    public void onShowNews(int id) {
        Intent intent = new Intent(NewsByCategoryActivity.this, NewsShowActivity.class);
        intent.putExtra("news_id", id);
        startActivity(intent);
    }

    @Override
    public void onVisitNews(int id) {
        presenter.requestNewsVisit(id, getAppPreferencesHelper().getUserId());
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        } else if (view.getId() == binding.btnConfirm.getId()) {
            nextPage = 1;
            page = 1;
            search = null;
            adapterNews.clearAll();
            showLoadingFullPage();
            String search = binding.etSearch.getText().toString().trim();
            SearchHistoryModel searchHistoryModel = DataParser.fromJson(getCacheManager().getUserSearchHistory(), SearchHistoryModel.class);
            if (searchHistoryModel == null) {
                searchHistoryModel = new SearchHistoryModel();
                searchHistoryModel.setSearchs(new ArrayList<>());
            }
            searchHistoryModel.getSearchs().add(0, search);
            if (searchHistoryModel.getSearchs().size() > 8) {
                searchHistoryModel.getSearchs().remove(8);
            }
            getCacheManager().setUserSearchHistory(DataParser.toJson(searchHistoryModel));
            presenter.requestNewsSearch(nextPage, search, null, getAppPreferencesHelper().getUserId(), getAppPreferencesHelper().getIsShowNewsGalleryPref());
        }
    }
}