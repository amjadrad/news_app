package ir.tildaweb.news.ui.news_show.view;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;
import com.asura.library.posters.Poster;
import com.asura.library.posters.RemoteImage;
import com.asura.library.posters.RemoteVideo;

import java.util.ArrayList;
import java.util.List;

import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterNewsComments;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityNewsShowBinding;
import ir.tildaweb.news.dialogs.DialogNewsCommentCreate;
import ir.tildaweb.news.listeners.NewsCommentsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.main.view.MainActivity;
import ir.tildaweb.news.ui.news_by_category.view.NewsByCategoryActivity;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;
import ir.tildaweb.news.ui.news_show.model.NewsShowResponse;
import ir.tildaweb.news.ui.news_comments.presenter.NewsCommentsIndexActivityPresenter;
import ir.tildaweb.news.ui.news_show.presenter.NewsShowActivityPresenter;
import ir.tildaweb.news.utils.IntentHelper;

public class NewsShowActivity extends BaseActivity implements View.OnClickListener, NewsCommentsIndexActivityPresenter.View, OnLoadMoreListener, NewsCommentsClickListener, NewsShowActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityNewsShowBinding binding;
    private NewsShowActivityPresenter presenter;
    private NewsCommentsIndexActivityPresenter newsCommentsIndexActivityPresenter;
    private AdapterNewsComments adapterNewsComments;
    private int newsId;
    private int nextPage = 1;
    private int page = 1;
    private NewsResponse.NewsData.News news;
    private ColorStateList cslMutedText;
    private ColorStateList cslPrimary;
    private ColorStateList cslDanger;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsId = getIntent().getIntExtra("news_id", -1);
        binding = ActivityNewsShowBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new NewsShowActivityPresenter(this);
        newsCommentsIndexActivityPresenter = new NewsCommentsIndexActivityPresenter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterNewsComments = new AdapterNewsComments(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterNewsComments);

        cslMutedText = AppCompatResources.getColorStateList(this, R.color.colorMutedText);
        cslPrimary = AppCompatResources.getColorStateList(this, R.color.colorPrimary);
        cslDanger = AppCompatResources.getColorStateList(this, R.color.colorDanger);

        binding.imageViewFavorite.setOnClickListener(this);
        binding.imageViewLike.setOnClickListener(this);
        binding.imageViewDislike.setOnClickListener(this);
        binding.imageViewShare.setOnClickListener(this);

        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("جزئیات خبر");
        showLoadingFullPage();
        presenter.requestNewsShow(newsId, getAppPreferencesHelper().getUserId());
        newsCommentsIndexActivityPresenter.requestNewsComments(nextPage, newsId);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imageViewBack) {
            onBackPressed();
        }
    }

    @Override
    public void onResponseNewsComments(NewsCommentsResponse newsCommentsResponse) {
        dismissLoading();
        if (newsCommentsResponse.getData() != null && newsCommentsResponse.getData().getComments() != null) {
            adapterNewsComments.addItems(newsCommentsResponse.getData().getComments());
            nextPage = VolleyRequestController.getNextPage(newsCommentsResponse.getData().getNextPageUrl());
        }
    }

    @Override
    public void onErrorResponseNewsComments(VolleyError volleyError) {
        dismissLoading();
    }

    @Override
    public void onResponseNewsCommentsReply(NewsCommentStoreResponse newsCommentStoreResponse) {
        toast("نظر شما با موفقیت ذخیره شد و پس از تایید منتظر خواهد شد.");
        dismissLoading();
    }

    @Override
    public void onErrorResponseNewsCommentsReply(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ثبت نظر رخ داد.");
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            newsCommentsIndexActivityPresenter.requestNewsComments(nextPage, newsId);
        }
    }

    @Override
    public void onReplyComment(int newsId, int replyId) {
        DialogNewsCommentCreate dialogNewsCommentCreate = new DialogNewsCommentCreate(this);
        dialogNewsCommentCreate.setOnConfirmClickListener(view -> {
            String comment = dialogNewsCommentCreate.getDescription();
            hideKeyboard(NewsShowActivity.this, dialogNewsCommentCreate.getEtDescription());
            dialogNewsCommentCreate.dismiss();
            showLoadingFullPage();
            newsCommentsIndexActivityPresenter.requestNewsCommentReply(newsId, replyId, getAppPreferencesHelper().getUserId(), comment);
        });
        dialogNewsCommentCreate.show();
    }

    @Override
    public void onResponseNewsShow(NewsShowResponse response) {
        dismissLoading();
        news = response.getNews();
        updateNews();
    }

    @Override
    public void onErrorResponseNewsShow(VolleyError volleyError) {
        toast("امکان دریافت اطلاعات خبر وجود ندارد.");
        dismissLoading();
    }


    public void onLike(int id) {
        dismissLoading();
        presenter.requestNewsLike(id, getAppPreferencesHelper().getUserId(), true);
    }


    public void onDisLike(int id) {
        dismissLoading();
        presenter.requestNewsLike(id, getAppPreferencesHelper().getUserId(), false);
    }


    public void onCreateComment(int id) {
        DialogNewsCommentCreate dialogNewsCommentCreate = new DialogNewsCommentCreate(NewsShowActivity.this);
        dialogNewsCommentCreate.setOnConfirmClickListener(view -> {
            String comment = dialogNewsCommentCreate.getDescription();
            hideKeyboard(NewsShowActivity.this, dialogNewsCommentCreate.getEtDescription());
            dialogNewsCommentCreate.dismiss();
            showLoadingFullPage();
            presenter.requestNewsCommentCreate(id, getAppPreferencesHelper().getUserId(), comment);
        });
        dialogNewsCommentCreate.show();
    }

    @Override
    public void onResponseNewsCommentsStore(NewsCommentStoreResponse newsCommentStoreResponse) {
        dismissLoading();
        toast("نظر شما با موفقیت ذخیره شد و پس از تایید منتظر خواهد شد.");
    }

    @Override
    public void onErrorResponseNewsCommentsStore(VolleyError volleyError) {
        dismissLoading();
        toast("خطایی هنگام ذخیره نظر رخ داد. مجدد امتحان کنید.");
    }

    @Override
    public void onResponseNewsLike(NewsLikeResponse newsLikeResponse) {
        dismissLoading();
        if (newsLikeResponse.getStatus() == 201) {
            news.setUserLike(null);
            news.setLikesCount(newsLikeResponse.getLikesCount());
            news.setDislikesCount(newsLikeResponse.getDislikesCount());
            news.setUserLike(newsLikeResponse.getUserLike());
        }
        updateNews();
    }

    @Override
    public void onErrorResponseNewsLike(VolleyError volleyError) {
        dismissLoading();
    }

    private void updateNews() {

        if (news.getFavorite() == null) {
            ImageViewCompat.setImageTintList(binding.imageViewFavorite, cslMutedText);
        } else {
            ImageViewCompat.setImageTintList(binding.imageViewFavorite, cslPrimary);
        }
        binding.linearShow.setVisibility(View.VISIBLE);
        binding.tvTitle.setText(String.format("%s", news.getTitle()));
        binding.tvDescription.setText(String.format("%s", news.getDescription()));
        binding.tvDisLikesCount.setText(String.format("%s", news.getDislikesCount()));
        binding.tvLikesCount.setText(String.format("%s", news.getLikesCount()));
        binding.tvCategory.setText(String.format("#%s", news.getCategory().getTitle()));
        binding.tvViews.setText(String.format("%s بازدید", news.getVisitsCount()));
        PersianCalendar calendar = new PersianCalendar();
        calendar.setPersianDate(Integer.parseInt(news.getCreatedAt().substring(0, 4)), Integer.parseInt(news.getCreatedAt().substring(5, 7)), Integer.parseInt(news.getCreatedAt().substring(8, 10)));
        binding.tvDate.setText(String.format("%s   %s", calendar.getPersianLongDate(), news.getCreatedAt().substring(11)));
        binding.tvCategory.setOnClickListener(view -> {
            Intent intent = new Intent(this, NewsByCategoryActivity.class);
            intent.putExtra("category_id", news.getCategory().getId());
            intent.putExtra("category_title", news.getCategory().getTitle());
            startActivity(intent);
        });
        if (news.getLink() != null) {
            binding.tvLink.setVisibility(View.VISIBLE);
        } else {
            binding.tvLink.setVisibility(View.GONE);
        }

        if (news.getUserLike() == null) {
            ColorStateList cslDark = AppCompatResources.getColorStateList(this, R.color.colorDark1);
            ImageViewCompat.setImageTintList(binding.imageViewLike, cslDark);
            ImageViewCompat.setImageTintList(binding.imageViewDislike, cslDark);
        } else {
            if (news.getUserLike() == 1) {
                ColorStateList csl = AppCompatResources.getColorStateList(this, R.color.colorPrimary);
                ImageViewCompat.setImageTintList(binding.imageViewLike, csl);
                ColorStateList cslDark = AppCompatResources.getColorStateList(this, R.color.colorDark1);
                ImageViewCompat.setImageTintList(binding.imageViewDislike, cslDark);
            } else {
                ColorStateList cslDark = AppCompatResources.getColorStateList(this, R.color.colorDark1);
                ImageViewCompat.setImageTintList(binding.imageViewLike, cslDark);
                ColorStateList csl = AppCompatResources.getColorStateList(this, R.color.colorDanger);
                ImageViewCompat.setImageTintList(binding.imageViewDislike, csl);
            }
        }

        binding.slider.setHideIndicators(false);
        binding.slider.setInterval(600000);
        binding.slider.setLoopSlides(false);

        List<Poster> posters = new ArrayList<>();
        List<NewsResponse.NewsData.News.NewsMedia> newsMedia = news.getNewsMedia();
        if (newsMedia.size() == 0) {
            binding.slider.setVisibility(View.GONE);
        } else {
            binding.slider.setVisibility(View.VISIBLE);
            for (NewsResponse.NewsData.News.NewsMedia media : newsMedia) {
                if (media.getType().name().equals(NewsResponse.NewsType.picture.name())) {
                    posters.add(new RemoteImage(BuildConfig.FILE_URL + media.getPath()));
                } else {
                    posters.add(new RemoteVideo(Uri.parse(BuildConfig.FILE_URL + media.getPath())));
                }
            }
            binding.slider.setPosters(posters);
        }

        binding.imageViewLike.setOnClickListener(view -> {
//            if (news.getUserLike() == null || news.getUserLike() == 1) {
                onLike(news.getId());
//            }
        });

        binding.imageViewDislike.setOnClickListener(view -> {
//            if (news.getUserLike() == null || news.getUserLike() == 0) {
                onDisLike(news.getId());
//            }
        });
        binding.imageViewShare.setOnClickListener(view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    String.format("%s \n %s", news.getTitle(), news.getDescription()));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        });
        binding.tvLink.setOnClickListener(view -> {
            if (news.getLink() != null) {
                IntentHelper intentHelper = new IntentHelper(NewsShowActivity.this);
                intentHelper.openSite(news.getLink());
            }
        });
        binding.tvCreateComment.setOnClickListener(view -> onCreateComment(news.getId()));
        binding.imageViewFavorite.setOnClickListener(view -> {
            presenter.requestNewsFavorite(news.getId(), getAppPreferencesHelper().getUserId());
        });

    }

    @Override
    public void onResponseUserNewsFavorite(UserNewsFavoriteResponse response) {
        dismissLoading();
        if (response.getFavorite() == null || response.getDeleted()) {
            ImageViewCompat.setImageTintList(binding.imageViewFavorite, cslMutedText);
        } else {
            ImageViewCompat.setImageTintList(binding.imageViewFavorite, cslPrimary);
        }
    }

    @Override
    public void onErrorResponseUserNewsFavorite(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی پیش آمد. مجدد امتحان کنید.");
    }


}