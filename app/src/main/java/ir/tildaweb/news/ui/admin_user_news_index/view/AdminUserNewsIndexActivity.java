package ir.tildaweb.news.ui.admin_user_news_index.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminUserNews;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityAdminNewsIndexBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.listeners.OnAdminNewsItemClickListener;
import ir.tildaweb.news.listeners.OnAdminUserNewsItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.admin_news_comments_index.view.AdminNewsCommentsIndexActivity;
import ir.tildaweb.news.ui.admin_news_create.view.AdminNewsCreateActivity;
import ir.tildaweb.news.ui.admin_news_edit.view.AdminNewsUpdateActivity;
import ir.tildaweb.news.ui.admin_user_news_edit.view.AdminUserNewsUpdateActivity;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsDeleteResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsRejectResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminNewsShowResponse;
import ir.tildaweb.news.ui.admin_user_news_index.presenter.AdminUserNewsIndexActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminUserNewsIndexActivity extends BaseActivity implements View.OnClickListener, AdminUserNewsIndexActivityPresenter.View, OnLoadMoreListener, OnAdminUserNewsItemClickListener {

    private String TAG = this.getClass().getName();
    private ActivityAdminNewsIndexBinding binding;
    private AdminUserNewsIndexActivityPresenter presenter;
    private AdapterAdminUserNews adapterNews;
    private int nextPage = 1;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminNewsIndexBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminUserNewsIndexActivityPresenter(this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterNews = new AdapterAdminUserNews(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterNews);

        binding.linearNewsCreate.setOnClickListener(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("مدیریت خبرهای مردمی");
        showLoadingFullPage();
        presenter.requestNewsIndex(nextPage);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.linearNewsCreate: {
                if (getAppPreferencesHelper().getAdminAccessNewsInsert() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
                    startActivityForResult(new Intent(AdminUserNewsIndexActivity.this, AdminNewsCreateActivity.class), 1);
                } else {
                    toast("شما دسترسی افزودن خبر ندارید.");
                }
                break;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data != null && data.hasExtra("user_news_id")) {
                int newsId = data.getIntExtra("user_news_id", -1);
                if (newsId > 0) {
                    showLoadingFullPage();
                    presenter.requestNewsShow(newsId);
                }
            }
        } else if (requestCode == 2) {
            adapterNews.deleteItem(data.getIntExtra("user_news_id", -1));
        }
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestNewsIndex(nextPage);
        }
    }

    @Override
    public void onResponseAdminNews(AdminUserNewsResponse newsResponse) {
        dismissLoading();
        if (newsResponse.getData().getNews().size() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
        adapterNews.addItems(newsResponse.getData().getNews());
        nextPage = VolleyRequestController.getNextPage(newsResponse.getData().getNextPageUrl());
    }

    @Override
    public void onErrorResponseAdminNews(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در دریافت اخبار به وجود آمد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseAdminNewsShow(AdminNewsShowResponse adminNewsShowResponse) {
        dismissLoading();
        adapterNews.addItem(adminNewsShowResponse.getNews());
        if (adapterNews.getItemCount() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponseAdminNewsShow(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت اطلاعات خبر وجود ندارد.");
    }

    @Override
    public void onResponseAdminNewsDelete(AdminUserNewsDeleteResponse adminUserNewsDeleteResponse) {
        dismissLoading();
        adapterNews.deleteItem(adminUserNewsDeleteResponse.getUserNewsId());
        if (adapterNews.getItemCount() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponseAdminNewsDelete(VolleyError volleyError) {
        dismissLoading();
        toast("حذف خبر با مشکل مواجه شد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseAdminNewsReject(AdminUserNewsRejectResponse response) {
        dismissLoading();
        adapterNews.rejectItem(response.getUserNewsId());
    }

    @Override
    public void onErrorResponseAdminNewsReject(VolleyError volleyError) {
        dismissLoading();
        toast("رد کردن خبر با مشکل مواجه شد. مجددا امتحان کنید.");
    }

    @Override
    public void onEdit(int id) {
        if (getAppPreferencesHelper().getAdminAccessUserNewsUpdate() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            Intent intent = new Intent(AdminUserNewsIndexActivity.this, AdminUserNewsUpdateActivity.class);
            intent.putExtra("user_news_id", id);
            startActivity(intent);
        } else {
            toast("شما دسترسی ویرایش خبر ندارید.");
        }
    }

    @Override
    public void onReject(int id) {
        String t = "رد کردن خبر";
        String description = "آیا میخواهید این خبر را رد کنید؟";
        DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(this, t, description);
        dialogConfirmMessage.setDanger();
        dialogConfirmMessage.setConfirmButtonText("رد کردن");
        dialogConfirmMessage.show();
        dialogConfirmMessage.setOnConfirmClickListener(view -> {
            dialogConfirmMessage.dismiss();
            showLoadingFullPage();
            presenter.requestNewsReject(id, getAppPreferencesHelper().getAdminId());
        });
    }

    @Override
    public void onDelete(int id, String title) {
        if (getAppPreferencesHelper().getAdminAccessUserNewsDelete() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            String t = "حذف خبر";
            String description = String.format("آیا میخواهید خبر با عنوان (%s) را حذف کنید؟", title);
            DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(this, t, description);
            dialogConfirmMessage.setDanger();
            dialogConfirmMessage.setConfirmButtonText("حذف");
            dialogConfirmMessage.show();
            dialogConfirmMessage.setOnConfirmClickListener(view -> {
                dialogConfirmMessage.dismiss();
                showLoadingFullPage();
                presenter.requestNewsDelete(id, getAppPreferencesHelper().getAdminId());
            });
        } else {
            toast("شما دسترسی حذف خبر ندارید.");
        }
    }
}