package ir.tildaweb.news.ui.admin_categories_index.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;

public class AdminCategoriesDeleteResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("category_id")
    private Integer categoryId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
