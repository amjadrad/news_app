package ir.tildaweb.news.ui.sigin.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.android.volley.VolleyError;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ActivitySiginBinding;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.sigin.model.SigninResponse;
import ir.tildaweb.news.ui.sigin.presenter.SignActivityPresenter;
import ir.tildaweb.news.ui.sigin_verification.view.SigninVerificationActivity;

public class SignActivity extends BaseActivity implements View.OnClickListener, SignActivityPresenter.View {

    private ActivitySiginBinding activitySiginBinding;
    private SignActivityPresenter signActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySiginBinding = ActivitySiginBinding.inflate(getLayoutInflater());
        setContentView(activitySiginBinding.getRoot());

        signActivityPresenter = new SignActivityPresenter(this);

        activitySiginBinding.btnNext.setOnClickListener(this);
        activitySiginBinding.etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 11) {
                    hideKeyboard(SignActivity.this, activitySiginBinding.etPhone);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnNext) {
            String phone = activitySiginBinding.etPhone.getText().toString().trim();
            if (phone.length() == 11 && phone.startsWith("09")) {
                showLoadingFullPage();
                signActivityPresenter.requestSignin(activitySiginBinding.etPhone.getText().toString());
            } else {
                toast("لطفا شماره همراه را به درستی وارد کنید.");
            }
        }
    }

    @Override
    public void onResponseSignIn(SigninResponse signinResponse) {
        dismissLoading();
        Intent intent = new Intent(SignActivity.this, SigninVerificationActivity.class);
        intent.putExtra("phone", activitySiginBinding.etPhone.getText().toString());
        intent.putExtra("is_admin", signinResponse.getAdmin());
        startActivity(intent);
        finish();
    }

    @Override
    public void onErrorResponseSignIn(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی پیش آمده لطفا مجددا امتحان کنید.");
    }
}