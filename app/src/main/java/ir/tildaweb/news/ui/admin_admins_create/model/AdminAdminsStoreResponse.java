package ir.tildaweb.news.ui.admin_admins_create.model;

import com.google.gson.annotations.SerializedName;

public class AdminAdminsStoreResponse {

    @SerializedName("status")
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
