package ir.tildaweb.news.ui.contact_us_list.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationResponse;

public class ContactUsListResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("data")
    private ContactsData data;


    public class ContactsData {
        @SerializedName("data")
        private ArrayList<Contact> contacts;
        @SerializedName("next_page_url")
        private String nextPageUrl;

        public class Contact {
            @SerializedName("id")
            private Integer id;
            @SerializedName("title")
            private String title;
            @SerializedName("description")
            private String description;
            @SerializedName("seen")
            private Integer seen;
            @SerializedName("user")
            private User user;

            public class User {
                @SerializedName("id")
                private Integer id;
                @SerializedName("full_name")
                private String fullName;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getFullName() {
                    return fullName;
                }

                public void setFullName(String fullName) {
                    this.fullName = fullName;
                }
            }

            public Integer getSeen() {
                return seen;
            }

            public void setSeen(Integer seen) {
                this.seen = seen;
            }

            public User getUser() {
                return user;
            }

            public void setUser(User user) {
                this.user = user;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }

        public ArrayList<Contact> getContacts() {
            return contacts;
        }

        public void setContacts(ArrayList<Contact> contacts) {
            this.contacts = contacts;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setData(ContactsData data) {
        this.data = data;
    }

    public ContactsData getData() {
        return data;
    }

    public Integer getStatus() {
        return status;
    }
}
