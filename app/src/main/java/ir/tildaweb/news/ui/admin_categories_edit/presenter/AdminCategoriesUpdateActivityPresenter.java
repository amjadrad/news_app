package ir.tildaweb.news.ui.admin_categories_edit.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateRequest;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateResponse;
import ir.tildaweb.news.ui.admin_news_create.model.AdminCategoriesShowRequest;
import ir.tildaweb.news.ui.admin_news_create.model.AdminCategoriesShowResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;


public class AdminCategoriesUpdateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminCategoriesUpdateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesShow(int categoryId) {
        String url = Endpoints.BASE_URL_ADMIN_CATEGORIES_SHOW;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminCategoriesShowResponse adminCategoriesShowResponse = DataParser.fromJson(response, AdminCategoriesShowResponse.class);
            view.onResponseAdminCategoriesShow(adminCategoriesShowResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesShow(error);
        });

        AdminCategoriesShowRequest adminCategoriesShowRequest = new AdminCategoriesShowRequest();
        adminCategoriesShowRequest.setCategoryId(categoryId);
        volleyRequestController.setParameters(adminCategoriesShowRequest);
        volleyRequestController.start();
    }


    public void requestCategoriesUpdate(AdminCategoriesUpdateRequest adminCategoriesUpdateRequest) {
        String url = Endpoints.BASE_URL_ADMIN_CATEGORIES_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminCategoriesUpdateResponse adminCategoriesUpdateResponse = DataParser.fromJson(response, AdminCategoriesUpdateResponse.class);
            view.onResponseAdminCategoriesUpdate(adminCategoriesUpdateResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesUpdate(error);
        });
        volleyRequestController.setBody(adminCategoriesUpdateRequest);
        volleyRequestController.start();
    }


    public interface View {
        void onResponseAdminCategoriesUpdate(AdminCategoriesUpdateResponse adminCategoriesUpdateResponse);

        void onErrorResponseAdminCategoriesUpdate(VolleyError volleyError);

        void onResponseAdminCategoriesShow(AdminCategoriesShowResponse adminCategoriesShowResponse);

        void onErrorResponseAdminCategoriesShow(VolleyError volleyError);

    }

}