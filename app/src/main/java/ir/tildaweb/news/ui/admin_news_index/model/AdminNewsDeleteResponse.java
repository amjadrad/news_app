package ir.tildaweb.news.ui.admin_news_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsDeleteResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("news_id")
    private Integer newsId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
