package ir.tildaweb.news.ui.app_comments_list.model;

import com.google.gson.annotations.SerializedName;

public class AppCommentsAverageRatingResponse {

    @SerializedName("average_rating")
    private Float averageRating;

    public Float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Float averageRating) {
        this.averageRating = averageRating;
    }
}
