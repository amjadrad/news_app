package ir.tildaweb.news.ui.fragments.fragment_home_profile.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.zcw.togglebutton.ToggleButton;


import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.FragmentMainProfileBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.ui.admin_admins_index.view.AdminAdminsIndexActivity;
import ir.tildaweb.news.ui.admin_categories_index.view.AdminCategoriesIndexActivity;
import ir.tildaweb.news.ui.admin_news_index.view.AdminNewsIndexActivity;
import ir.tildaweb.news.ui.base.BaseFragment;
import ir.tildaweb.news.ui.fragments.fragment_home_profile.presenter.ProfileFragmentPresenter;
import ir.tildaweb.news.ui.sigin.view.SignActivity;


public class ProfileFragment extends BaseFragment implements View.OnClickListener, ProfileFragmentPresenter.View {

    private FragmentMainProfileBinding binding;
    private String TAG = this.getClass().getName();

    private ProfileFragmentPresenter profileFragmentPresenter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMainProfileBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        profileFragmentPresenter = new ProfileFragmentPresenter(this);

        if (getAppPreferencesHelper().getUserFullNamePref() != null) {
            binding.tvFullname.setText(String.format("%s", getAppPreferencesHelper().getUserFullNamePref()));
        }
        if (getAppPreferencesHelper().getUserPhonePref() != null) {
            binding.tvPhone.setText(String.format("%s", getAppPreferencesHelper().getUserPhonePref()));
        }

        binding.toggleNotification.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
            }
        });

        if (getAppPreferencesHelper().getUserIsAdminPref()) {
            binding.linearManageContent.setVisibility(View.VISIBLE);
        } else {
            binding.linearManageContent.setVisibility(View.GONE);
        }
        if (getAppPreferencesHelper().getUserAdminTypePref() != null && getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            binding.linearManageAdmins.setVisibility(View.VISIBLE);
        } else {
            binding.linearManageAdmins.setVisibility(View.GONE);
        }

        binding.linearLogout.setOnClickListener(this);
        binding.linearAdmins.setOnClickListener(this);
        binding.linearCategories.setOnClickListener(this);
        binding.linearNews.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearAdmins: {
                startActivity(new Intent(getContext(), AdminAdminsIndexActivity.class));
                break;
            }
            case R.id.linearCategories: {
                startActivity(new Intent(getContext(), AdminCategoriesIndexActivity.class));
                break;
            }
            case R.id.linearNews: {
                startActivity(new Intent(getContext(), AdminNewsIndexActivity.class));
                break;
            }
            case R.id.linearLogout: {
                String title = "خروج از حساب کاربری";
                String description = "آیا میخواهید از حساب کاربری تان خارج شوید؟";
                DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(getContext(), title, description);
                dialogConfirmMessage.setConfirmButtonText("خروج");
                dialogConfirmMessage.setDanger();
                dialogConfirmMessage.setOnConfirmClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getAppPreferencesHelper().setLoginPref(false);
                        getAppPreferencesHelper().setUserAdminTypePref(null);
                        getAppPreferencesHelper().setUserIsAdminPref(false);
                        startActivity(new Intent(getContext(), SignActivity.class));
                        requireActivity().finish();
                    }
                });
                dialogConfirmMessage.show();
                break;
            }
        }
    }


}
