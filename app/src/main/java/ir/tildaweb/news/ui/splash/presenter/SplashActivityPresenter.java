package ir.tildaweb.news.ui.splash.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditResponse;
import ir.tildaweb.news.ui.splash.model.AdminAccessRequest;


public class SplashActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public SplashActivityPresenter(View view) {
        this.view = view;
    }

    public void requestAdminAccess(AdminAccessRequest request) {
        String url = Endpoints.BASE_URL_ADMIN_ADMIN_ACCESS_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminAccessEditResponse response1 = DataParser.fromJson(response, AdminAccessEditResponse.class);
            view.onResponseUserInfo(response1);
        }
                , error -> {
            view.onErrorResponseUserInfo(error);
        });
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseUserInfo(AdminAccessEditResponse response);

        void onErrorResponseUserInfo(VolleyError volleyError);

    }


}