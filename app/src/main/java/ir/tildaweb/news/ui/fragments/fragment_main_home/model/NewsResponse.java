package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;

public class NewsResponse {

    public enum NewsType {
        picture,
        video
    }

    @SerializedName("data")
    private NewsData data;

    public class NewsData {

        @SerializedName("data")
        private ArrayList<News> news;
        @SerializedName("next_page_url")
        private String nextPageUrl;

        public class News {

            @SerializedName("id")
            private Integer id;
            @SerializedName("title")
            private String title;
            @SerializedName("description")
            private String description;
            @SerializedName("link")
            private String link;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("likes_count")
            private Integer likesCount;
            @SerializedName("dislikes_count")
            private Integer dislikesCount;
            @SerializedName("visits_count")
            private Integer visitsCount;
            @SerializedName("medias")
            private ArrayList<NewsMedia> newsMedia;
            @SerializedName("comments_count")
            private Integer commentsCount;
            @SerializedName("category")
            private CategoriesResponse.Category category;
            @SerializedName("user_like")
            private Integer userLike;
            @SerializedName("active")
            private Integer active;
            @SerializedName("type")
            private String type;
            @SerializedName("favorite")
            private Favorite favorite;
            @SerializedName("comments_count_unseen")
            private Integer commentsCountUnSeen;
            @SerializedName("status")
            private String status;
            @SerializedName("user_visit")
            private Boolean isUserVisit;


            public Boolean getUserVisit() {
                return isUserVisit;
            }

            public void setUserVisit(Boolean userVisit) {
                isUserVisit = userVisit;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Integer getCommentsCountUnSeen() {
                return commentsCountUnSeen;
            }

            public void setCommentsCountUnSeen(Integer commentsCountUnSeen) {
                this.commentsCountUnSeen = commentsCountUnSeen;
            }

            public class Visits {
                @SerializedName("user_id")
                private Integer userId;

                public Integer getUserId() {
                    return userId;
                }

                public void setUserId(Integer userId) {
                    this.userId = userId;
                }
            }

            public class Favorite {
                @SerializedName("id")
                private Integer id;
                @SerializedName("user_id")
                private Integer userId;
                @SerializedName("news_id")
                private Integer newsId;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getUserId() {
                    return userId;
                }

                public void setUserId(Integer userId) {
                    this.userId = userId;
                }

                public Integer getNewsId() {
                    return newsId;
                }

                public void setNewsId(Integer newsId) {
                    this.newsId = newsId;
                }
            }

            public Favorite getFavorite() {
                return favorite;
            }

            public void setFavorite(Favorite favorite) {
                this.favorite = favorite;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public Integer getActive() {
                return active;
            }

            public void setActive(Integer active) {
                this.active = active;
            }

            public Integer getUserLike() {
                return userLike;
            }

            public void setUserLike(Integer userLike) {
                this.userLike = userLike;
            }

            public CategoriesResponse.Category getCategory() {
                return category;
            }

            public void setCategory(CategoriesResponse.Category category) {
                this.category = category;
            }

            public Integer getCommentsCount() {
                return commentsCount;
            }

            public void setCommentsCount(Integer commentsCount) {
                this.commentsCount = commentsCount;
            }

            public class NewsMedia {
                @SerializedName("id")
                private Integer id;
                @SerializedName("path")
                private String path;
                @SerializedName("type")
                private NewsType type;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getPath() {
                    return path;
                }

                public void setPath(String path) {
                    this.path = path;
                }

                public NewsType getType() {
                    return type;
                }

                public void setType(NewsType type) {
                    this.type = type;
                }
            }

            public ArrayList<NewsMedia> getNewsMedia() {
                return newsMedia;
            }

            public void setNewsMedia(ArrayList<NewsMedia> newsMedia) {
                this.newsMedia = newsMedia;
            }

            public Integer getDislikesCount() {
                return dislikesCount;
            }

            public void setDislikesCount(Integer dislikesCount) {
                this.dislikesCount = dislikesCount;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Integer getLikesCount() {
                return likesCount;
            }

            public void setLikesCount(Integer likesCount) {
                this.likesCount = likesCount;
            }

            public Integer getVisitsCount() {
                return visitsCount;
            }

            public void setVisitsCount(Integer visitsCount) {
                this.visitsCount = visitsCount;
            }
        }

        public ArrayList<News> getNews() {
            return news;
        }

        public void setNews(ArrayList<News> news) {
            this.news = news;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }
    }

    public NewsData getData() {
        return data;
    }

    public void setData(NewsData data) {
        this.data = data;
    }
}
