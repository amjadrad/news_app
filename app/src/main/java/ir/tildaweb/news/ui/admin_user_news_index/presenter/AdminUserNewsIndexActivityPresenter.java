package ir.tildaweb.news.ui.admin_user_news_index.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsDeleteRequest;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsDeleteResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsRejectRequest;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsRejectResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminNewsShowRequest;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminNewsShowResponse;
import ir.tildaweb.news.ui.admin_user_news_index.model.AdminUserNewsRequest;


public class AdminUserNewsIndexActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminUserNewsIndexActivityPresenter(View view) {
        this.view = view;
    }

    public void requestNewsIndex(int page) {

        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminUserNewsResponse newsResponse = DataParser.fromJson(response, AdminUserNewsResponse.class);
            view.onResponseAdminNews(newsResponse);
        }
                , error -> {
            view.onErrorResponseAdminNews(error);
        });
        AdminUserNewsRequest request = new AdminUserNewsRequest();
        request.setPage(page);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsShow(int newsId) {
        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_SHOW;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminNewsShowResponse adminNewsShowResponse = DataParser.fromJson(response, AdminNewsShowResponse.class);
            view.onResponseAdminNewsShow(adminNewsShowResponse);
        }
                , error -> {
            view.onErrorResponseAdminNewsShow(error);
        });

        AdminNewsShowRequest request = new AdminNewsShowRequest();
        request.setNewsId(newsId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsDelete(int newsId, int adminId) {
        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminUserNewsDeleteResponse result = DataParser.fromJson(response, AdminUserNewsDeleteResponse.class);
            view.onResponseAdminNewsDelete(result);
        }
                , error -> {
            view.onErrorResponseAdminNewsDelete(error);
        });

        AdminUserNewsDeleteRequest adminUserNewsDeleteRequest = new AdminUserNewsDeleteRequest();
        adminUserNewsDeleteRequest.setUserNewsId(newsId);
        adminUserNewsDeleteRequest.setAdminId(adminId);
        volleyRequestController.setBody(adminUserNewsDeleteRequest);
        volleyRequestController.start();
    }

    public void requestNewsReject(int userNewsId, int adminId) {
        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_REJECT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminUserNewsRejectResponse result = DataParser.fromJson(response, AdminUserNewsRejectResponse.class);
            view.onResponseAdminNewsReject(result);
        }
                , error -> {
            view.onErrorResponseAdminNewsReject(error);
        });

        AdminUserNewsRejectRequest request = new AdminUserNewsRejectRequest();
        request.setUserNewsId(userNewsId);
        request.setAdminId(adminId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseAdminNews(AdminUserNewsResponse adminUserNewsResponse);

        void onErrorResponseAdminNews(VolleyError volleyError);

        void onResponseAdminNewsShow(AdminNewsShowResponse adminNewsShowResponse);

        void onErrorResponseAdminNewsShow(VolleyError volleyError);

        void onResponseAdminNewsDelete(AdminUserNewsDeleteResponse adminUserNewsDeleteResponse);

        void onErrorResponseAdminNewsDelete(VolleyError volleyError);

        void onResponseAdminNewsReject(AdminUserNewsRejectResponse response);

        void onErrorResponseAdminNewsReject(VolleyError volleyError);


    }


}