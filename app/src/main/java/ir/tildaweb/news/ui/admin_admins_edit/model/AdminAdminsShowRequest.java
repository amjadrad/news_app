package ir.tildaweb.news.ui.admin_admins_edit.model;

import com.google.gson.annotations.SerializedName;

public class AdminAdminsShowRequest {

    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

}
