package ir.tildaweb.news.ui.contact_us_list.model;

import com.google.gson.annotations.SerializedName;

public class ContactUsListRequest {

    @SerializedName("page")
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
