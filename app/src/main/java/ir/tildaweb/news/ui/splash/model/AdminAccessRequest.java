package ir.tildaweb.news.ui.splash.model;

import com.google.gson.annotations.SerializedName;

public class AdminAccessRequest {

    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }
}
