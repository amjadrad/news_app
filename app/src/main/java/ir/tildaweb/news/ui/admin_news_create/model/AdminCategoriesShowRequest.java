package ir.tildaweb.news.ui.admin_news_create.model;

import com.google.gson.annotations.SerializedName;

public class AdminCategoriesShowRequest {

    @SerializedName("category_id")
    private Integer categoryId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
