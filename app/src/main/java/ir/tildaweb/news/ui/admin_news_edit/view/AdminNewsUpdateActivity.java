package ir.tildaweb.news.ui.admin_news_edit.view;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import java.io.File;
import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminNewsImages;
import ir.tildaweb.news.adapter.AdapterDropDown;
import ir.tildaweb.news.data.network.FileUploader;
import ir.tildaweb.news.databinding.ActivityAdminNewsEditBinding;
import ir.tildaweb.news.dialogs.DialogBottomSheetSelect;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreRequest;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsEditResponse;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsMediaDeleteResponse;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsUpdateRequest;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsUpdateResponse;
import ir.tildaweb.news.ui.admin_news_edit.presenter.AdminNewsUpdateActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.utils.FileUtils;
import ir.tildaweb.news.utils.TextUtils;
import ir.tildaweb.tilda_filepicker.TildaFilePicker;
import ir.tildaweb.tilda_filepicker.enums.FileMimeType;
import ir.tildaweb.tilda_filepicker.enums.FileType;
import ir.tildaweb.tilda_filepicker.models.FileModel;

public class AdminNewsUpdateActivity extends BaseActivity implements View.OnClickListener, ItemClickListener, AdminNewsUpdateActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminNewsEditBinding binding;
    private AdminNewsUpdateActivityPresenter presenter;
    private FileUploader fileUploader;
    private CategoriesResponse categoriesResponse;
    private Integer categoryId;
    private Integer PICK_FILE_PERMISSION_CODE = 1001;
    private Integer uploadFileRequestId = 0;
    private AdapterAdminNewsImages adapterAdminNewsImages;
    private ArrayList<AdminNewsStoreRequest.MediaModel> mediaModels;
    private Integer uploadedFilesCount = 0;
    private Integer newsId;
    private Integer active;
    private Integer hot;
    private ArrayList<DialogBottomSheetSelect.SelectObject> listSelectCategories;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminNewsEditBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        newsId = getIntent().getIntExtra("news_id", -1);
        presenter = new AdminNewsUpdateActivityPresenter(this);
        fileUploader = new FileUploader();
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.coordinatorSelectFile.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.coordinatorSelectCategory.setOnClickListener(this);
        listSelectCategories = new ArrayList<>();

        binding.toolbar.tvToolbarTitle.setText("ویرایش خبر");
        adapterAdminNewsImages = new AdapterAdminNewsImages(this, new ArrayList<>(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerViewImages.setLayoutManager(linearLayoutManager);
        binding.recyclerViewImages.setAdapter(adapterAdminNewsImages);

        binding.toggleActive.setOnToggleChanged(on -> {
            active = on ? 1 : 0;
        });
        binding.toggleHot.setOnToggleChanged(on -> {
            hot = on ? 1 : 0;
        });

        showLoadingFullPage();
        presenter.requestNewsEdit(newsId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.coordinatorSelectFile: {
                if (checkReadExternalPermission(AdminNewsUpdateActivity.this, PICK_FILE_PERMISSION_CODE)) {
                    TildaFilePicker tildaFilePicker = new TildaFilePicker(AdminNewsUpdateActivity.this, new FileType[]{FileType.FILE_TYPE_IMAGE});
                    tildaFilePicker.setOnTildaFileSelectListener(list -> {
                        for (FileModel model : list) {
                            if (model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_IMAGE || model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_VIDEO) {
                                adapterAdminNewsImages.addItem(model);
                                binding.recyclerViewImages.scrollToPosition(0);
                            } else {
                                toast("فقط مجاز به انتخاب عکس و ویدیو هستید.");
                            }
                        }
                    });
                    tildaFilePicker.show(getSupportFragmentManager());
                }
                break;
            }
            case R.id.btnConfirm: {
                hideKeyboard(AdminNewsUpdateActivity.this, binding.etTitle);
                hideKeyboard(AdminNewsUpdateActivity.this, binding.etDescription);
                hideKeyboard(AdminNewsUpdateActivity.this, binding.etLink);
                if (checkValidation()) {
                    showLoadingFullPage();
                    if (adapterAdminNewsImages.getList().size() > 0) {
                        uploadedFilesCount = 0;
                        mediaModels = new ArrayList<>();
                        toast("بر اساس حجم عکس و فیلم ها ممکن است مدتی طول بکشد...");
                        for (FileModel model : adapterAdminNewsImages.getList()) {
                            if (model.getId() == null) {
                                uploadFileRequestId++;
                                AdminNewsStoreRequest.MediaModel mediaModel = new AdminNewsStoreRequest().new MediaModel();
                                mediaModel.setId(uploadFileRequestId);
                                mediaModel.setType(model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_IMAGE ? "picture" : "video");
                                mediaModels.add(mediaModel);

                                fileUploader.upload(model.getPath(), uploadFileRequestId, new FileUploader.OnFileUploaderListener() {
                                    @Override
                                    public void onFileUploadProgress(int requestId, int percent) {
                                    }

                                    @Override
                                    public void onFileUploaded(String fileName, int requestId) {
                                        uploadedFilesCount++;
                                        if (fileName != null) {
                                            for (AdminNewsStoreRequest.MediaModel mediaModel : mediaModels) {
                                                if (mediaModel.getId() == requestId) {
                                                    mediaModel.setPath(fileName);
                                                    break;
                                                }
                                            }
                                        }
                                        if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                            sendFinalStoreRequest();
                                        }
                                    }

                                    @Override
                                    public void onFileUploadError(String error) {
                                        toast("امکان ارسال فایل وجود ندارد.");
                                        uploadedFilesCount++;
                                        if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                            sendFinalStoreRequest();
                                        }
                                    }
                                });
                            } else {
                                uploadedFilesCount++;
                                if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                    sendFinalStoreRequest();
                                }
                            }
                        }
                    } else {
                        sendFinalStoreRequest();
                    }
                }
                break;
            }
            case R.id.coordinatorSelectCategory: {
                String title = "دسته بندی مورد نظر را انتخاب کنید.";
                String searchHint = "جستجو دسته بندی:";
                DialogBottomSheetSelect dialog = new DialogBottomSheetSelect(title, searchHint, listSelectCategories);
                dialog.setClickListener(id -> {
                    for (CategoriesResponse.Category item : categoriesResponse.getCategories())
                        if (item.getId() == id) {
                            categoryId = id;
                            binding.tvSelectedCategoryName.setText(String.valueOf(item.getTitle()));
                            dialog.dismiss();
                            break;
                        }
                });
                dialog.show(getSupportFragmentManager(), null);
                break;
            }
        }
    }

    private void sendFinalStoreRequest() {
        AdminNewsUpdateRequest adminNewsUpdateRequest = new AdminNewsUpdateRequest();
        adminNewsUpdateRequest.setActive(active);
        adminNewsUpdateRequest.setHot(hot);
        adminNewsUpdateRequest.setNewsId(newsId);
        adminNewsUpdateRequest.setTitle(binding.etTitle.getText().toString().trim());
        adminNewsUpdateRequest.setCategoryId(categoryId);
        adminNewsUpdateRequest.setAdminId(getAppPreferencesHelper().getAdminId());
        adminNewsUpdateRequest.setDescription(binding.etDescription.getText().toString().trim());

        String link = binding.etLink.getText().toString().trim();
        if (link.length() > 0) {
            if (!link.startsWith("https") || !link.startsWith("http")) {
                link = "https://" + link;
            }
            adminNewsUpdateRequest.setLink(link);
        }
        adminNewsUpdateRequest.setMedia(mediaModels);
        adminNewsUpdateRequest.setAdminId(getAppPreferencesHelper().getAdminId());
        presenter.requestNewsUpdate(adminNewsUpdateRequest);
    }

    @Override
    public void onResponseAdminCategoriesIndex(CategoriesResponse categoriesResponse) {
        dismissLoading();
        this.categoriesResponse = categoriesResponse;
        listSelectCategories.clear();
        Drawable icon = ContextCompat.getDrawable(this, R.drawable.ic_list);
        for (CategoriesResponse.Category item : categoriesResponse.getCategories()) {
            DialogBottomSheetSelect.SelectObject selectObject = new DialogBottomSheetSelect.SelectObject();
            selectObject.setTitle(item.getTitle());
            selectObject.setId(item.getId());
            selectObject.setIcon(icon);
            selectObject.setSelected(false);
            if (categoryId != null) {
                if (categoryId.intValue() == item.getId()) {
                    selectObject.setSelected(true);
                    categoryId = item.getId();
                    binding.tvSelectedCategoryName.setText(String.valueOf(item.getTitle()));
                }
            }
            listSelectCategories.add(selectObject);
        }
    }

    @Override
    public void onErrorResponseAdminCategoriesIndex(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت دسته بندی ها وجود ندارد.");
    }

    @Override
    public void onResponseAdminNewsEdit(AdminNewsEditResponse adminNewsEditResponse) {
        dismissLoading();

        binding.etTitle.setText(adminNewsEditResponse.getNews().getTitle());
        binding.etDescription.setText(adminNewsEditResponse.getNews().getDescription());
        binding.etLink.setText(adminNewsEditResponse.getNews().getLink());
        categoryId = adminNewsEditResponse.getNews().getCategory().getId();
        active = adminNewsEditResponse.getNews().getActive();
        hot = adminNewsEditResponse.getNews().getType().equals("hot") ? 1 : 0;
        presenter.requestCategoriesIndex();
        if (adminNewsEditResponse.getNews().getActive() == 1) {
            binding.toggleActive.setToggleOn();
        } else {
            binding.toggleActive.setToggleOff();
        }
        if (adminNewsEditResponse.getNews().getType().equals("hot")) {
            binding.toggleHot.setToggleOn();
        } else {
            binding.toggleHot.setToggleOff();
        }
        for (NewsResponse.NewsData.News.NewsMedia newsMedia : adminNewsEditResponse.getNews().getNewsMedia()) {
            FileModel model = new FileModel();
            if (newsMedia.getType() == NewsResponse.NewsType.picture) {
                model.setFileMimeType(FileMimeType.FILE_MIME_TYPE_IMAGE);
            } else {
                model.setFileMimeType(FileMimeType.FILE_MIME_TYPE_VIDEO);
            }
            model.setPath(newsMedia.getPath());
            model.setId(newsMedia.getId());
            adapterAdminNewsImages.addItem(model);
        }

    }

    @Override
    public void onErrorResponseAdminNewsEdit(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت اطلاعات خبر وجود ندارد.");
    }

    @Override
    public void onResponseAdminNewsUpdate(AdminNewsUpdateResponse adminNewsUpdateResponse) {
        dismissLoading();
        toast("خبر با موفقیت ویرایش و منتشر شد.");
        Intent intent = new Intent();
        intent.putExtra("news_id", adminNewsUpdateResponse.getNewsId());
        setResult(2, intent);
        finish();
    }

    @Override
    public void onErrorResponseAdminNewsUpdate(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ویرایش خبر به وجود آمد.");
    }

    @Override
    public void onResponseAdminNewsMediaDelete(AdminNewsMediaDeleteResponse adminNewsMediaDeleteResponse) {
        dismissLoading();
        adapterAdminNewsImages.delete(adminNewsMediaDeleteResponse.getMediaId());
    }

    @Override
    public void onErrorResponseAdminNewsMediaDelete(VolleyError volleyError) {
        dismissLoading();
        toast("حذف عکس/فیلم با مشکل مواجه شد.");
    }


    private boolean checkValidation() {
        if (binding.etTitle.getText().toString().trim().length() == 0) {
            toast("لطفا عنوان خبر را وارد کنید.");
            return false;
        } else if (binding.etDescription.getText().toString().trim().length() == 0) {
            toast("لطفا توضیحات خبر را وارد کنید.");
            return false;
        } else if (binding.etLink.getText().toString().trim().length() > 0 && !TextUtils.isWebsite(binding.etLink.getText().toString())) {
            toast("لطفا لینک خبر را به درستی وارد کنید. مثال: https://example.com");
            return false;
        }
//        else if (adapterAdminNewsImages.getList().size() == 0) {
//            toast("لطفا حداقل یک تصویر برای خبر انتخاب کنید.");
//            return false;
//        }
        else if (categoryId == null) {
            toast("لطفا دسته بندی را انتخاب کنید.");
            return false;
        }
        return true;
    }


    @Override
    public void onEdit(int id) {

    }

    @Override
    public void onDelete(int id, String title) {
        presenter.requestNewsMediaDelete(id);
    }


}