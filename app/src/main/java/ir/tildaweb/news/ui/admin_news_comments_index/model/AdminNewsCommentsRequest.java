package ir.tildaweb.news.ui.admin_news_comments_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsCommentsRequest {

    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("page")
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}