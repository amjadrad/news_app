package ir.tildaweb.news.ui.settings.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.android.volley.VolleyError;
import com.zcw.togglebutton.ToggleButton;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ActivityProfileUpdateBinding;
import ir.tildaweb.news.databinding.ActivitySettingsBinding;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.profile_edit.model.ContactUsStoreResponse;
import ir.tildaweb.news.ui.profile_edit.presenter.ProfileEditActivityPresenter;
import ir.tildaweb.news.ui.splash.view.SplashActivity;
import ir.tildaweb.news.utils.FontUtils;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {

    private String TAG = this.getClass().getName();
    private ActivitySettingsBinding binding;
    private int font;
    private boolean isShowGallery;
    private float textSizeStandard;
    private float factor;
    private float oldFactor;
    private int oldTheme;
    private int newTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        textSizeStandard = FontUtils.spToPx(this, 12);
        binding.toolbar.tvToolbarTitle.setText(String.format("%s", "تنظیمات"));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.radioIranYekan.setTypeface(FontUtils.getFont(this));
        binding.radioKalameh.setTypeface(FontUtils.getFontKalameh(this));
        font = getAppPreferencesHelper().getFontPref();
        factor = getAppPreferencesHelper().getTextSizePref();
        oldFactor = factor;
        isShowGallery = getAppPreferencesHelper().getIsShowNewsGalleryPref();

        binding.linearThemeMain.setOnClickListener(this);
        binding.linearTheme1.setOnClickListener(this);
        binding.linearTheme2.setOnClickListener(this);
        binding.linearTheme3.setOnClickListener(this);
        binding.linearTheme4.setOnClickListener(this);
        oldTheme = getAppPreferencesHelper().getThemePref();
        newTheme = oldTheme;
        changeTheme(oldTheme);
        if (isShowGallery) {
            binding.toggleShowNewsGallery.setToggleOn();
        } else {
            binding.toggleShowNewsGallery.setToggleOff();
        }

        binding.radioGroupFont.setOnCheckedChangeListener((radioGroup, i) -> {
            if (i == binding.radioIranYekan.getId()) {
                font = 1;
            } else {
                font = 2;
            }
        });
        binding.toggleShowNewsGallery.setOnToggleChanged(on -> isShowGallery = on);
        if (font == 1) {
            binding.radioIranYekan.setChecked(true);
        } else {
            binding.radioKalameh.setChecked(true);
        }

        int textSize = (int) (getAppPreferencesHelper().getTextSizePref() * 50);
        binding.seekBarTextSize.setProgress(textSize);
        binding.tvStandard.setText(String.format("اندازه متن: %d", textSize));
        binding.seekBarTextSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i > 0) {
                    factor = i / 50f;
                    binding.tvTestTextSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeStandard * factor);
                    binding.tvStandard.setText(String.format("اندازه متن: %d", i));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void changeTheme(int theme) {
        if (theme != oldTheme) {
            newTheme = theme;
        }
        switch (theme) {
            case 0: {
                binding.linearThemeMain.setBackground(getResources().getDrawable(R.drawable.bg_primary_rounded_outline));
                binding.linearTheme1.setBackground(null);
                binding.linearTheme2.setBackground(null);
                binding.linearTheme3.setBackground(null);
                binding.linearTheme4.setBackground(null);

                binding.tvThemeMain.setTextColor(getResources().getColor(R.color.colorPrimary));
                binding.tvTheme1.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme2.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme3.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme4.setTextColor(getResources().getColor(R.color.colorText));
                break;
            }
            case 1: {
                binding.linearThemeMain.setBackground(null);
                binding.linearTheme1.setBackground(getResources().getDrawable(R.drawable.bg_primary_rounded_outline));
                binding.linearTheme2.setBackground(null);
                binding.linearTheme3.setBackground(null);
                binding.linearTheme4.setBackground(null);

                binding.tvThemeMain.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme1.setTextColor(getResources().getColor(R.color.colorPrimary_theme_1));
                binding.tvTheme2.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme3.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme4.setTextColor(getResources().getColor(R.color.colorText));
                break;
            }
            case 2: {
                binding.linearThemeMain.setBackground(null);
                binding.linearTheme1.setBackground(null);
                binding.linearTheme2.setBackground(getResources().getDrawable(R.drawable.bg_primary_rounded_outline));
                binding.linearTheme3.setBackground(null);
                binding.linearTheme4.setBackground(null);

                binding.tvThemeMain.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme1.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme2.setTextColor(getResources().getColor(R.color.colorPrimary_theme_2));
                binding.tvTheme3.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme4.setTextColor(getResources().getColor(R.color.colorText));
                break;
            }
            case 3: {
                binding.linearThemeMain.setBackground(null);
                binding.linearTheme1.setBackground(null);
                binding.linearTheme2.setBackground(null);
                binding.linearTheme3.setBackground(getResources().getDrawable(R.drawable.bg_primary_rounded_outline));
                binding.linearTheme4.setBackground(null);

                binding.tvThemeMain.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme1.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme2.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme3.setTextColor(getResources().getColor(R.color.colorPrimary_theme_3));
                binding.tvTheme4.setTextColor(getResources().getColor(R.color.colorText));
                break;
            }
            case 4: {
                binding.linearThemeMain.setBackground(null);
                binding.linearTheme1.setBackground(null);
                binding.linearTheme2.setBackground(null);
                binding.linearTheme3.setBackground(null);
                binding.linearTheme4.setBackground(getResources().getDrawable(R.drawable.bg_primary_rounded_outline));

                binding.tvThemeMain.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme1.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme2.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme3.setTextColor(getResources().getColor(R.color.colorText));
                binding.tvTheme4.setTextColor(getResources().getColor(R.color.colorPrimary_theme_4));
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        } else if (view.getId() == binding.btnConfirm.getId()) {
            getAppPreferencesHelper().setIsShowNewsGalleryPref(isShowGallery);
            getAppPreferencesHelper().setTextSizePref(factor);
            if (getAppPreferencesHelper().getFontPref() != font
                    || oldFactor != factor || oldTheme != newTheme) {
                getAppPreferencesHelper().setThemePref(newTheme);
                getAppPreferencesHelper().setFontPref(font);
                themeSettings();
                startActivity(new Intent(SettingsActivity.this, SplashActivity.class));
                finishAffinity();
            } else {
                finish();
            }
        } else if (view.getId() == binding.linearThemeMain.getId()) {
            changeTheme(0);
        } else if (view.getId() == binding.linearTheme1.getId()) {
            changeTheme(1);
        } else if (view.getId() == binding.linearTheme2.getId()) {
            changeTheme(2);
        } else if (view.getId() == binding.linearTheme3.getId()) {
            changeTheme(3);
        } else if (view.getId() == binding.linearTheme4.getId()) {
            changeTheme(4);
        }
    }

}