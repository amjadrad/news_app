package ir.tildaweb.news.ui.contact_us.model;

import com.google.gson.annotations.SerializedName;

public class ContactUsStoreResponse {

    @SerializedName("status")
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}