package ir.tildaweb.news.ui.admin_admins_edit.model;

import com.google.gson.annotations.SerializedName;

public class AdminAdminsUpdateRequest {

    @SerializedName("admin_id")
    private Integer adminId;
    @SerializedName("full_name")
    private String name;


    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
