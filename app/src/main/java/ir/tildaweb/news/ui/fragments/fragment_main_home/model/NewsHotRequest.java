package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;


public class NewsHotRequest {

    public NewsHotRequest() {
        this.withMedia = true;
    }

    @SerializedName("with_media")
    private Boolean withMedia;

    public Boolean getWithMedia() {
        return withMedia;
    }

    public void setWithMedia(Boolean withMedia) {
        this.withMedia = withMedia;
    }


}
