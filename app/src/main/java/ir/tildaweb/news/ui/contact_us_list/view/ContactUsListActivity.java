package ir.tildaweb.news.ui.contact_us_list.view;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.adapter.AdapterAdminContacts;
import ir.tildaweb.news.adapter.AdapterAdminNews;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityContactUsListBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.listeners.OnContactItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.contact_us_list.model.ContactUsListResponse;
import ir.tildaweb.news.ui.contact_us_list.model.ContactsSeenResponse;
import ir.tildaweb.news.ui.contact_us_list.presenter.ContactUsListActivityPresenter;

public class ContactUsListActivity extends BaseActivity implements View.OnClickListener, ContactUsListActivityPresenter.View, OnLoadMoreListener, OnContactItemClickListener {

    private ActivityContactUsListBinding binding;
    private ContactUsListActivityPresenter presenter;
    private AdapterAdminContacts adapterAdminContacts;
    private int nextPage = 1;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityContactUsListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tvToolbarTitle.setText(String.format("%s", "مدیریت پیام های کاربران"));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        presenter = new ContactUsListActivityPresenter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterAdminContacts = new AdapterAdminContacts(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterAdminContacts);

        showLoadingFullPage();
        presenter.requestContactUsList(nextPage);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        }
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestContactUsList(nextPage);
        }
    }

    @Override
    public void onResponseContactUsList(ContactUsListResponse response) {
        dismissLoading();
        if (response.getData().getContacts().size() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
        adapterAdminContacts.addItems(response.getData().getContacts());
        nextPage = VolleyRequestController.getNextPage(response.getData().getNextPageUrl());
    }

    @Override
    public void onErrorResponseContactUsList(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت لیست پیام ها وجود ندارد.");
    }

    @Override
    public void onResponseContactsSeen(ContactsSeenResponse response) {
        adapterAdminContacts.updateSeen(response.getContactId());
    }

    @Override
    public void onErrorResponseContactsSeen(VolleyError volleyError) {
        //error
    }

    @Override
    public void onResponseContactsDelete(ContactsSeenResponse response) {
        dismissLoading();
        toast("با موفقیت حذف شد.");
        adapterAdminContacts.deleteItem(response.getContactId());
    }

    @Override
    public void onErrorResponseContactsDelete(VolleyError volleyError) {
        dismissLoading();
        toast("امکان حذف پیام وجود ندارد. مجدد امتحان کنید.");
    }

    @Override
    public void onShow(int seen, int id, String title, String description) {
        if (seen == 0) {
            presenter.requestContactsSeen(id);
        }
        new DialogMessage(ContactUsListActivity.this, title, description).show();
    }

    @Override
    public void onDelete(int id, String title) {
        DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(ContactUsListActivity.this, "آیا میخواهید این پیام را حذف کنید؟", "");
        dialogConfirmMessage.setOnConfirmClickListener(view -> {
            dialogConfirmMessage.dismiss();
            showLoadingFullPage();
            presenter.requestContactsDelete(id);
        });
        dialogConfirmMessage.setDanger();
        dialogConfirmMessage.setConfirmButtonText("حذف");
        dialogConfirmMessage.show();
    }
}