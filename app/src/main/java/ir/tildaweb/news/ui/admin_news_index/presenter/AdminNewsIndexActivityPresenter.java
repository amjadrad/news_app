package ir.tildaweb.news.ui.admin_news_index.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsDeleteRequest;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsDeleteResponse;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsRequest;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsResponse;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsShowRequest;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsShowResponse;


public class AdminNewsIndexActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminNewsIndexActivityPresenter(View view) {
        this.view = view;
    }


    public void requestNewsSearch(int page, String search, Integer categoryId, int adminId) {

        String url = Endpoints.BASE_URL_ADMIN_NEWS_SEARCH;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminNewsResponse newsResponse = DataParser.fromJson(response, AdminNewsResponse.class);
            view.onResponseAdminNews(newsResponse);
        }
                , error -> {
            view.onErrorResponseAdminNews(error);
        });

        AdminNewsRequest newsRequest = new AdminNewsRequest();
        newsRequest.setCategoryId(categoryId);
        newsRequest.setAdminId(adminId);
        newsRequest.setPage(page);
        newsRequest.setSearch(search);
        volleyRequestController.setParameters(newsRequest);
        volleyRequestController.start();
    }

    public void requestNewsShow(int newsId) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_SHOW;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminNewsShowResponse adminNewsShowResponse = DataParser.fromJson(response, AdminNewsShowResponse.class);
            view.onResponseAdminNewsShow(adminNewsShowResponse);
        }
                , error -> {
            view.onErrorResponseAdminNewsShow(error);
        });

        AdminNewsShowRequest request = new AdminNewsShowRequest();
        request.setNewsId(newsId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsDelete(int newsId , int adminId) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsDeleteResponse result = DataParser.fromJson(response, AdminNewsDeleteResponse.class);
            view.onResponseAdminNewsDelete(result);
        }
                , error -> {
            view.onErrorResponseAdminNewsDelete(error);
        });

        AdminNewsDeleteRequest adminNewsDeleteRequest = new AdminNewsDeleteRequest();
        adminNewsDeleteRequest.setNewsId(newsId);
        adminNewsDeleteRequest.setAdminId(adminId);
        volleyRequestController.setParameters(adminNewsDeleteRequest);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseAdminNews(AdminNewsResponse adminNewsResponse);

        void onErrorResponseAdminNews(VolleyError volleyError);

        void onResponseAdminNewsShow(AdminNewsShowResponse adminNewsShowResponse);

        void onErrorResponseAdminNewsShow(VolleyError volleyError);

        void onResponseAdminNewsDelete(AdminNewsDeleteResponse adminNewsDeleteResponse);

        void onErrorResponseAdminNewsDelete(VolleyError volleyError);


    }


}