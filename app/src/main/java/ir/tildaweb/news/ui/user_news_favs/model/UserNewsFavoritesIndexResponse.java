package ir.tildaweb.news.ui.user_news_favs.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;

public class UserNewsFavoritesIndexResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("user_news_favorites")
    private UserNewsFavoritesData userNewsFavoritesData;

    public class UserNewsFavoritesData {
        @SerializedName("next_page_url")
        private String nextPageUrl;
        @SerializedName("data")
        private ArrayList<UserNewsFavorite> userNewsFavorites;

        public class UserNewsFavorite {

            @SerializedName("id")
            private Integer id;
            @SerializedName("user_id")
            private Integer userId;
            @SerializedName("news")
            private NewsResponse.NewsData.News news;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public NewsResponse.NewsData.News getNews() {
                return news;
            }

            public void setNews(NewsResponse.NewsData.News news) {
                this.news = news;
            }
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }

        public ArrayList<UserNewsFavorite> getUserNewsFavorites() {
            return userNewsFavorites;
        }

        public void setUserNewsFavorites(ArrayList<UserNewsFavorite> userNewsFavorites) {
            this.userNewsFavorites = userNewsFavorites;
        }

    }

    public UserNewsFavoritesData getUserNewsFavoritesData() {
        return userNewsFavoritesData;
    }

    public void setUserNewsFavoritesData(UserNewsFavoritesData userNewsFavoritesData) {
        this.userNewsFavoritesData = userNewsFavoritesData;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
