package ir.tildaweb.news.ui.admin_settings.model;

import com.google.gson.annotations.SerializedName;

public class AdminSettingsEditRequest {
    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }
}
