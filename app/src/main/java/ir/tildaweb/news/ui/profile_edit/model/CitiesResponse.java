package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CitiesResponse {

    @SerializedName("cities")
    private ArrayList<City> cities;

    public class City {
        @SerializedName("id")
        private Integer id;
        @SerializedName("province_id")
        private Integer provinceId;
        @SerializedName("province")
        private ProvincesResponse.Province province;
        @SerializedName("title")
        private String title;

        public ProvincesResponse.Province getProvince() {
            return province;
        }

        public void setProvince(ProvincesResponse.Province province) {
            this.province = province;
        }

        public Integer getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(Integer provinceId) {
            this.provinceId = provinceId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public ArrayList<City> getCities() {
        return cities;
    }

    public void setCities(ArrayList<City> cities) {
        this.cities = cities;
    }
}
