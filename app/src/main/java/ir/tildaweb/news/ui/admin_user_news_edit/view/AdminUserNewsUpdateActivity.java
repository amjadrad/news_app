package ir.tildaweb.news.ui.admin_user_news_edit.view;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminNewsImages;
import ir.tildaweb.news.data.network.FileUploader;
import ir.tildaweb.news.databinding.ActivityAdminUserNewsEditBinding;
import ir.tildaweb.news.dialogs.DialogBottomSheetSelect;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreRequest;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsEditResponse;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsMediaDeleteResponse;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminUserNewsUpdateRequest;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsUpdateResponse;
import ir.tildaweb.news.ui.admin_user_news_edit.presenter.AdminUserNewsUpdateActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.utils.TextUtils;
import ir.tildaweb.tilda_filepicker.TildaFilePicker;
import ir.tildaweb.tilda_filepicker.enums.FileMimeType;
import ir.tildaweb.tilda_filepicker.enums.FileType;
import ir.tildaweb.tilda_filepicker.models.FileModel;

public class AdminUserNewsUpdateActivity extends BaseActivity implements View.OnClickListener, ItemClickListener, AdminUserNewsUpdateActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminUserNewsEditBinding binding;
    private AdminUserNewsUpdateActivityPresenter presenter;
    private FileUploader fileUploader;
    private CategoriesResponse categoriesResponse;
    private Integer categoryId;
    private Integer PICK_FILE_PERMISSION_CODE = 1001;
    private Integer uploadFileRequestId = 0;
    private AdapterAdminNewsImages adapterAdminNewsImages;
    private ArrayList<AdminNewsStoreRequest.MediaModel> mediaModels;
    private Integer uploadedFilesCount = 0;
    private Integer userNewsId;
    private Integer active = 1;
    private Integer hot = 0;
    private ArrayList<DialogBottomSheetSelect.SelectObject> listSelectCategories;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminUserNewsEditBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        userNewsId = getIntent().getIntExtra("user_news_id", -1);
        presenter = new AdminUserNewsUpdateActivityPresenter(this);
        fileUploader = new FileUploader();
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.coordinatorSelectFile.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.coordinatorSelectCategory.setOnClickListener(this);
        listSelectCategories = new ArrayList<>();

        binding.toolbar.tvToolbarTitle.setText("ویرایش خبر");
        adapterAdminNewsImages = new AdapterAdminNewsImages(this, new ArrayList<>(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerViewImages.setLayoutManager(linearLayoutManager);
        binding.recyclerViewImages.setAdapter(adapterAdminNewsImages);

        binding.toggleActive.setOnToggleChanged(on -> {
            active = on ? 1 : 0;
        });
        binding.toggleHot.setOnToggleChanged(on -> {
            hot = on ? 1 : 0;
        });

        showLoadingFullPage();
        presenter.requestNewsEdit(userNewsId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.coordinatorSelectFile: {
                if (checkReadExternalPermission(AdminUserNewsUpdateActivity.this, PICK_FILE_PERMISSION_CODE)) {
                    TildaFilePicker tildaFilePicker = new TildaFilePicker(AdminUserNewsUpdateActivity.this, new FileType[]{FileType.FILE_TYPE_IMAGE});
                    tildaFilePicker.setOnTildaFileSelectListener(list -> {
                        for (FileModel model : list) {
                            if (model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_IMAGE || model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_VIDEO) {
                                adapterAdminNewsImages.addItem(model);
                                binding.recyclerViewImages.scrollToPosition(0);
                            } else {
                                toast("فقط مجاز به انتخاب عکس و ویدیو هستید.");
                            }
                        }
                    });
                    tildaFilePicker.show(getSupportFragmentManager());
                }
                break;
            }
            case R.id.btnConfirm: {
                hideKeyboard(AdminUserNewsUpdateActivity.this, binding.etTitle);
                hideKeyboard(AdminUserNewsUpdateActivity.this, binding.etDescription);
                hideKeyboard(AdminUserNewsUpdateActivity.this, binding.etLink);
                if (checkValidation()) {
                    showLoadingFullPage();
                    if (adapterAdminNewsImages.getList().size() > 0) {
                        uploadedFilesCount = 0;
                        mediaModels = new ArrayList<>();
                        toast("بر اساس حجم عکس و فیلم ها ممکن است مدتی طول بکشد...");
                        for (FileModel model : adapterAdminNewsImages.getList()) {
                            if (model.getId() == null) {
                                uploadFileRequestId++;
                                AdminNewsStoreRequest.MediaModel mediaModel = new AdminNewsStoreRequest().new MediaModel();
                                mediaModel.setId(uploadFileRequestId);
                                mediaModel.setType(model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_IMAGE ? "picture" : "video");
                                mediaModels.add(mediaModel);

                                fileUploader.upload(model.getPath(), uploadFileRequestId, new FileUploader.OnFileUploaderListener() {
                                    @Override
                                    public void onFileUploadProgress(int requestId, int percent) {
                                    }

                                    @Override
                                    public void onFileUploaded(String fileName, int requestId) {
                                        uploadedFilesCount++;
                                        if (fileName != null) {
                                            for (AdminNewsStoreRequest.MediaModel mediaModel : mediaModels) {
                                                if (mediaModel.getId() == requestId) {
                                                    mediaModel.setPath(fileName);
                                                    break;
                                                }
                                            }
                                        }
                                        if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                            sendFinalStoreRequest();
                                        }
                                    }

                                    @Override
                                    public void onFileUploadError(String error) {
                                        toast("امکان ارسال فایل وجود ندارد.");
                                        uploadedFilesCount++;
                                        if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                            sendFinalStoreRequest();
                                        }
                                    }
                                });
                            } else {
                                uploadedFilesCount++;
                                if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                    sendFinalStoreRequest();
                                }
                            }
                        }
                    } else {
                        sendFinalStoreRequest();
                    }
                }
                break;
            }
            case R.id.coordinatorSelectCategory: {
                String title = "دسته بندی مورد نظر را انتخاب کنید.";
                String searchHint = "جستجو دسته بندی:";
                DialogBottomSheetSelect dialog = new DialogBottomSheetSelect(title, searchHint, listSelectCategories);
                dialog.setClickListener(id -> {
                    for (CategoriesResponse.Category item : categoriesResponse.getCategories())
                        if (item.getId() == id) {
                            categoryId = id;
                            binding.tvSelectedCategoryName.setText(String.valueOf(item.getTitle()));
                            dialog.dismiss();
                            break;
                        }
                });
                dialog.show(getSupportFragmentManager(), null);
                break;
            }
        }
    }

    private void sendFinalStoreRequest() {
        AdminUserNewsUpdateRequest adminUserNewsUpdateRequest = new AdminUserNewsUpdateRequest();
        adminUserNewsUpdateRequest.setActive(active);
        adminUserNewsUpdateRequest.setHot(hot);
        adminUserNewsUpdateRequest.setUserNewsId(userNewsId);
        adminUserNewsUpdateRequest.setTitle(binding.etTitle.getText().toString().trim());
        adminUserNewsUpdateRequest.setCategoryId(categoryId);
        adminUserNewsUpdateRequest.setAdminId(getAppPreferencesHelper().getAdminId());
        adminUserNewsUpdateRequest.setDescription(binding.etDescription.getText().toString().trim());

        String link = binding.etLink.getText().toString().trim();
        if (link.length() > 0) {
            if (!link.startsWith("https") || !link.startsWith("http")) {
                link = "https://" + link;
            }
            adminUserNewsUpdateRequest.setLink(link);
        }
        adminUserNewsUpdateRequest.setMedia(mediaModels);
        adminUserNewsUpdateRequest.setAdminId(getAppPreferencesHelper().getAdminId());
        presenter.requestNewsUpdate(adminUserNewsUpdateRequest);
    }

    @Override
    public void onResponseCategoriesIndex(CategoriesResponse categoriesResponse) {
        dismissLoading();
        this.categoriesResponse = categoriesResponse;
        listSelectCategories.clear();
        Drawable icon = ContextCompat.getDrawable(this, R.drawable.ic_list);
        for (CategoriesResponse.Category item : categoriesResponse.getCategories()) {
            DialogBottomSheetSelect.SelectObject selectObject = new DialogBottomSheetSelect.SelectObject();
            selectObject.setTitle(item.getTitle());
            selectObject.setId(item.getId());
            selectObject.setIcon(icon);
            selectObject.setSelected(false);
            if (categoryId != null) {
                if (categoryId.intValue() == item.getId()) {
                    selectObject.setSelected(true);
                    categoryId = item.getId();
                    binding.tvSelectedCategoryName.setText(String.valueOf(item.getTitle()));
                }
            }
            listSelectCategories.add(selectObject);
        }
    }

    @Override
    public void onErrorResponseCategoriesIndex(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت دسته بندی ها وجود ندارد.");
    }

    @Override
    public void onResponseAdminUserNewsEdit(AdminNewsEditResponse adminNewsEditResponse) {
        dismissLoading();

        binding.etTitle.setText(adminNewsEditResponse.getNews().getTitle());
        binding.etDescription.setText(adminNewsEditResponse.getNews().getDescription());
        binding.etLink.setText(adminNewsEditResponse.getNews().getLink());
        categoryId = adminNewsEditResponse.getNews().getCategory().getId();
        presenter.requestCategoriesIndex();
        for (NewsResponse.NewsData.News.NewsMedia newsMedia : adminNewsEditResponse.getNews().getNewsMedia()) {
            FileModel model = new FileModel();
            if (newsMedia.getType() == NewsResponse.NewsType.picture) {
                model.setFileMimeType(FileMimeType.FILE_MIME_TYPE_IMAGE);
            } else {
                model.setFileMimeType(FileMimeType.FILE_MIME_TYPE_VIDEO);
            }
            model.setPath(newsMedia.getPath());
            model.setId(newsMedia.getId());
            adapterAdminNewsImages.addItem(model);
        }

    }

    @Override
    public void onErrorResponseAdminUserNewsEdit(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت اطلاعات خبر وجود ندارد.");
    }

    @Override
    public void onResponseAdminUserNewsUpdate(AdminNewsUpdateResponse adminNewsUpdateResponse) {
        dismissLoading();
        toast("خبر با موفقیت ویرایش و منتشر شد.");
        Intent intent = new Intent();
        intent.putExtra("user_news_id", adminNewsUpdateResponse.getUserNewsId());
        setResult(2, intent);
        finish();
    }

    @Override
    public void onErrorResponseAdminUserNewsUpdate(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ویرایش خبر به وجود آمد.");
    }

    @Override
    public void onResponseAdminUserNewsMediaDelete(AdminNewsMediaDeleteResponse adminNewsMediaDeleteResponse) {
        dismissLoading();
        adapterAdminNewsImages.delete(adminNewsMediaDeleteResponse.getMediaId());
    }

    @Override
    public void onErrorResponseAdminUserNewsMediaDelete(VolleyError volleyError) {
        dismissLoading();
        toast("حذف عکس/فیلم با مشکل مواجه شد.");
    }

    private boolean checkValidation() {
        if (binding.etTitle.getText().toString().trim().length() == 0) {
            toast("لطفا عنوان خبر را وارد کنید.");
            return false;
        } else if (binding.etDescription.getText().toString().trim().length() == 0) {
            toast("لطفا توضیحات خبر را وارد کنید.");
            return false;
        } else if (binding.etLink.getText().toString().trim().length() > 0 && !TextUtils.isWebsite(binding.etLink.getText().toString())) {
            toast("لطفا لینک خبر را به درستی وارد کنید. مثال: https://example.com");
            return false;
        }
//        else if (adapterAdminNewsImages.getList().size() == 0) {
//            toast("لطفا حداقل یک تصویر برای خبر انتخاب کنید.");
//            return false;
//        }
        else if (categoryId == null) {
            toast("لطفا دسته بندی را انتخاب کنید.");
            return false;
        }
        return true;
    }


    @Override
    public void onEdit(int id) {

    }

    @Override
    public void onDelete(int id, String title) {
        presenter.requestNewsMediaDelete(id);
    }


}