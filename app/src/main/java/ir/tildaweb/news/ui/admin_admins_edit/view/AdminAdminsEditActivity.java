package ir.tildaweb.news.ui.admin_admins_edit.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.android.volley.VolleyError;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ActivityAdminAdminsCreateBinding;
import ir.tildaweb.news.databinding.ActivityAdminAdminsEditBinding;
import ir.tildaweb.news.ui.admin_admins_create.view.AdminAdminsCreateActivity;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsShowResponse;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsUpdateRequest;
import ir.tildaweb.news.ui.admin_admins_edit.model.AdminAdminsUpdateResponse;
import ir.tildaweb.news.ui.admin_admins_edit.presenter.AdminAdminsEditActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminAdminsEditActivity extends BaseActivity implements View.OnClickListener, AdminAdminsEditActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminAdminsEditBinding binding;
    private AdminAdminsEditActivityPresenter presenter;
    private Integer adminId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminAdminsEditBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminAdminsEditActivityPresenter(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("ویرایش مدیر");
        adminId = getIntent().getIntExtra("admin_id", -1);
        showLoadingFullPage();
        presenter.requestAdminShow(adminId);

        binding.etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 11) {
                    hideKeyboard(AdminAdminsEditActivity.this, binding.etPhone);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.btnConfirm: {
                String name = binding.etName.getText().toString().trim();
                if (name.length() == 0) {
                    toast("لطفا نام و نام خانوادگی را وارد کنید.");
                } else {
                    AdminAdminsUpdateRequest request = new AdminAdminsUpdateRequest();
                    request.setAdminId(adminId);
                    request.setName(name);
                    showLoadingFullPage();
                    presenter.requestAdminUpdate(request);
                }
                break;
            }

        }

    }

    @Override
    public void onResponseAdminAdminsUpdate(AdminAdminsUpdateResponse adminAdminsUpdateResponse) {
        dismissLoading();
        toast("با موفقیت ویرایش شد.");
        finish();
    }

    @Override
    public void onErrorResponseAdminAdminsUpdate(VolleyError volleyError) {
        dismissLoading();
        toast("امکان ویرایش مدیر وجود ندارد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseAdminAdminsShow(AdminAdminsShowResponse adminAdminsShowResponse) {
        dismissLoading();
        binding.etName.setText(String.format("%s", adminAdminsShowResponse.getAdmin().getName()));
        binding.etPhone.setText(String.format("%s", adminAdminsShowResponse.getAdmin().getPhone()));
        if (adminAdminsShowResponse.getAdmin().getType().equals("owner")) {
            binding.etPhone.setEnabled(false);
        }
    }

    @Override
    public void onErrorResponseAdminAdminsShow(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت اطلاعات مدیر وجود ندارد.");
    }
}