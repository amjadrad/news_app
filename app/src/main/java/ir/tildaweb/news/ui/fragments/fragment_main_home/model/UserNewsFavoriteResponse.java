package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;


public class UserNewsFavoriteResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("deleted")
    private Boolean deleted;
    @SerializedName("favorite")
    private NewsResponse.NewsData.News.Favorite favorite;

    public NewsResponse.NewsData.News.Favorite getFavorite() {
        return favorite;
    }

    public void setFavorite(NewsResponse.NewsData.News.Favorite favorite) {
        this.favorite = favorite;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
