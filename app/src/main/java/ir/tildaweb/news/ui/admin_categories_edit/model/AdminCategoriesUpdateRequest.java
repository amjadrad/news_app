package ir.tildaweb.news.ui.admin_categories_edit.model;

import com.google.gson.annotations.SerializedName;

public class AdminCategoriesUpdateRequest {

    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("title")
    private String title;
    @SerializedName("picture")
    private String picture;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
