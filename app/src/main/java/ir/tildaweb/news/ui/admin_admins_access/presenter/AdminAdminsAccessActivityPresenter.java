package ir.tildaweb.news.ui.admin_admins_access.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditRequest;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditResponse;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessUpdateRequest;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessUpdateResponse;


public class AdminAdminsAccessActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminAdminsAccessActivityPresenter(View view) {
        this.view = view;
    }


    public void requestAdminAccessEdit(AdminAccessEditRequest request) {
        String url = Endpoints.BASE_URL_ADMIN_ADMIN_ACCESS_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminAccessEditResponse adminAccessEditResponse = DataParser.fromJson(response, AdminAccessEditResponse.class);
            view.onResponseAdminAccessEdit(adminAccessEditResponse);
        }
                , error -> {
            view.onErrorResponseAdminAccessEdit(error);
        });
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestAdminAccessUpdate(AdminAccessUpdateRequest request) {
        String url = Endpoints.BASE_URL_ADMIN_ADMIN_ACCESS_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminAccessUpdateResponse response1 = DataParser.fromJson(response, AdminAccessUpdateResponse.class);
            view.onResponseAdminAccessUpdate(response1);
        }
                , error -> {
            view.onErrorResponseAdminAccessUpdate(error);
        });
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public interface View {
        void onResponseAdminAccessEdit(AdminAccessEditResponse adminAccessEditResponse);

        void onErrorResponseAdminAccessEdit(VolleyError volleyError);

        void onResponseAdminAccessUpdate(AdminAccessUpdateResponse adminAccessUpdateResponse);

        void onErrorResponseAdminAccessUpdate(VolleyError volleyError);
    }

}