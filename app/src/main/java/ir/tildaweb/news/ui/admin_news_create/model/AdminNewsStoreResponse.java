package ir.tildaweb.news.ui.admin_news_create.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsStoreResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("news_id")
    private Integer newsId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}