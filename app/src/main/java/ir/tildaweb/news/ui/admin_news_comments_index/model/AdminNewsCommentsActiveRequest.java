package ir.tildaweb.news.ui.admin_news_comments_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsCommentsActiveRequest {

    @SerializedName("news_comment_id")
    private Integer newsCommentId;
    @SerializedName("active")
    private Integer active;

    public Integer getNewsCommentId() {
        return newsCommentId;
    }

    public void setNewsCommentId(Integer newsCommentId) {
        this.newsCommentId = newsCommentId;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }
}