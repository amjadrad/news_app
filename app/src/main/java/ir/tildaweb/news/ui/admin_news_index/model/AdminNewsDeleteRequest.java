package ir.tildaweb.news.ui.admin_news_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsDeleteRequest {

    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
