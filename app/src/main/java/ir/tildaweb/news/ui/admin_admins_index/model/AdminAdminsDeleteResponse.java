package ir.tildaweb.news.ui.admin_admins_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminAdminsDeleteResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }
}
