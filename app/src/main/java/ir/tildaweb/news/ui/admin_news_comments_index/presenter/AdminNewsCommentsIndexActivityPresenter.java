package ir.tildaweb.news.ui.admin_news_comments_index.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_news_comments_index.model.AdminNewsCommentsActiveRequest;
import ir.tildaweb.news.ui.admin_news_comments_index.model.AdminNewsCommentsActiveResponse;
import ir.tildaweb.news.ui.admin_news_comments_index.model.AdminNewsCommentsRequest;
import ir.tildaweb.news.ui.admin_news_comments_index.model.AdminNewsCommentsResponse;


public class AdminNewsCommentsIndexActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminNewsCommentsIndexActivityPresenter(View view) {
        this.view = view;
    }


    public void requestComments(int newsId, int page) {

        String url = Endpoints.BASE_URL_ADMIN_NEWS_COMMENTS;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminNewsCommentsResponse commentsResponse = DataParser.fromJson(response, AdminNewsCommentsResponse.class);
            view.onResponseAdminNewsComments(commentsResponse);
        }
                , error -> {
            view.onErrorResponseAdminNewsComments(error);
        });

        AdminNewsCommentsRequest request = new AdminNewsCommentsRequest();
        request.setNewsId(newsId);
        request.setPage(page);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestCommentActiveChange(int newsCommentId, boolean isActive) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_COMMENTS_ACTIVE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsCommentsActiveResponse result = DataParser.fromJson(response, AdminNewsCommentsActiveResponse.class);
            view.onResponseAdminNewsCommentActive(result);
        }
                , error -> {
            view.onErrorResponseAdminNewsCommentActive(error);
        });

        AdminNewsCommentsActiveRequest adminNewsDeleteRequest = new AdminNewsCommentsActiveRequest();
        adminNewsDeleteRequest.setActive(isActive ? 1 : 0);
        adminNewsDeleteRequest.setNewsCommentId(newsCommentId);
        volleyRequestController.setParameters(adminNewsDeleteRequest);
        volleyRequestController.start();
    }

    public void requestCommentSeen(int newsCommentId) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_COMMENTS_SEEN;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            Log.d(TAG, "requestCommentSeen: " + response);
        }
                , error -> {
            Log.d(TAG, "requestCommentSeen: error");
        });

        AdminNewsCommentsActiveRequest adminNewsDeleteRequest = new AdminNewsCommentsActiveRequest();
        adminNewsDeleteRequest.setNewsCommentId(newsCommentId);
        volleyRequestController.setParameters(adminNewsDeleteRequest);
        volleyRequestController.start();
    }

    public interface View {

        void onResponseAdminNewsComments(AdminNewsCommentsResponse response);

        void onErrorResponseAdminNewsComments(VolleyError volleyError);

        void onResponseAdminNewsCommentActive(AdminNewsCommentsActiveResponse response);

        void onErrorResponseAdminNewsCommentActive(VolleyError volleyError);


    }


}