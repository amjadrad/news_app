package ir.tildaweb.news.ui.main.view;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.ffaa00.yummy_bottomnav.OnItemSelectedListener;
import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterCategories;
import ir.tildaweb.news.adapter.AdapterHotNews;
import ir.tildaweb.news.adapter.AdapterMostVisitNews;
import ir.tildaweb.news.adapter.AdapterNews;
import ir.tildaweb.news.adapter.AdapterPosterMainNews;
import ir.tildaweb.news.adapter.AdapterWeather;
import ir.tildaweb.news.app.CacheManager;
import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.cache.SearchHistoryModel;
import ir.tildaweb.news.databinding.ActivityMainBinding;
import ir.tildaweb.news.databinding.FragmentMainHomeBinding;
import ir.tildaweb.news.dialogs.DialogAppCommentCreate;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.dialogs.DialogNewsCommentCreate;
import ir.tildaweb.news.dialogs.DialogProfile;
import ir.tildaweb.news.dialogs.DialogProfileAdmin;
import ir.tildaweb.news.dialogs.DialogSearch;
import ir.tildaweb.news.dialogs.DialogShareApp;
import ir.tildaweb.news.listeners.NewsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.admin_admins_index.view.AdminAdminsIndexActivity;
import ir.tildaweb.news.ui.admin_categories_index.view.AdminCategoriesIndexActivity;
import ir.tildaweb.news.ui.admin_news_index.view.AdminNewsIndexActivity;
import ir.tildaweb.news.ui.admin_settings.view.AdminSettingsActivity;
import ir.tildaweb.news.ui.admin_user_news_index.view.AdminUserNewsIndexActivity;
import ir.tildaweb.news.ui.app_comments_list.view.AppCommentsListActivity;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.contact_us.view.ContactUsActivity;
import ir.tildaweb.news.ui.contact_us_list.view.ContactUsListActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.presenter.CategoriesFragmentPresenter;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.AppCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsHotResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsMostVisitResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsPosterResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UsersCountResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.WeatherResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.presenter.NewsFragmentPresenter;
import ir.tildaweb.news.ui.news_comments.view.NewsCommentsIndexActivity;
import ir.tildaweb.news.ui.news_show.view.NewsShowActivity;
import ir.tildaweb.news.ui.profile_edit.view.ProfileEditActivity;
import ir.tildaweb.news.ui.search.view.SearchActivity;
import ir.tildaweb.news.ui.settings.view.SettingsActivity;
import ir.tildaweb.news.ui.sigin.view.SignActivity;
import ir.tildaweb.news.ui.user_news_favs.view.UserNewsBookmarksActivity;
import ir.tildaweb.news.ui.user_news_index.view.UserNewsIndexActivity;
import ir.tildaweb.news.utils.FileUtils;
import ir.tildaweb.news.utils.FontUtils;
import ir.tildaweb.news.utils.IntentHelper;
import ir.tildaweb.news.utils.MathHelper;

public class MainActivity extends BaseActivity implements View.OnClickListener, NewsFragmentPresenter.View, OnLoadMoreListener, NewsClickListener, CategoriesFragmentPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityMainBinding binding;
    private NewsFragmentPresenter presenter;
    private CategoriesFragmentPresenter categoriesFragmentPresenter;
    private AdapterNews adapterNews;
    private AdapterHotNews adapterHotNews;
    private AdapterMostVisitNews adapterMostVisitNews;
    //    private AdapterPosterMainNews adapterPosterMainNews;
    private int nextPage = 1;
    private int page = 1;
    private String search = null;
    private Integer categoryId = null;
    private AdapterCategories adapterCategories;
    private AdapterWeather adapterWeather;
    private CacheManager cacheManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.imageViewProfile.setOnClickListener(this);
        binding.imageViewProfileAdmin.setOnClickListener(this);
        binding.imageViewSearch.setOnClickListener(this);
        binding.linearDeleteSearchHistory.setOnClickListener(this);

        if (getAppPreferencesHelper().getUserIsAdminPref() && getAppPreferencesHelper().getAdminId() > 0) {
            binding.imageViewProfileAdmin.setVisibility(View.VISIBLE);
        } else {
            binding.imageViewProfileAdmin.setVisibility(View.GONE);
        }

        binding.btnChangeCity.setOnClickListener(this);
        binding.imageViewCloseSearch.setOnClickListener(this);
        binding.btnSearch.setOnClickListener(this);

        binding.etSearch.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == EditorInfo.IME_ACTION_SEARCH ||
                    keyCode == EditorInfo.IME_ACTION_DONE || (event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                binding.btnSearch.callOnClick();
                return true;
            }
            return false;
        });

        presenter = new NewsFragmentPresenter(this);
        categoriesFragmentPresenter = new CategoriesFragmentPresenter(this);
        //News
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        adapterNews = new AdapterNews(MainActivity.this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterNews);
        //Categories
        LinearLayoutManager linearLayoutManagerCategories = new LinearLayoutManager(MainActivity.this);
        linearLayoutManagerCategories.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.recyclerViewCategories.setLayoutManager(linearLayoutManagerCategories);
        adapterCategories = new AdapterCategories(MainActivity.this, new ArrayList<>());
        binding.recyclerViewCategories.setAdapter(adapterCategories);
        //Hot news
        LinearLayoutManager linearLayoutManagerHotNews = new LinearLayoutManager(MainActivity.this);
        linearLayoutManagerHotNews.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerViewHotNews.setLayoutManager(linearLayoutManagerHotNews);
        adapterHotNews = new AdapterHotNews(MainActivity.this, new ArrayList<>(), this);
        binding.recyclerViewHotNews.setAdapter(adapterHotNews);
        //Most visit news
        LinearLayoutManager linearLayoutManagerMostVisitNews = new LinearLayoutManager(MainActivity.this);
        linearLayoutManagerMostVisitNews.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerViewMostVisitNews.setLayoutManager(linearLayoutManagerMostVisitNews);
        adapterMostVisitNews = new AdapterMostVisitNews(MainActivity.this, new ArrayList<>(), this);
        binding.recyclerViewMostVisitNews.setAdapter(adapterMostVisitNews);
        //Poster news
//        LinearLayoutManager linearLayoutManagerPosterNews = new LinearLayoutManager(MainActivity.this);
//        linearLayoutManagerPosterNews.setOrientation(RecyclerView.HORIZONTAL);
//        binding.recyclerViewPostersMainNews.setLayoutManager(linearLayoutManagerPosterNews);
//        adapterPosterMainNews = new AdapterPosterMainNews(MainActivity.this, new ArrayList<>(), this);
//        binding.recyclerViewPostersMainNews.setAdapter(adapterPosterMainNews);
        //Weather
        adapterWeather = new AdapterWeather(this, new ArrayList<>());
        LinearLayoutManager linearLayoutManagerWeather = new LinearLayoutManager(this);
        linearLayoutManagerWeather.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerViewWeather.setLayoutManager(linearLayoutManagerWeather);
        binding.recyclerViewWeather.setAdapter(adapterWeather);
        binding.swipeRefresh.setOnRefreshListener(() -> {
            categoriesFragmentPresenter.requestCategories(getAppPreferencesHelper().getUserId());
            updateNews();
        });
        categoriesFragmentPresenter.requestCategories(getAppPreferencesHelper().getUserId());
        updateNews();
    }

    public void updateNews() {
        nextPage = 1;
        page = 1;
        search = null;
        categoryId = null;
        adapterNews.clearAll();
        presenter.requestUsersCount();
        presenter.requestNewsHot(getAppPreferencesHelper().getIsShowNewsGalleryPref());
        presenter.requestNewsMostVisit(getAppPreferencesHelper().getIsShowNewsGalleryPref());
//        presenter.requestNewsPostersMain();
        presenter.requestWeather(getAppPreferencesHelper().getUserCenterCityTitlePref());
        presenter.requestNewsSearch(page, search, categoryId, getAppPreferencesHelper().getUserId(), getAppPreferencesHelper().getIsShowNewsGalleryPref());
        if (getAppPreferencesHelper().getUserCenterCityTitlePref() != null) {
            binding.tvWeatherCityName.setText(getAppPreferencesHelper().getUserCenterCityTitlePref());
        }
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestNewsSearch(nextPage, search, categoryId, getAppPreferencesHelper().getUserId(), getAppPreferencesHelper().getIsShowNewsGalleryPref());
        }
    }

    @Override
    public void onResponseNews(NewsResponse newsResponse) {
        dismissLoading();
        binding.swipeRefresh.setRefreshing(false);
        if (newsResponse.getData() != null && newsResponse.getData().getNews() != null) {
            adapterNews.addItems(newsResponse.getData().getNews());
            nextPage = VolleyRequestController.getNextPage(newsResponse.getData().getNextPageUrl());
        }
    }

    @Override
    public void onErrorResponseNews(VolleyError volleyError) {
        dismissLoading();
        binding.swipeRefresh.setRefreshing(false);
        toast("مشکلی در دریافت اخبار به وجود آمد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseNewsHot(NewsHotResponse response) {
        if (response.getNews() != null && response.getNews().size() > 0) {
            adapterHotNews.clearAll();
            adapterHotNews.addItems(response.getNews());
            binding.recyclerViewHotNews.setVisibility(View.VISIBLE);
        } else {
            binding.recyclerViewHotNews.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponseNewsHot(VolleyError volleyError) {

    }

    @Override
    public void onResponseNewsMostVisit(NewsMostVisitResponse response) {
        if (response.getNews() != null && response.getNews().size() > 0) {
            adapterMostVisitNews.clearAll();
//            adapterMostVisitNews.addItem(null);
            adapterMostVisitNews.addItems(response.getNews());
            binding.recyclerViewMostVisitNews.setVisibility(View.VISIBLE);
        } else {
            binding.recyclerViewMostVisitNews.setVisibility(View.GONE);
        }

    }

    @Override
    public void onErrorResponseNewsMostVisit(VolleyError volleyError) {

    }

    @Override
    public void onResponseNewsPostersMain(NewsPosterResponse response) {
//        if (response.getPosterNews() != null && response.getPosterNews().size() > 0) {
//            adapterPosterMainNews.clearAll();
//            adapterPosterMainNews.addItem(null);
//            adapterPosterMainNews.addItems(response.getPosterNews());
//            binding.recyclerViewPostersMainNews.setVisibility(View.VISIBLE);
//        } else {
//            binding.recyclerViewPostersMainNews.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onErrorResponseNewsPostersMain(VolleyError volleyError) {

    }

    @Override
    public void onResponseNewsCommentsStore(NewsCommentStoreResponse newsCommentStoreResponse) {
        dismissLoading();
        toast("نظر شما با موفقیت ذخیره شد و پس از تایید منتظر خواهد شد.");
    }

    @Override
    public void onErrorResponseNewsCommentsStore(VolleyError volleyError) {
        dismissLoading();
        toast("خطایی هنگام ذخیره نظر رخ داد. مجدد امتحان کنید.");
    }

    @Override
    public void onResponseNewsLike(NewsLikeResponse newsLikeResponse) {
        dismissLoading();
        adapterNews.updateLike(newsLikeResponse);
    }

    @Override
    public void onErrorResponseNewsLike(VolleyError volleyError) {
        dismissLoading();
    }

    @Override
    public void onResponseUserNewsFavorite(UserNewsFavoriteResponse response) {
        dismissLoading();
        adapterNews.updateFavorite(response);
    }

    @Override
    public void onErrorResponseUserNewsFavorite(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی پیش آمد. مجدد امتحان کنید.");
    }

    @Override
    public void onResponseNewsVisit(NewsLikeResponse response) {

    }

    @Override
    public void onErrorResponseNewsVisit(VolleyError volleyError) {

    }

    @Override
    public void onResponseWeathers(WeatherResponse response) {
        dismissLoading();
        if (response.getStatus() == 200) {
            adapterWeather.clearAll();
            adapterWeather.addItems(response.getWeather());
        }
    }

    @Override
    public void onErrorResponseWeathers(VolleyError volleyError) {
        dismissLoading();
    }

    @Override
    public void onResponseUsersCount(UsersCountResponse response) {
        getAppPreferencesHelper().setUsersCount(response.getCount());
        getAppPreferencesHelper().setShowUsersCount(response.getShow());
    }

    @Override
    public void onErrorResponseUsersCount(VolleyError volleyError) {

    }

    @Override
    public void onResponseAppCommentStore(AppCommentStoreResponse response) {
        dismissLoading();
        toast("نظر شما با موفقیت ثبت شد. ممنون :)");
    }

    @Override
    public void onErrorResponseAppCommentStore(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ذخیره نظر به وجود آمد.");
    }

    @Override
    public void onLike(int id) {
        dismissLoading();
        presenter.requestNewsLike(id, getAppPreferencesHelper().getUserId(), true);
    }

    @Override
    public void onDisLike(int id) {
        dismissLoading();
        presenter.requestNewsLike(id, getAppPreferencesHelper().getUserId(), false);
    }

    @Override
    public void onFavorite(int id) {
        presenter.requestNewsFavorite(id, getAppPreferencesHelper().getUserId());
    }

    @Override
    public void onCreateComment(int id) {
        DialogNewsCommentCreate dialogNewsCommentCreate = new DialogNewsCommentCreate(MainActivity.this);
        dialogNewsCommentCreate.setOnConfirmClickListener(view -> {
            String comment = dialogNewsCommentCreate.getDescription();
            hideKeyboard(MainActivity.this, dialogNewsCommentCreate.getEtDescription());
            dialogNewsCommentCreate.dismiss();
            showLoadingFullPage();
            presenter.requestNewsCommentCreate(id, getAppPreferencesHelper().getUserId(), comment);
        });
        dialogNewsCommentCreate.show();
    }

    @Override
    public void onCommentsList(int id) {
        Intent intent = new Intent(MainActivity.this, NewsCommentsIndexActivity.class);
        intent.putExtra("news_id", id);
        startActivity(intent);
    }

    @Override
    public void onShowNews(int id) {
        Intent intent = new Intent(MainActivity.this, NewsShowActivity.class);
        intent.putExtra("news_id", id);
        startActivity(intent);
    }

    @Override
    public void onVisitNews(int id) {
        presenter.requestNewsVisit(id, getAppPreferencesHelper().getUserId());
    }

    @Override
    public void onResponseCategories(CategoriesResponse categoriesResponse) {
        adapterCategories.clearAll();
        binding.swipeRefresh.setRefreshing(false);
        adapterCategories.addItems(categoriesResponse.getCategories());
    }

    @Override
    public void onErrorResponseCategories(VolleyError volleyError) {
        binding.swipeRefresh.setRefreshing(false);
        toast("امکان دریافت لیست دسته بندی اخبار وجود ندارد.");
    }

    @Override
    public void onBackPressed() {
        if (binding.linearSearch.getVisibility() == View.VISIBLE) {
            binding.imageViewCloseSearch.callOnClick();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewProfile: {
                DialogProfile dialogProfile = new DialogProfile(MainActivity.this);
                dialogProfile.setOnDialogProfileClickListener(new DialogProfile.OnDialogProfileClickListener() {
                    @Override
                    public void onLogout() {
                        String title = "خروج از حساب کاربری";
                        String description = "آیا میخواهید از حساب کاربری تان خارج شوید؟";
                        DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(MainActivity.this, title, description);
                        dialogConfirmMessage.setConfirmButtonText("خروج");
                        dialogConfirmMessage.setDanger();
                        dialogConfirmMessage.setOnConfirmClickListener(view15 -> {
                            dialogProfile.dismiss();
                            dialogConfirmMessage.dismiss();
                            getAppPreferencesHelper().setLoginPref(false);
                            getAppPreferencesHelper().setUserId(-1);
                            getAppPreferencesHelper().setUserAdminTypePref(null);
                            getAppPreferencesHelper().setAdminId(-1);
                            getAppPreferencesHelper().setUserIsAdminPref(false);
                            startActivity(new Intent(MainActivity.this, SignActivity.class));
                            finish();
                        });
                        dialogConfirmMessage.show();
                    }

                    @Override
                    public void onClickAdvertise() {
                        dialogProfile.dismiss();
                        new IntentHelper(MainActivity.this).openSite(BuildConfig.SITE_URL);
                    }


                    @Override
                    public void onClickContactUs() {
                        dialogProfile.dismiss();
                        startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
                    }


                    @Override
                    public void onClickEditProfile() {
                        dialogProfile.dismiss();
                        startActivity(new Intent(MainActivity.this, ProfileEditActivity.class));
                    }

                    @Override
                    public void onClickSettings() {
                        dialogProfile.dismiss();
                        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                    }

                    @Override
                    public void onClickBookmarks() {
                        dialogProfile.dismiss();
                        startActivity(new Intent(MainActivity.this, UserNewsBookmarksActivity.class));
                    }

                    @Override
                    public void onUserNewsClick() {
                        dialogProfile.dismiss();
                        startActivity(new Intent(MainActivity.this, UserNewsIndexActivity.class));
                    }

                    @Override
                    public void onSendToFriendsClick() {
                        DialogShareApp dialogShareApp = new DialogShareApp(MainActivity.this);
                        dialogShareApp.setOnShareApkClickListener(view12 -> FileUtils.shareApplication(MainActivity.this));
                        dialogShareApp.setOnShareLinkClickListener(view13 -> {
                            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                            String shareBody = "دانلود برنامه " + getApplicationContext().getString(getApplicationContext().getApplicationInfo().labelRes) + "\n" + BuildConfig.SITE_URL_DOWNLOAD_APK;
                            intent.setType("text/plain");
                            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "اشتراک برنامه");
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            startActivity(Intent.createChooser(intent, "اشتراک لینک برنامه با"));
                        });

                        dialogShareApp.show();
                    }

                    @Override
                    public void onAppCommentClick() {
                        DialogAppCommentCreate dialogAppCommentCreate = new DialogAppCommentCreate(MainActivity.this);
                        dialogAppCommentCreate.setOnConfirmClickListener(view14 -> {
                            float rating = dialogAppCommentCreate.getRating();
                            String comment = dialogAppCommentCreate.getDescription();
                            showLoadingFullPage();
                            presenter.requestAppCommentStore(getAppPreferencesHelper().getUserId(), comment, rating);
                            dialogAppCommentCreate.dismiss();
                        });
                        dialogAppCommentCreate.show();
                    }
                });
                dialogProfile.show();
                break;
            }
            case R.id.imageViewProfileAdmin: {
                DialogProfileAdmin dialogProfileAdmin = new DialogProfileAdmin(MainActivity.this);
                dialogProfileAdmin.setonDialogProfileAdminClickListener(new DialogProfileAdmin.OnDialogProfileAdminClickListener() {
                    @Override
                    public void onClickAdmins() {
                        dialogProfileAdmin.dismiss();
                        startActivity(new Intent(MainActivity.this, AdminAdminsIndexActivity.class));
                    }

                    @Override
                    public void onClickCategories() {
                        dialogProfileAdmin.dismiss();
                        startActivity(new Intent(MainActivity.this, AdminCategoriesIndexActivity.class));
                    }

                    @Override
                    public void onClickNews() {
                        dialogProfileAdmin.dismiss();
                        startActivity(new Intent(MainActivity.this, AdminNewsIndexActivity.class));
                    }


                    @Override
                    public void onClickContactUsList() {
                        dialogProfileAdmin.dismiss();
                        if (getAppPreferencesHelper().getAdminAccessUserContacts() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
                            startActivity(new Intent(MainActivity.this, ContactUsListActivity.class));
                        } else {
                            toast("شما دسترسی مشاهده پیام های کاربر را ندارید.");
                        }
                    }

                    @Override
                    public void onUserNewsManagementClick() {
                        dialogProfileAdmin.dismiss();
                        startActivity(new Intent(MainActivity.this, AdminUserNewsIndexActivity.class));
                    }

                    @Override
                    public void onAppCommentsListClick() {
                        dialogProfileAdmin.dismiss();
                        if (getAppPreferencesHelper().getAdminAccessAppComments() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
                            startActivity(new Intent(MainActivity.this, AppCommentsListActivity.class));
                        } else {
                            toast("شما دسترسی مشاهده نظرات برنامه را ندارید.");
                        }
                    }

                    @Override
                    public void onSettingsClick() {
                        dialogProfileAdmin.dismiss();
                        startActivity(new Intent(MainActivity.this, AdminSettingsActivity.class));
                    }


                });
                dialogProfileAdmin.show();
                break;
            }
            case R.id.imageViewCloseSearch: {
                binding.linearSearch.setVisibility(View.GONE);
                binding.linearToolbar.setVisibility(View.VISIBLE);
                hideKeyboard(MainActivity.this, binding.etSearch);
                binding.etSearch.setText("");
                break;
            }
            case R.id.btnSearch: {
                String search = binding.etSearch.getText().toString().trim();
                if (search.length() > 0) {
                    SearchHistoryModel searchHistoryModel = DataParser.fromJson(getCacheManager().getUserSearchHistory(), SearchHistoryModel.class);
                    if (searchHistoryModel == null) {
                        searchHistoryModel = new SearchHistoryModel();
                        searchHistoryModel.setSearchs(new ArrayList<>());
                    }
                    searchHistoryModel.getSearchs().add(0, search);
                    if (searchHistoryModel.getSearchs().size() > 8) {
                        searchHistoryModel.getSearchs().remove(8);
                    }
                    getCacheManager().setUserSearchHistory(DataParser.toJson(searchHistoryModel));
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    intent.putExtra("search", search);
                    startActivity(intent);
                    binding.imageViewCloseSearch.callOnClick();
                } else {
                    toast("لطفا متن مورد نظرتان را وارد کنید.");
                }

                break;
            }
            case R.id.imageViewSearch: {
                binding.etSearch.setText("");
                binding.linearSearch.setVisibility(View.VISIBLE);
                binding.linearToolbar.setVisibility(View.GONE);
                binding.etSearch.requestFocus();
                showKeyboard(MainActivity.this);
                binding.chipGroup.removeAllViews();
                cacheManager = new CacheManager(MainActivity.this);
                SearchHistoryModel searchHistoryModel = DataParser.fromJson(cacheManager.getUserSearchHistory(), SearchHistoryModel.class);
                if (searchHistoryModel != null && searchHistoryModel.getSearchs() != null) {
                    if (searchHistoryModel.getSearchs().size() == 0) {
                        binding.linearDeleteSearchHistory.setVisibility(View.GONE);
                        binding.chipGroup.setVisibility(View.GONE);
                    } else {
                        binding.linearDeleteSearchHistory.setVisibility(View.VISIBLE);
                        binding.chipGroup.setVisibility(View.VISIBLE);
                    }
                    for (String str : searchHistoryModel.getSearchs()) {
                        AppCompatTextView textView = new AppCompatTextView(MainActivity.this);
                        textView.setText(str);
                        int dp8 = MathHelper.convertDipToPixels(MainActivity.this, 8);
                        int dp4 = MathHelper.convertDipToPixels(MainActivity.this, 4);
                        textView.setPadding(dp8, dp4, dp8, dp4);
                        textView.setBackgroundDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.bg_info_pill_default));
                        textView.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorText));
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12 * getAppPreferencesHelper().getTextSizePref());
                        textView.setTypeface(FontUtils.getFont(MainActivity.this), Typeface.BOLD);
                        textView.setOnClickListener(view2 -> {
                            String text = ((AppCompatTextView) view2).getText().toString();
                            binding.etSearch.setText(text);
                            binding.etSearch.setSelection(text.length());
                        });
                        binding.chipGroup.addView(textView);
                    }
                } else {
                    binding.linearDeleteSearchHistory.setVisibility(View.GONE);
                    binding.chipGroup.setVisibility(View.GONE);
                }


//                DialogSearch dialogSearch = new DialogSearch(MainActivity.this);
//                dialogSearch.setOnConfirmClickListener(view1 -> {
//                    String search = dialogSearch.getEtSearch().getText().toString().trim();
//                    SearchHistoryModel searchHistoryModel = DataParser.fromJson(getCacheManager().getUserSearchHistory(), SearchHistoryModel.class);
//                    if (searchHistoryModel == null) {
//                        searchHistoryModel = new SearchHistoryModel();
//                        searchHistoryModel.setSearchs(new ArrayList<>());
//                    }
//                    searchHistoryModel.getSearchs().add(0, search);
//                    if (searchHistoryModel.getSearchs().size() > 8) {
//                        searchHistoryModel.getSearchs().remove(8);
//                    }
//                    getCacheManager().setUserSearchHistory(DataParser.toJson(searchHistoryModel));
//                    dialogSearch.dismiss();
//                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
//                    intent.putExtra("search", search);
//                    startActivity(intent);
//                });
//                dialogSearch.show();
//                dialogSearch.getEtSearch().requestFocus();
                break;
            }
            case R.id.btnChangeCity: {
                startActivity(new Intent(MainActivity.this, ProfileEditActivity.class));
                break;
            }
            case R.id.linearDeleteSearchHistory: {
                cacheManager.setUserSearchHistory(null);
                binding.chipGroup.removeAllViews();
                break;
            }
        }
    }


}