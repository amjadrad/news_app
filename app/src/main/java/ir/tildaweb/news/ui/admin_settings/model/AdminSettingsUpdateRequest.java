package ir.tildaweb.news.ui.admin_settings.model;

import com.google.gson.annotations.SerializedName;

public class AdminSettingsUpdateRequest {
    @SerializedName("is_show_user_count")
    private Boolean isShowUserCount;

    public Boolean getShowUserCount() {
        return isShowUserCount;
    }

    public void setShowUserCount(Boolean showUserCount) {
        isShowUserCount = showUserCount;
    }
}
