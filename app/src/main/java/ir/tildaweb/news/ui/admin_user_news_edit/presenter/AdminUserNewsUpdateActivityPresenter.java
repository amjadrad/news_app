package ir.tildaweb.news.ui.admin_user_news_edit.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsEditResponse;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsMediaDeleteRequest;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsMediaDeleteResponse;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminUserNewsUpdateRequest;
import ir.tildaweb.news.ui.admin_user_news_edit.model.AdminNewsUpdateResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsShowRequest;


public class AdminUserNewsUpdateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminUserNewsUpdateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesIndex() {
        String url = Endpoints.BASE_URL_CATEGORIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CategoriesResponse categoriesResponse = DataParser.fromJson(response, CategoriesResponse.class);
            view.onResponseCategoriesIndex(categoriesResponse);
        }
                , error -> {
            view.onErrorResponseCategoriesIndex(error);
        });
        volleyRequestController.start();
    }

    public void requestNewsEdit(int newsId) {
        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminNewsEditResponse resp = DataParser.fromJson(response, AdminNewsEditResponse.class);
            view.onResponseAdminUserNewsEdit(resp);
        }
                , error -> {
            view.onErrorResponseAdminUserNewsEdit(error);
        });

        UserNewsShowRequest request = new UserNewsShowRequest();
        request.setUserNewsId(newsId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }


    public void requestNewsUpdate(AdminUserNewsUpdateRequest adminUserNewsUpdateRequest) {
        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsUpdateResponse adminNewsUpdateResponse = DataParser.fromJson(response, AdminNewsUpdateResponse.class);
            view.onResponseAdminUserNewsUpdate(adminNewsUpdateResponse);
        }
                , error -> {
            view.onErrorResponseAdminUserNewsUpdate(error);
        });

        volleyRequestController.setBody(adminUserNewsUpdateRequest);
        volleyRequestController.start();
    }

    public void requestNewsMediaDelete(Integer mediaId) {
        String url = Endpoints.BASE_URL_ADMIN_USER_NEWS_MEDIA_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsMediaDeleteResponse adminNewsMediaDeleteResponse = DataParser.fromJson(response, AdminNewsMediaDeleteResponse.class);
            view.onResponseAdminUserNewsMediaDelete(adminNewsMediaDeleteResponse);
        }
                , error -> {
            view.onErrorResponseAdminUserNewsMediaDelete(error);
        });

        AdminNewsMediaDeleteRequest adminNewsMediaDeleteRequest = new AdminNewsMediaDeleteRequest();
        adminNewsMediaDeleteRequest.setMediaId(mediaId);
        volleyRequestController.setBody(adminNewsMediaDeleteRequest);
        volleyRequestController.start();
    }


    public interface View {
        void onResponseCategoriesIndex(CategoriesResponse categoriesResponse);

        void onErrorResponseCategoriesIndex(VolleyError volleyError);

        void onResponseAdminUserNewsEdit(AdminNewsEditResponse adminNewsEditResponse);

        void onErrorResponseAdminUserNewsEdit(VolleyError volleyError);

        void onResponseAdminUserNewsUpdate(AdminNewsUpdateResponse adminNewsUpdateResponse);

        void onErrorResponseAdminUserNewsUpdate(VolleyError volleyError);

        void onResponseAdminUserNewsMediaDelete(AdminNewsMediaDeleteResponse adminNewsMediaDeleteResponse);

        void onErrorResponseAdminUserNewsMediaDelete(VolleyError volleyError);
    }

}