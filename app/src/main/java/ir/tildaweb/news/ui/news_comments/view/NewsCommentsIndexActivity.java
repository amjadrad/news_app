package ir.tildaweb.news.ui.news_comments.view;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterNewsComments;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityNewsCommentsIndexBinding;
import ir.tildaweb.news.dialogs.DialogNewsCommentCreate;
import ir.tildaweb.news.listeners.NewsCommentsClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;
import ir.tildaweb.news.ui.news_comments.presenter.NewsCommentsIndexActivityPresenter;

public class NewsCommentsIndexActivity extends BaseActivity implements View.OnClickListener, NewsCommentsIndexActivityPresenter.View, OnLoadMoreListener, NewsCommentsClickListener {

    private String TAG = this.getClass().getName();
    private ActivityNewsCommentsIndexBinding binding;
    private NewsCommentsIndexActivityPresenter presenter;
    private AdapterNewsComments adapterNewsComments;
    private int newsId;
    private int nextPage = 1;
    private int page = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsId = getIntent().getIntExtra("news_id", -1);
        binding = ActivityNewsCommentsIndexBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new NewsCommentsIndexActivityPresenter(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterNewsComments = new AdapterNewsComments(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterNewsComments);

        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("نظرات خبر");
        showLoadingFullPage();
        presenter.requestNewsComments(nextPage, newsId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
        }
    }

    @Override
    public void onResponseNewsComments(NewsCommentsResponse newsCommentsResponse) {
        dismissLoading();
        if (newsCommentsResponse.getData() != null && newsCommentsResponse.getData().getComments() != null) {
            adapterNewsComments.addItems(newsCommentsResponse.getData().getComments());
            nextPage = VolleyRequestController.getNextPage(newsCommentsResponse.getData().getNextPageUrl());
        }
    }

    @Override
    public void onErrorResponseNewsComments(VolleyError volleyError) {
        dismissLoading();
        binding.linearNoItem.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResponseNewsCommentsReply(NewsCommentStoreResponse newsCommentStoreResponse) {
        toast("نظر شما با موفقیت ذخیره شد و پس از تایید منتظر خواهد شد.");
        dismissLoading();
    }

    @Override
    public void onErrorResponseNewsCommentsReply(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ثبت نظر رخ داد.");
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestNewsComments(nextPage, newsId);
        }
    }

    @Override
    public void onReplyComment(int newsId, int replyId) {
        DialogNewsCommentCreate dialogNewsCommentCreate = new DialogNewsCommentCreate(this);
        dialogNewsCommentCreate.setOnConfirmClickListener(view -> {
            String comment = dialogNewsCommentCreate.getDescription();
            hideKeyboard(NewsCommentsIndexActivity.this, dialogNewsCommentCreate.getEtDescription());
            dialogNewsCommentCreate.dismiss();
            showLoadingFullPage();
            presenter.requestNewsCommentReply(newsId, replyId, getAppPreferencesHelper().getUserId(), comment);
        });
        dialogNewsCommentCreate.show();
    }
}