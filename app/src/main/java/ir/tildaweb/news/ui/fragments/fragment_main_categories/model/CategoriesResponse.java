package ir.tildaweb.news.ui.fragments.fragment_main_categories.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoriesResponse {

    @SerializedName("categories")
    private ArrayList<Category> categories;

    public class Category {
        @SerializedName("id")
        private Integer id;
        @SerializedName("title")
        private String title;
        @SerializedName("picture")
        private String picture;
        @SerializedName("news_count_unseen")
        private Integer newsCountUnSeen;

        public Integer getNewsCountUnSeen() {
            return newsCountUnSeen;
        }

        public void setNewsCountUnSeen(Integer newsCountUnSeen) {
            this.newsCountUnSeen = newsCountUnSeen;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
