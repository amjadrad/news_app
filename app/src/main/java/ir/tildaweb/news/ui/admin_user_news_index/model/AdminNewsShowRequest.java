package ir.tildaweb.news.ui.admin_user_news_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminNewsShowRequest {

    @SerializedName("news_id")
    private Integer newsId;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}