package ir.tildaweb.news.ui.admin_news_create.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreRequest;
import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;


public class AdminNewsCreateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminNewsCreateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesIndex() {
        String url = Endpoints.BASE_URL_CATEGORIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CategoriesResponse categoriesResponse = DataParser.fromJson(response, CategoriesResponse.class);
            view.onResponseAdminCategoriesIndex(categoriesResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesIndex(error);
        });
        volleyRequestController.start();
    }

    public void requestNewsStore(AdminNewsStoreRequest adminNewsStoreRequest) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsStoreResponse adminNewsStoreResponse = DataParser.fromJson(response, AdminNewsStoreResponse.class);
            view.onResponseAdminNewsStore(adminNewsStoreResponse);
        }
                , error -> {
            view.onErrorResponseAdminNewsStore(error);
        });

        volleyRequestController.setBody(adminNewsStoreRequest);
        volleyRequestController.start();
    }


    public interface View {
        void onResponseAdminCategoriesIndex(CategoriesResponse categoriesResponse);

        void onErrorResponseAdminCategoriesIndex(VolleyError volleyError);

        void onResponseAdminNewsStore(AdminNewsStoreResponse adminNewsStoreResponse);

        void onErrorResponseAdminNewsStore(VolleyError volleyError);
    }

}