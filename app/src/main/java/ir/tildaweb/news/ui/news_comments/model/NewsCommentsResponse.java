package ir.tildaweb.news.ui.news_comments.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationResponse;


public class NewsCommentsResponse {

    @SerializedName("data")
    private CommentData data;

    public class CommentData {

        @SerializedName("data")
        private ArrayList<NewsComment> comments;
        @SerializedName("next_page_url")
        private String nextPageUrl;

        public class NewsComment {
            @SerializedName("id")
            private Integer id;
            @SerializedName("news_id")
            private Integer newsId;
            @SerializedName("comment")
            private String comment;
            @SerializedName("user_id")
            private Integer userId;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("comments")
            private ArrayList<NewsComment> comments;
            @SerializedName("user")
            private SigninVerificationResponse.User user;
            @SerializedName("active")
            private Integer active;
            @SerializedName("seen")
            private Integer seen;

            public Integer getSeen() {
                return seen;
            }

            public void setSeen(Integer seen) {
                this.seen = seen;
            }

            public Integer getActive() {
                return active;
            }

            public void setActive(Integer active) {
                this.active = active;
            }

            public ArrayList<NewsComment> getComments() {
                return comments;
            }

            public void setComments(ArrayList<NewsComment> comments) {
                this.comments = comments;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public SigninVerificationResponse.User getUser() {
                return user;
            }

            public void setUser(SigninVerificationResponse.User user) {
                this.user = user;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getNewsId() {
                return newsId;
            }

            public void setNewsId(Integer newsId) {
                this.newsId = newsId;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }
        }


        public ArrayList<NewsComment> getComments() {
            return comments;
        }

        public void setComments(ArrayList<NewsComment> comments) {
            this.comments = comments;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }
    }

    public CommentData getData() {
        return data;
    }

    public void setData(CommentData data) {
        this.data = data;
    }
}
