package ir.tildaweb.news.ui.admin_user_news_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminUserNewsRequest {

    @SerializedName("page")
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
