package ir.tildaweb.news.ui.admin_admins_create.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.zcw.togglebutton.ToggleButton;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.data.network.FileUploader;
import ir.tildaweb.news.databinding.ActivityAdminAdminsCreateBinding;
import ir.tildaweb.news.databinding.ActivityAdminCategoriesCreateBinding;
import ir.tildaweb.news.ui.admin_admins_create.model.AdminAdminsStoreRequest;
import ir.tildaweb.news.ui.admin_admins_create.model.AdminAdminsStoreResponse;
import ir.tildaweb.news.ui.admin_admins_create.presenter.AdminAdminsCreateActivityPresenter;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateRequest;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateResponse;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.sigin.view.SignActivity;
import ir.tildaweb.news.utils.FileUtils;
import ir.tildaweb.tilda_filepicker.TildaFilePicker;
import ir.tildaweb.tilda_filepicker.enums.FileMimeType;
import ir.tildaweb.tilda_filepicker.models.FileModel;

public class AdminAdminsCreateActivity extends BaseActivity implements View.OnClickListener, AdminAdminsCreateActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminAdminsCreateBinding binding;
    private AdminAdminsCreateActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminAdminsCreateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminAdminsCreateActivityPresenter(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("ایجاد مدیر جدید");
        binding.etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 11) {
                    hideKeyboard(AdminAdminsCreateActivity.this, binding.etPhone);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.btnConfirm: {
                String name = binding.etName.getText().toString().trim();
                String phone = binding.etPhone.getText().toString();
                if (name.length() == 0) {
                    toast("لطفا نام و نام خانوادگی را وارد کنید.");
                } else if (phone.length() != 11 || !phone.startsWith("09")) {
                    toast("لطفا شماره همراه را به درستی وارد کنید.");
                } else {
                    AdminAdminsStoreRequest request = new AdminAdminsStoreRequest();
                    request.setName(name);
                    request.setPhone(phone);
                    showLoadingFullPage();
                    presenter.requestAdminStore(request);
                }
                break;
            }

        }

    }

    @Override
    public void onResponseAdminAdminsStore(AdminAdminsStoreResponse adminAdminsStoreResponse) {
        dismissLoading();
        toast("با موفقیت ذخیره شد.");
        finish();
    }

    @Override
    public void onErrorResponseAdminAdminsStore(VolleyError volleyError) {
        dismissLoading();
        toast("امکان ذخیره مدیر وجود ندارد. مجددا امتحان کنید.");
    }
}