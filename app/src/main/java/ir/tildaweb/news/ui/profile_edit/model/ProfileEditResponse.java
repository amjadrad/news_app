package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationResponse;

public class ProfileEditResponse {

    @SerializedName("user")
    private SigninVerificationResponse.User user;

    public SigninVerificationResponse.User getUser() {
        return user;
    }

    public void setUser(SigninVerificationResponse.User user) {
        this.user = user;
    }
}