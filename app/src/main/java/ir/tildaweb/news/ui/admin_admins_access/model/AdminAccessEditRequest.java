package ir.tildaweb.news.ui.admin_admins_access.model;

import com.google.gson.annotations.SerializedName;

public class AdminAccessEditRequest {

    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }
}
