package ir.tildaweb.news.ui.admin_settings.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsEditRequest;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsEditResponse;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsUpdateRequest;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsUpdateResponse;


public class AdminSettingsActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminSettingsActivityPresenter(View view) {
        this.view = view;
    }

    public void requestAdminSettingsEdit(int adminId) {
        String url = Endpoints.BASE_URL_ADMIN_SETTINGS_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminSettingsEditResponse adminSettingsEditResponse = DataParser.fromJson(response, AdminSettingsEditResponse.class);
            view.onResponseAdminSettingsEdit(adminSettingsEditResponse);
        }
                , error -> {
            view.onErrorResponseAdminSettingsEdit(error);
        });
        AdminSettingsEditRequest request = new AdminSettingsEditRequest();
        request.setAdminId(adminId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestAdminSettingsUpdate(AdminSettingsUpdateRequest adminSettingsUpdateRequest) {
        String url = Endpoints.BASE_URL_ADMIN_SETTINGS_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminSettingsUpdateResponse adminSettingsUpdateResponse = DataParser.fromJson(response, AdminSettingsUpdateResponse.class);
            view.onResponseAdminSettingsUpdate(adminSettingsUpdateResponse);
        }
                , error -> {
            view.onErrorResponseAdminSettingsUpdate(error);
        });

        volleyRequestController.setBody(adminSettingsUpdateRequest);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseAdminSettingsEdit(AdminSettingsEditResponse response);

        void onErrorResponseAdminSettingsEdit(VolleyError volleyError);

        void onResponseAdminSettingsUpdate(AdminSettingsUpdateResponse response);

        void onErrorResponseAdminSettingsUpdate(VolleyError volleyError);

    }


}