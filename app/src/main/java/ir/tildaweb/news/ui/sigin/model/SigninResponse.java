package ir.tildaweb.news.ui.sigin.model;

import com.google.gson.annotations.SerializedName;

public class SigninResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("is_admin")
    private Boolean isAdmin;

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
