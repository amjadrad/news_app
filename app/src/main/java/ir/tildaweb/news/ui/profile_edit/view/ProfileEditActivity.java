package ir.tildaweb.news.ui.profile_edit.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterDropDown;
import ir.tildaweb.news.databinding.ActivityProfileUpdateBinding;
import ir.tildaweb.news.dialogs.DialogBottomSheetSelect;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.ui.profile_edit.model.CitiesResponse;
import ir.tildaweb.news.ui.profile_edit.model.ProfileEditResponse;
import ir.tildaweb.news.ui.profile_edit.model.ProfileUpdateRequest;
import ir.tildaweb.news.ui.profile_edit.model.ProfileUpdateResponse;
import ir.tildaweb.news.ui.profile_edit.model.ProvincesResponse;
import ir.tildaweb.news.ui.admin_news_edit.presenter.CityInfoPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.profile_edit.model.ContactUsStoreResponse;
import ir.tildaweb.news.ui.profile_edit.presenter.ProfileEditActivityPresenter;

public class ProfileEditActivity extends BaseActivity implements View.OnClickListener, ProfileEditActivityPresenter.View, CityInfoPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityProfileUpdateBinding binding;
    private ProfileEditActivityPresenter presenter;
    private CityInfoPresenter cityInfoPresenter;
    //    private AdapterDropDown adapterDropDownProvinces;
//    private AdapterDropDown adapterDropDownCities;
    private Integer provinceId, cityId;
    private ProvincesResponse provincesResponse;
    private CitiesResponse citiesResponse;

    private boolean isFirstTimeOpenPage = true;
    private ArrayList<DialogBottomSheetSelect.SelectObject> listCitiesSelect;
    private ArrayList<DialogBottomSheetSelect.SelectObject> listProvincesSelect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileUpdateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tvToolbarTitle.setText(String.format("%s", "ویرایش پروفایل"));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        presenter = new ProfileEditActivityPresenter(this);
        cityInfoPresenter = new CityInfoPresenter(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.coordinatorSelectCity.setOnClickListener(this);
        binding.coordinatorSelectProvince.setOnClickListener(this);

//        adapterDropDownProvinces = new AdapterDropDown(this, new ArrayList<>(), "provinces", this);
//        binding.recyclerProvinces.setLayoutManager(new LinearLayoutManager(this));
//        binding.recyclerProvinces.setAdapter(adapterDropDownProvinces);
//        adapterDropDownCities = new AdapterDropDown(this, new ArrayList<>(), "cities", this);
//        binding.recyclerCities.setLayoutManager(new LinearLayoutManager(this));
//        binding.recyclerCities.setAdapter(adapterDropDownCities);
        listCitiesSelect = new ArrayList<>();
        listProvincesSelect = new ArrayList<>();
        //Set data
        showLoadingFullPage();
        presenter.requestProfileEdit(getAppPreferencesHelper().getUserId());
        binding.etFullName.setText(getAppPreferencesHelper().getUserFullNamePref());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConfirm: {
                String name = binding.etFullName.getText().toString().trim();
                if (name.length() < 3) {
                    toast("لطفا نام خود را وارد کنید.");
                } else if (cityId == null) {
                    toast("لطفا استان و شهر خود را انتخاب کنید.");
                } else {
                    showLoadingFullPage();
                    ProfileUpdateRequest request = new ProfileUpdateRequest();
                    request.setName(name);
                    request.setUserId(getAppPreferencesHelper().getUserId());
                    request.setCityId(cityId);
                    presenter.requestProfileUpdate(request);
                }
                break;
            }
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.coordinatorSelectProvince: {
                String title = "استان خود را انتخاب کنید.";
                String searchHint = "استان خود را جستجو کنید:";
                DialogBottomSheetSelect dialog = new DialogBottomSheetSelect(title, searchHint, listProvincesSelect);
                dialog.setClickListener(id -> {
                    for (ProvincesResponse.Province province : provincesResponse.getProvinces())
                        if (province.getId() == id) {
                            provinceId = id;
                            binding.tvSelectedProvinceName.setText(String.valueOf(province.getTitle()));
                            cityInfoPresenter.requestCities(provinceId);
                            dialog.dismiss();
                            break;
                        }
                });
                dialog.show(getSupportFragmentManager(), null);
                break;
            }
            case R.id.coordinatorSelectCity: {
                String title = "شهر خود را انتخاب کنید.";
                String searchHint = "شهر خود را جستجو کنید:";
                DialogBottomSheetSelect dialog = new DialogBottomSheetSelect(title, searchHint, listCitiesSelect);
                dialog.setClickListener(id -> {
                    for (CitiesResponse.City city : citiesResponse.getCities())
                        if (city.getId() == id) {
                            cityId = id;
                            binding.tvSelectedCityName.setText(String.valueOf(city.getTitle()));
                            dialog.dismiss();
                            break;
                        }
                });
                dialog.show(getSupportFragmentManager(), null);
                break;
            }
        }

    }

    @Override
    public void onResponseProfileUpdate(ProfileUpdateResponse response) {
        dismissLoading();
        getAppPreferencesHelper().setUserFullNamePref(response.getUser().getFullName());
        getAppPreferencesHelper().setUserCityIdPref(response.getUser().getCity().getId());
        getAppPreferencesHelper().setUserCityTitlePref(response.getUser().getCity().getTitle());
        getAppPreferencesHelper().setUserCenterCityTitlePref(response.getUser().getCity().getProvince().getCenterCity().getTitle());

        DialogMessage dialogMessage = new DialogMessage(ProfileEditActivity.this, "ویرایش پروفایل", "ویرایش پروفایل با موفقیت انجام شد.");
        dialogMessage.setOnConfirmClickListener(view -> {
            dialogMessage.dismiss();
            finish();
        });
        dialogMessage.show();
    }

    @Override
    public void onErrorResponseProfileUpdate(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی پیش آمد. مجدد امتحان کنید.");

    }

    @Override
    public void onResponseProfileEdit(ProfileEditResponse response) {
        dismissLoading();
        if (isNotNull(response.getUser().getFullName()))
            binding.etFullName.setText(String.format("%s", response.getUser().getFullName()));
        if (response.getUser().getCity() != null) {
            cityId = response.getUser().getCity().getId();
            provinceId = response.getUser().getCity().getProvinceId();
            binding.tvSelectedCityName.setText(String.format("%s", response.getUser().getCity().getTitle()));
            binding.tvSelectedProvinceName.setText(String.format("%s", response.getUser().getCity().getProvince().getTitle()));
        }
        showLoadingFullPage();
        cityInfoPresenter.requestProvinces();
    }

    @Override
    public void onErrorResponseProfileEdit(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی پیش آمد. مجدد امتحان کنید.");
    }

    @Override
    public void onResponseProvinces(ProvincesResponse provincesResponse) {
        dismissLoading();
        this.provincesResponse = provincesResponse;
        listProvincesSelect.clear();
        Drawable icon = ContextCompat.getDrawable(this, R.drawable.ic_map_pin);
        for (ProvincesResponse.Province province : provincesResponse.getProvinces()) {
            DialogBottomSheetSelect.SelectObject selectObject = new DialogBottomSheetSelect.SelectObject();
            selectObject.setTitle(province.getTitle());
            selectObject.setId(province.getId());
            selectObject.setIcon(icon);
            selectObject.setSelected(false);
            if (provinceId != null) {
                if (provinceId.intValue() == province.getId()) {
                    selectObject.setSelected(true);
                    provinceId = province.getId();
                    binding.tvSelectedProvinceName.setText(String.valueOf(province.getTitle()));
                }
            }
            listProvincesSelect.add(selectObject);
        }
        showLoadingFullPage();
        if (provinceId == null) {
            cityInfoPresenter.requestCities(provincesResponse.getProvinces().get(0).getId());
        } else {
            cityInfoPresenter.requestCities(provinceId);
        }

    }

    @Override
    public void onErrorResponseProvinces(VolleyError volleyError) {
        dismissLoading();
    }

    @Override
    public void onResponseCities(CitiesResponse citiesResponse) {
        dismissLoading();
        this.citiesResponse = citiesResponse;
        listCitiesSelect.clear();
        Drawable icon = ContextCompat.getDrawable(this, R.drawable.ic_map_pin);
        for (CitiesResponse.City city : citiesResponse.getCities()) {
            DialogBottomSheetSelect.SelectObject selectObject = new DialogBottomSheetSelect.SelectObject();
            selectObject.setTitle(city.getTitle());
            selectObject.setId(city.getId());
            selectObject.setIcon(icon);
            selectObject.setSelected(false);
            if (cityId != null) {
                if (cityId.intValue() == city.getId()) {
                    selectObject.setSelected(true);
                    cityId = city.getId();
                    binding.tvSelectedCityName.setText(String.valueOf(city.getTitle()));
                }
            }
            listCitiesSelect.add(selectObject);
        }
        if (!isFirstTimeOpenPage) {
            binding.coordinatorSelectCity.callOnClick();
        } else {
            isFirstTimeOpenPage = false;
        }

    }


    @Override
    public void onErrorResponseCities(VolleyError volleyError) {
        dismissLoading();
    }

}