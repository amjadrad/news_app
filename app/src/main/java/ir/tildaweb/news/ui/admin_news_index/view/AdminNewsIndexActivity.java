package ir.tildaweb.news.ui.admin_news_index.view;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminNews;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityAdminNewsIndexBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.listeners.OnAdminNewsItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.admin_news_comments_index.view.AdminNewsCommentsIndexActivity;
import ir.tildaweb.news.ui.admin_news_create.view.AdminNewsCreateActivity;
import ir.tildaweb.news.ui.admin_news_edit.view.AdminNewsUpdateActivity;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsDeleteResponse;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsResponse;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsShowResponse;
import ir.tildaweb.news.ui.admin_news_index.presenter.AdminNewsIndexActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminNewsIndexActivity extends BaseActivity implements View.OnClickListener, AdminNewsIndexActivityPresenter.View, OnLoadMoreListener, OnAdminNewsItemClickListener {

    private String TAG = this.getClass().getName();
    private ActivityAdminNewsIndexBinding binding;
    private AdminNewsIndexActivityPresenter presenter;
    private AdapterAdminNews adapterNews;
    private int nextPage = 1;
    private int page = 1;
    private String search = null;
    private Integer categoryId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminNewsIndexBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminNewsIndexActivityPresenter(this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterNews = new AdapterAdminNews(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterNews);

        binding.linearNewsCreate.setOnClickListener(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("مدیریت خبرها");
        showLoadingFullPage();
        presenter.requestNewsSearch(page, search, categoryId, getAppPreferencesHelper().getAdminId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.linearNewsCreate: {
                if (getAppPreferencesHelper().getAdminAccessNewsInsert() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
                    startActivityForResult(new Intent(AdminNewsIndexActivity.this, AdminNewsCreateActivity.class), 1);
                } else {
                    toast("شما دسترسی افزودن خبر ندارید.");
                }
                break;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data != null && data.hasExtra("news_id")) {
                int newsId = data.getIntExtra("news_id", -1);
                if (newsId > 0) {
                    showLoadingFullPage();
                    presenter.requestNewsShow(newsId);
                }
            }
        }
    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestNewsSearch(nextPage, search, categoryId, getAppPreferencesHelper().getAdminId());
        }
    }

    @Override
    public void onResponseAdminNews(AdminNewsResponse newsResponse) {
        dismissLoading();
        if (newsResponse.getData().getNews().size() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
        adapterNews.addItems(newsResponse.getData().getNews());
        nextPage = VolleyRequestController.getNextPage(newsResponse.getData().getNextPageUrl());
    }

    @Override
    public void onErrorResponseAdminNews(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در دریافت اخبار به وجود آمد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseAdminNewsShow(AdminNewsShowResponse adminNewsShowResponse) {
        dismissLoading();
        adapterNews.addItem(adminNewsShowResponse.getNews());
        if (adapterNews.getItemCount() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponseAdminNewsShow(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت اطلاعات خبر وجود ندارد.");
    }

    @Override
    public void onResponseAdminNewsDelete(AdminNewsDeleteResponse adminNewsDeleteResponse) {
        dismissLoading();
        adapterNews.deleteItem(adminNewsDeleteResponse.getNewsId());
        if (adapterNews.getItemCount() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponseAdminNewsDelete(VolleyError volleyError) {
        dismissLoading();
        toast("حذف خبر با مشکل مواجه شد. مجددا امتحان کنید.");
    }

    @Override
    public void onEdit(int id) {
        if (getAppPreferencesHelper().getAdminAccessNewsUpdate() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            Intent intent = new Intent(AdminNewsIndexActivity.this, AdminNewsUpdateActivity.class);
            intent.putExtra("news_id", id);
            startActivity(intent);
        } else {
            toast("شما دسترسی ویرایش خبر ندارید.");
        }
    }

    @Override
    public void onComments(int id) {
        if (getAppPreferencesHelper().getAdminAccessNewsComments() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            Intent intent = new Intent(AdminNewsIndexActivity.this, AdminNewsCommentsIndexActivity.class);
            intent.putExtra("news_id", id);
            startActivity(intent);
        } else {
            toast("شما دسترسی مشاهده نظرات خبر را ندارید.");
        }
    }

    @Override
    public void onDelete(int id, String title) {
        if (getAppPreferencesHelper().getAdminAccessNewsDelete() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            String t = "حذف خبر";
            String description = String.format("آیا میخواهید خبر با عنوان (%s) را حذف کنید؟", title);
            DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(this, t, description);
            dialogConfirmMessage.setDanger();
            dialogConfirmMessage.setConfirmButtonText("حذف");
            dialogConfirmMessage.show();
            dialogConfirmMessage.setOnConfirmClickListener(view -> {
                dialogConfirmMessage.dismiss();
                showLoadingFullPage();
                presenter.requestNewsDelete(id, getAppPreferencesHelper().getAdminId());
            });
        } else {
            toast("شما دسترسی حذف خبر ندارید.");
        }
    }
}