package ir.tildaweb.news.ui.fragments.fragment_main_categories.model;

import com.google.gson.annotations.SerializedName;

public class CategoriesRequest {

    @SerializedName("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
