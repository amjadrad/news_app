package ir.tildaweb.news.ui.admin_categories_edit.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import java.io.File;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.data.network.FileUploader;
import ir.tildaweb.news.databinding.ActivityAdminCategoriesCreateBinding;
import ir.tildaweb.news.ui.admin_categories_create.view.AdminCategoriesCreateActivity;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateRequest;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateResponse;
import ir.tildaweb.news.ui.admin_categories_edit.presenter.AdminCategoriesUpdateActivityPresenter;
import ir.tildaweb.news.ui.admin_news_create.model.AdminCategoriesShowResponse;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.utils.FileUtils;
import ir.tildaweb.tilda_filepicker.TildaFilePicker;
import ir.tildaweb.tilda_filepicker.enums.FileMimeType;
import ir.tildaweb.tilda_filepicker.enums.FileType;
import ir.tildaweb.tilda_filepicker.models.FileModel;

public class AdminCategoriesUpdateActivity extends BaseActivity implements View.OnClickListener, AdminCategoriesUpdateActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminCategoriesCreateBinding binding;
    private AdminCategoriesUpdateActivityPresenter presenter;
    private Integer PICK_FILE_PERMISSION_CODE = 1001;
    private Integer uploadFileRequestId = 0;
    private String picture = null;
    private Integer categoryId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminCategoriesCreateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminCategoriesUpdateActivityPresenter(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.coordinatorSelectFile.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("ویرایش دسته بندی");
        categoryId = getIntent().getIntExtra("category_id", -1);
        presenter.requestCategoriesShow(categoryId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.btnConfirm: {
                if (binding.etTitle.getText() != null && binding.etTitle.getText().toString().trim().length() > 0) {
                    if (picture != null) {
                        showLoadingFullPage();
                        new FileUploader().upload(picture, uploadFileRequestId, new FileUploader.OnFileUploaderListener() {
                            @Override
                            public void onFileUploadProgress(int requestId, int percent) {
                            }

                            @Override
                            public void onFileUploaded(String fileName, int requestId) {
                                picture = fileName;
                                showLoadingFullPage();
                                requestUpdate();
                            }

                            @Override
                            public void onFileUploadError(String error) {
                                dismissLoading();
                                toast("امکان ارسال تصویر وجود ندارد.");
                            }
                        });

                    } else {
                        showLoadingFullPage();
                        requestUpdate();
                    }
                } else {
                    toast("لطفا عنوان دسته بندی را وارد کنید.");
                }
                break;
            }
            case R.id.coordinatorSelectFile: {
                if (checkReadExternalPermission(AdminCategoriesUpdateActivity.this, PICK_FILE_PERMISSION_CODE)) {
                    TildaFilePicker tildaFilePicker = new TildaFilePicker(AdminCategoriesUpdateActivity.this, new FileType[]{FileType.FILE_TYPE_IMAGE});
                    tildaFilePicker.setSingleChoice();
                    tildaFilePicker.setOnTildaFileSelectListener(list -> {
                        for (FileModel model : list) {
                            picture = model.getPath();
                            Glide.with(AdminCategoriesUpdateActivity.this).load(new File(model.getPath())).into(binding.imageView);
                            break;
                        }
                    });
                    tildaFilePicker.show(getSupportFragmentManager());
                }
                break;
            }
        }

    }

    private void requestUpdate() {
        AdminCategoriesUpdateRequest request = new AdminCategoriesUpdateRequest();
        request.setTitle(binding.etTitle.getText().toString().trim());
        request.setPicture(picture);
        request.setCategoryId(categoryId);
        presenter.requestCategoriesUpdate(request);
    }

    @Override
    public void onResponseAdminCategoriesUpdate(AdminCategoriesUpdateResponse adminCategoriesUpdateResponse) {
        dismissLoading();
        toast("با موفقیت ویرایش شد.");
        finish();
    }

    @Override
    public void onErrorResponseAdminCategoriesUpdate(VolleyError volleyError) {
        dismissLoading();
        toast("امکان ویرایش دسته بندی وجود ندارد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseAdminCategoriesShow(AdminCategoriesShowResponse adminCategoriesShowResponse) {
        dismissLoading();
        binding.etTitle.setText(adminCategoriesShowResponse.getCategory().getTitle());
        Glide.with(AdminCategoriesUpdateActivity.this).load(BuildConfig.FILE_URL + adminCategoriesShowResponse.getCategory().getPicture()).into(binding.imageView);
    }

    @Override
    public void onErrorResponseAdminCategoriesShow(VolleyError volleyError) {
        toast("امکان دریافت اطلاعات دسته بندی وجود ندارد. مجددا امتحان کنید.");
    }
}