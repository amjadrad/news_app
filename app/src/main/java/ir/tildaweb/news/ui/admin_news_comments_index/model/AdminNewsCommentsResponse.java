package ir.tildaweb.news.ui.admin_news_comments_index.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;

public class AdminNewsCommentsResponse {

    @SerializedName("data")
    private NewsCommentData data;

    public class NewsCommentData {

        @SerializedName("data")
        private ArrayList<NewsCommentsResponse.CommentData.NewsComment> comments;
        @SerializedName("next_page_url")
        private String nextPageUrl;

        public ArrayList<NewsCommentsResponse.CommentData.NewsComment> getComments() {
            return comments;
        }

        public void setComments(ArrayList<NewsCommentsResponse.CommentData.NewsComment> comments) {
            this.comments = comments;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }
    }

    public NewsCommentData getData() {
        return data;
    }

    public void setData(NewsCommentData data) {
        this.data = data;
    }
}