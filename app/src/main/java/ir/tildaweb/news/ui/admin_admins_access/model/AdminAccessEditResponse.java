package ir.tildaweb.news.ui.admin_admins_access.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AdminAccessEditResponse {

    @SerializedName("admin_access")
    private ArrayList<AdminAccess> adminAccess;

    public class AdminAccess {
        @SerializedName("id")
        private Integer id;
        @SerializedName("type")
        private String type;
        @SerializedName("actions")
        private String actions;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getActions() {
            return actions;
        }

        public void setActions(String actions) {
            this.actions = actions;
        }
    }

    public ArrayList<AdminAccess> getAdminAccess() {
        return adminAccess;
    }

    public void setAdminAccess(ArrayList<AdminAccess> adminAccess) {
        this.adminAccess = adminAccess;
    }
}
