package ir.tildaweb.news.ui.news_show.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.news_show.model.NewsShowRequest;
import ir.tildaweb.news.ui.news_show.model.NewsShowResponse;

public class NewsShowActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public NewsShowActivityPresenter(View view) {
        this.view = view;
    }

    public void requestNewsShow(int newsId, int userId) {

        String url = Endpoints.BASE_URL_NEWS_SHOW;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            NewsShowResponse resp = DataParser.fromJson(response, NewsShowResponse.class);
            view.onResponseNewsShow(resp);
        }
                , error -> {
            view.onErrorResponseNewsShow(error);
        });

        NewsShowRequest request = new NewsShowRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsLike(int newsId, int userId, boolean isLike) {

        String url = Endpoints.BASE_URL_NEWS_LIKE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            NewsLikeResponse newsLikeResponse = DataParser.fromJson(response, NewsLikeResponse.class);
            view.onResponseNewsLike(newsLikeResponse);
        }
                , error -> {
            view.onErrorResponseNewsLike(error);
        });

        NewsLikeRequest request = new NewsLikeRequest();
        request.setNewsId(newsId);
        request.setLike(isLike ? 1 : 0);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestNewsCommentCreate(int newsId, int userId, String comment) {

        String url = Endpoints.BASE_URL_NEWS_COMMENTS_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            NewsCommentStoreResponse newsCommentStoreResponse = DataParser.fromJson(response, NewsCommentStoreResponse.class);
            view.onResponseNewsCommentsStore(newsCommentStoreResponse);
        }
                , error -> {
            view.onErrorResponseNewsCommentsStore(error);
        });

        NewsCommentStoreRequest request = new NewsCommentStoreRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        request.setComment(comment);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestNewsFavorite(int newsId, int userId) {
        String url = Endpoints.BASE_URL_USER_NEWS_FAVORITES_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            UserNewsFavoriteResponse userNewsFavoriteResponse = DataParser.fromJson(response, UserNewsFavoriteResponse.class);
            view.onResponseUserNewsFavorite(userNewsFavoriteResponse);
        }
                , error -> {
            view.onErrorResponseUserNewsFavorite(error);
        });

        UserNewsFavoriteRequest request = new UserNewsFavoriteRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public interface View {

        void onResponseNewsShow(NewsShowResponse response);

        void onErrorResponseNewsShow(VolleyError volleyError);

        void onResponseNewsCommentsStore(NewsCommentStoreResponse newsCommentStoreResponse);

        void onErrorResponseNewsCommentsStore(VolleyError volleyError);

        void onResponseNewsLike(NewsLikeResponse newsLikeResponse);

        void onErrorResponseNewsLike(VolleyError volleyError);

        void onResponseUserNewsFavorite(UserNewsFavoriteResponse response);

        void onErrorResponseUserNewsFavorite(VolleyError volleyError);


    }
}