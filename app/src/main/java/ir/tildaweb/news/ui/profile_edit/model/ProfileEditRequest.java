package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

public class ProfileEditRequest {

    @SerializedName("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
