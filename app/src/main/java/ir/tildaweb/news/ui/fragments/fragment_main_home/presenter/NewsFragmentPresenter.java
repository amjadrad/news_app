package ir.tildaweb.news.ui.fragments.fragment_main_home.presenter;

import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.AppCommentStoreRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.AppCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsHotRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsHotResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsLikeResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsMostVisitResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsPosterResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UserNewsFavoriteResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.UsersCountResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.WeatherResponse;


public class NewsFragmentPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public NewsFragmentPresenter(View view) {
        this.view = view;
    }


    public void requestNewsSearch(int page, String search, Integer categoryId, int userId, boolean withMedia) {

        String url = Endpoints.BASE_URL_NEWS_SEARCH;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            NewsResponse newsResponse = DataParser.fromJson(response, NewsResponse.class);
            view.onResponseNews(newsResponse);
        }
                , error -> {
            view.onErrorResponseNews(error);
        });

        NewsRequest newsRequest = new NewsRequest();
        newsRequest.setCategoryId(categoryId);
        newsRequest.setUserId(userId);
        newsRequest.setPage(page);
        newsRequest.setSearch(search);
        newsRequest.setWithMedia(withMedia);
        volleyRequestController.setParameters(newsRequest);
        volleyRequestController.start();
    }

    public void requestNewsHot(boolean withMedia) {

        String url = Endpoints.BASE_URL_NEWS_HOT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            NewsHotResponse newsResponse = DataParser.fromJson(response, NewsHotResponse.class);
            view.onResponseNewsHot(newsResponse);
        }
                , error -> {
            view.onErrorResponseNewsHot(error);
        });
        NewsHotRequest request = new NewsHotRequest();
        request.setWithMedia(withMedia);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsMostVisit(boolean withMedia) {

        String url = Endpoints.BASE_URL_NEWS_MOST_VISIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            NewsMostVisitResponse newsResponse = DataParser.fromJson(response, NewsMostVisitResponse.class);
            view.onResponseNewsMostVisit(newsResponse);
        }
                , error -> {
            view.onErrorResponseNewsMostVisit(error);
        });
        NewsHotRequest request = new NewsHotRequest();
        request.setWithMedia(withMedia);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsPostersMain() {

        String url = Endpoints.BASE_URL_NEWS_POSTER_MAIN;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            NewsPosterResponse newsResponse = DataParser.fromJson(response, NewsPosterResponse.class);
            view.onResponseNewsPostersMain(newsResponse);
        }
                , error -> {
            view.onErrorResponseNewsPostersMain(error);
        });

        volleyRequestController.start();
    }

    public void requestUsersCount() {

        String url = Endpoints.BASE_URL_USER_COUNT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            UsersCountResponse usersCountResponse = DataParser.fromJson(response, UsersCountResponse.class);
            view.onResponseUsersCount(usersCountResponse);
        }
                , error -> {
            view.onErrorResponseUsersCount(error);
        });

        volleyRequestController.start();
    }

    public void requestAppCommentStore(int userId, String comment, float rating) {

        String url = Endpoints.BASE_URL_APP_COMMENT_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AppCommentStoreResponse appCommentStoreResponse = DataParser.fromJson(response, AppCommentStoreResponse.class);
            view.onResponseAppCommentStore(appCommentStoreResponse);
        }
                , error -> {
            view.onErrorResponseAppCommentStore(error);
        });

        AppCommentStoreRequest request = new AppCommentStoreRequest();
        request.setComment(comment);
        request.setRate(rating);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestNewsLike(int newsId, int userId, boolean isLike) {

        String url = Endpoints.BASE_URL_NEWS_LIKE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            NewsLikeResponse newsLikeResponse = DataParser.fromJson(response, NewsLikeResponse.class);
            view.onResponseNewsLike(newsLikeResponse);
        }
                , error -> {
            view.onErrorResponseNewsLike(error);
        });

        NewsLikeRequest request = new NewsLikeRequest();
        request.setNewsId(newsId);
        request.setLike(isLike ? 1 : 0);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestNewsVisit(int newsId, int userId) {
        String url = Endpoints.BASE_URL_NEWS_VISIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            NewsLikeResponse newsLikeResponse = DataParser.fromJson(response, NewsLikeResponse.class);
            view.onResponseNewsVisit(newsLikeResponse);
        }
                , error -> {
            view.onErrorResponseNewsVisit(error);
        });

        NewsLikeRequest request = new NewsLikeRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestNewsCommentCreate(int newsId, int userId, String comment) {

        String url = Endpoints.BASE_URL_NEWS_COMMENTS_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            NewsCommentStoreResponse newsCommentStoreResponse = DataParser.fromJson(response, NewsCommentStoreResponse.class);
            view.onResponseNewsCommentsStore(newsCommentStoreResponse);
        }
                , error -> {
            view.onErrorResponseNewsCommentsStore(error);
        });

        NewsCommentStoreRequest request = new NewsCommentStoreRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        request.setComment(comment);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestNewsFavorite(int newsId, int userId) {
        String url = Endpoints.BASE_URL_USER_NEWS_FAVORITES_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            UserNewsFavoriteResponse userNewsFavoriteResponse = DataParser.fromJson(response, UserNewsFavoriteResponse.class);
            view.onResponseUserNewsFavorite(userNewsFavoriteResponse);
        }
                , error -> {
            view.onErrorResponseUserNewsFavorite(error);
        });

        UserNewsFavoriteRequest request = new UserNewsFavoriteRequest();
        request.setNewsId(newsId);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }

    public void requestWeather(final String cityName) {
        String url = Endpoints.BASE_URL_WEATHER;
        try {
            url += "q=" + URLEncoder.encode(cityName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            url += "q=" + (cityName);
            e.printStackTrace();
        }

        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            WeatherResponse weatherResponse = DataParser.fromJson(response, WeatherResponse.class);
            view.onResponseWeathers(weatherResponse);
        }, volleyError -> {
            view.onErrorResponseWeathers(volleyError);
        });
        volleyRequestController.start();
    }


    public interface View {

        void onResponseNews(NewsResponse response);

        void onErrorResponseNews(VolleyError volleyError);

        void onResponseNewsHot(NewsHotResponse response);

        void onErrorResponseNewsHot(VolleyError volleyError);

        void onResponseNewsMostVisit(NewsMostVisitResponse response);

        void onErrorResponseNewsMostVisit(VolleyError volleyError);

        void onResponseNewsPostersMain(NewsPosterResponse response);

        void onErrorResponseNewsPostersMain(VolleyError volleyError);


        void onResponseNewsCommentsStore(NewsCommentStoreResponse response);

        void onErrorResponseNewsCommentsStore(VolleyError volleyError);

        void onResponseNewsLike(NewsLikeResponse response);

        void onErrorResponseNewsLike(VolleyError volleyError);

        void onResponseUserNewsFavorite(UserNewsFavoriteResponse response);

        void onErrorResponseUserNewsFavorite(VolleyError volleyError);

        void onResponseNewsVisit(NewsLikeResponse response);

        void onErrorResponseNewsVisit(VolleyError volleyError);

        void onResponseWeathers(WeatherResponse response);

        void onErrorResponseWeathers(VolleyError volleyError);

        void onResponseUsersCount(UsersCountResponse response);

        void onErrorResponseUsersCount(VolleyError volleyError);

        void onResponseAppCommentStore(AppCommentStoreResponse response);

        void onErrorResponseAppCommentStore(VolleyError volleyError);

    }


}