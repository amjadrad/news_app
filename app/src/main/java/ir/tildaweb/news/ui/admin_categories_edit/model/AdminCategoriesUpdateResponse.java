package ir.tildaweb.news.ui.admin_categories_edit.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;

public class AdminCategoriesUpdateResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("category")
    private CategoriesResponse.Category category;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CategoriesResponse.Category getCategory() {
        return category;
    }

    public void setCategory(CategoriesResponse.Category category) {
        this.category = category;
    }
}
