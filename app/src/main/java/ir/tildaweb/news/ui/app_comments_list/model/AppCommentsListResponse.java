package ir.tildaweb.news.ui.app_comments_list.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AppCommentsListResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("data")
    private AppCommentsData data;


    public class AppCommentsData {
        @SerializedName("data")
        private ArrayList<AppComment> appComments;
        @SerializedName("next_page_url")
        private String nextPageUrl;

        public class AppComment {
            @SerializedName("id")
            private Integer id;
            @SerializedName("rate")
            private Float rate;
            @SerializedName("comment")
            private String comment;
            @SerializedName("user")
            private User user;

            public class User {
                @SerializedName("id")
                private Integer id;
                @SerializedName("full_name")
                private String fullName;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getFullName() {
                    return fullName;
                }

                public void setFullName(String fullName) {
                    this.fullName = fullName;
                }
            }


            public Float getRate() {
                return rate;
            }

            public void setRate(Float rate) {
                this.rate = rate;
            }

            public User getUser() {
                return user;
            }

            public void setUser(User user) {
                this.user = user;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }
        }

        public ArrayList<AppComment> getAppComments() {
            return appComments;
        }

        public void setAppComments(ArrayList<AppComment> appComments) {
            this.appComments = appComments;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public AppCommentsData getData() {
        return data;
    }

    public void setData(AppCommentsData data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }
}
