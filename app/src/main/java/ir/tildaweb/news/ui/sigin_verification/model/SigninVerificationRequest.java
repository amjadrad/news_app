package ir.tildaweb.news.ui.sigin_verification.model;

import com.google.gson.annotations.SerializedName;

public class SigninVerificationRequest {

    @SerializedName("phone")
    private String phone;
    @SerializedName("verification_code")
    private String verificationCode;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
