package ir.tildaweb.news.ui.admin_settings.model;

import com.google.gson.annotations.SerializedName;

public class AdminSettingsUpdateResponse {

    @SerializedName("admin_settings")
    private AdminSettingsEditResponse.AdminSetting adminSetting;

    public AdminSettingsEditResponse.AdminSetting getAdminSetting() {
        return adminSetting;
    }

    public void setAdminSetting(AdminSettingsEditResponse.AdminSetting adminSetting) {
        this.adminSetting = adminSetting;
    }
}
