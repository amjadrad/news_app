package ir.tildaweb.news.ui.admin_settings.model;

import com.google.gson.annotations.SerializedName;

public class AdminSettingsEditResponse {

    @SerializedName("admin_settings")
    private AdminSetting adminSetting;

    public class AdminSetting {

        @SerializedName("is_show_user_count")
        private Boolean isShowUserCount;

        public Boolean getShowUserCount() {
            return isShowUserCount;
        }

        public void setShowUserCount(Boolean showUserCount) {
            isShowUserCount = showUserCount;
        }
    }

    public AdminSetting getAdminSetting() {
        return adminSetting;
    }

    public void setAdminSetting(AdminSetting adminSetting) {
        this.adminSetting = adminSetting;
    }
}
