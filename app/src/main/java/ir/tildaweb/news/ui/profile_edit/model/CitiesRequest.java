package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

public class CitiesRequest {

    @SerializedName("province_id")
    private Integer provinceId;

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }
}
