package ir.tildaweb.news.ui.user_news_index.model;

import com.google.gson.annotations.SerializedName;


public class UserNewsRequest {

    @SerializedName("page")
    private Integer page;
    @SerializedName("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
