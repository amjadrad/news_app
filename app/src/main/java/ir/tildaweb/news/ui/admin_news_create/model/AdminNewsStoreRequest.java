package ir.tildaweb.news.ui.admin_news_create.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AdminNewsStoreRequest {

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("link")
    private String link;
    @SerializedName("media")
    private ArrayList<MediaModel> media;//    Map<MediaModel, String> media;
    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("admin_id")
    private Integer adminId;
    @SerializedName("active")
    private Integer active;
    @SerializedName("hot")
    private Integer hot;
    @SerializedName("poster_main")
    private Integer posterMain;

    public class MediaModel {

        private Integer id;
        @SerializedName("path")
        private String path;
        @SerializedName("type")
        private String type;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public Integer getPosterMain() {
        return posterMain;
    }

    public void setPosterMain(Integer posterMain) {
        this.posterMain = posterMain;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<MediaModel> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<MediaModel> media) {
        this.media = media;
    }
}
