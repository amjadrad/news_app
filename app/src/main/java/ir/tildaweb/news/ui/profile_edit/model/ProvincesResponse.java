package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProvincesResponse {

    @SerializedName("provinces")
    private ArrayList<Province> provinces;

    public class Province {
        @SerializedName("id")
        private Integer id;
        @SerializedName("title")
        private String title;
        @SerializedName("center_city")
        private CitiesResponse.City centerCity;

        public CitiesResponse.City getCenterCity() {
            return centerCity;
        }

        public void setCenterCity(CitiesResponse.City centerCity) {
            this.centerCity = centerCity;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public ArrayList<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(ArrayList<Province> provinces) {
        this.provinces = provinces;
    }
}
