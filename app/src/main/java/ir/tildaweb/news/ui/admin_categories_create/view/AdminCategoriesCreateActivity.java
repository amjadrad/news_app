package ir.tildaweb.news.ui.admin_categories_create.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import java.io.File;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.R;
import ir.tildaweb.news.data.network.FileUploader;
import ir.tildaweb.news.databinding.ActivityAdminCategoriesCreateBinding;
import ir.tildaweb.news.ui.admin_categories_create.presenter.AdminCategoriesCreateActivityPresenter;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateRequest;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateResponse;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.tilda_filepicker.TildaFilePicker;
import ir.tildaweb.tilda_filepicker.enums.FileType;
import ir.tildaweb.tilda_filepicker.models.FileModel;


public class AdminCategoriesCreateActivity extends BaseActivity implements View.OnClickListener, AdminCategoriesCreateActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminCategoriesCreateBinding binding;
    private AdminCategoriesCreateActivityPresenter presenter;
    private Integer PICK_FILE_PERMISSION_CODE = 1001;
    private Integer uploadFileRequestId = 0;
    private String picture = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminCategoriesCreateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminCategoriesCreateActivityPresenter(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.coordinatorSelectFile.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("ایجاد دسته بندی جدید");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.btnConfirm: {
                if (binding.etTitle.getText() != null && binding.etTitle.getText().toString().trim().length() > 0) {
                    if (picture != null) {
                        showLoadingFullPage();
                        new FileUploader().upload(picture, uploadFileRequestId, new FileUploader.OnFileUploaderListener() {
                            @Override
                            public void onFileUploadProgress(int requestId, int percent) {
                            }

                            @Override
                            public void onFileUploaded(String fileName, int requestId) {
                                AdminCategoriesUpdateRequest request = new AdminCategoriesUpdateRequest();
                                request.setTitle(binding.etTitle.getText().toString().trim());
                                request.setPicture(fileName);
                                presenter.requestCategoriesStore(request);
                            }

                            @Override
                            public void onFileUploadError(String error) {
                                dismissLoading();
                                toast("امکان ارسال تصویر وجود ندارد.");
                            }
                        });

                    } else {
                        toast("لطفا تصویر دسته را انتخاب کنید.");
                    }
                } else {
                    toast("لطفا عنوان دسته بندی را وارد کنید.");
                }
                break;
            }
            case R.id.coordinatorSelectFile: {
                if (checkReadExternalPermission(AdminCategoriesCreateActivity.this, PICK_FILE_PERMISSION_CODE)) {
                    TildaFilePicker tildaFilePicker = new TildaFilePicker(AdminCategoriesCreateActivity.this, new FileType[]{FileType.FILE_TYPE_IMAGE});
                    tildaFilePicker.setSingleChoice();
                    tildaFilePicker.setOnTildaFileSelectListener(list -> {
                        for (FileModel model : list) {
                            picture = model.getPath();
                            Glide.with(AdminCategoriesCreateActivity.this).load(new File(model.getPath())).into(binding.imageView);
                            break;
                        }
                    });
                    tildaFilePicker.show(getSupportFragmentManager());
                }
                break;
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_FILE_PERMISSION_CODE) {
            binding.coordinatorSelectFile.callOnClick();
        }
    }

    @Override
    public void onResponseAdminCategoriesStore(AdminCategoriesUpdateResponse adminCategoriesUpdateResponse) {
        dismissLoading();
        toast("با موفقیت ذخیره شد.");
        finish();
    }

    @Override
    public void onErrorResponseAdminCategoriesStore(VolleyError volleyError) {
        dismissLoading();
        toast("امکان ذخیره دسته بندی وجود ندارد. مجددا امتحان کنید.");
    }
}