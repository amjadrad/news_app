package ir.tildaweb.news.ui.news_show.model;

import com.google.gson.annotations.SerializedName;

public class NewsShowRequest {

    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("user_id")
    private Integer userId;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}