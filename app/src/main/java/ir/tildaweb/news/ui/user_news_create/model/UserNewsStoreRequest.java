package ir.tildaweb.news.ui.user_news_create.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreRequest;

public class UserNewsStoreRequest {

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("media")
    private ArrayList<AdminNewsStoreRequest.MediaModel> media;//    Map<MediaModel, String> media;
    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("user_id")
    private Integer userId;

    public ArrayList<AdminNewsStoreRequest.MediaModel> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<AdminNewsStoreRequest.MediaModel> media) {
        this.media = media;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

}
