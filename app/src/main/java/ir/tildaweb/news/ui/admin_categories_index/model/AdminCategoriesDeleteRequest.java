package ir.tildaweb.news.ui.admin_categories_index.model;

import com.google.gson.annotations.SerializedName;

public class AdminCategoriesDeleteRequest {

    @SerializedName("category_id")
    private Integer categoryId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
