package ir.tildaweb.news.ui.admin_news_index.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;

public class AdminNewsShowRequest {

    @SerializedName("news_id")
    private Integer newsId;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}