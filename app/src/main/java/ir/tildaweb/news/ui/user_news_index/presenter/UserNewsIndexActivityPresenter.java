package ir.tildaweb.news.ui.user_news_index.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsDeleteRequest;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsDeleteResponse;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsRequest;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsResponse;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsShowRequest;
import ir.tildaweb.news.ui.user_news_index.model.UserNewsShowResponse;


public class UserNewsIndexActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public UserNewsIndexActivityPresenter(View view) {
        this.view = view;
    }


    public void requestNewsSearch(int page, int userId) {

        String url = Endpoints.BASE_URL_USER_NEWS_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            UserNewsResponse newsResponse = DataParser.fromJson(response, UserNewsResponse.class);
            view.onResponseUserNews(newsResponse);
        }
                , error -> {
            view.onErrorResponseUserNews(error);
        });

        UserNewsRequest newsRequest = new UserNewsRequest();
        newsRequest.setPage(page);
        newsRequest.setUserId(userId);
        volleyRequestController.setParameters(newsRequest);
        volleyRequestController.start();
    }

    public void requestNewsShow(int newsId) {
        String url = Endpoints.BASE_URL_USER_NEWS_SHOW;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            UserNewsShowResponse userNewsShowResponse = DataParser.fromJson(response, UserNewsShowResponse.class);
            view.onResponseUserNewsShow(userNewsShowResponse);
        }
                , error -> {
            view.onErrorResponseUserNewsShow(error);
        });

        UserNewsShowRequest request = new UserNewsShowRequest();
        request.setUserNewsId(newsId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsDelete(int newsId) {
        String url = Endpoints.BASE_URL_USER_NEWS_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            UserNewsDeleteResponse result = DataParser.fromJson(response, UserNewsDeleteResponse.class);
            view.onResponseUserNewsDelete(result);
        }
                , error -> {
            view.onErrorResponseUserNewsDelete(error);
        });

        UserNewsDeleteRequest userNewsDeleteRequest = new UserNewsDeleteRequest();
        userNewsDeleteRequest.setUserNewsId(newsId);
        volleyRequestController.setBody(userNewsDeleteRequest);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseUserNews(UserNewsResponse userNewsResponse);

        void onErrorResponseUserNews(VolleyError volleyError);

        void onResponseUserNewsShow(UserNewsShowResponse userNewsShowResponse);

        void onErrorResponseUserNewsShow(VolleyError volleyError);

        void onResponseUserNewsDelete(UserNewsDeleteResponse userNewsDeleteResponse);

        void onErrorResponseUserNewsDelete(VolleyError volleyError);


    }


}