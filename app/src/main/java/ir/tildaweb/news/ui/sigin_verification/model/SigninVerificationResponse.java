package ir.tildaweb.news.ui.sigin_verification.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.profile_edit.model.CitiesResponse;

public class SigninVerificationResponse {

    @SerializedName("user")
    private User user;
    @SerializedName("admin")
    private User admin;

    public class User {
        @SerializedName("id")
        private Integer id;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("api_token")
        private String apiToken;
        @SerializedName("phone")
        private String phone;
        @SerializedName("type")
        private String type;
        @SerializedName("city")
        private CitiesResponse.City city;

        public CitiesResponse.City getCity() {
            return city;
        }

        public void setCity(CitiesResponse.City city) {
            this.city = city;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getApiToken() {
            return apiToken;
        }

        public void setApiToken(String apiToken) {
            this.apiToken = apiToken;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
