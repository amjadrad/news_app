package ir.tildaweb.news.ui.admin_user_news_edit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreRequest;

public class AdminUserNewsUpdateRequest {

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("link")
    private String link;
    @SerializedName("media")
    private ArrayList<AdminNewsStoreRequest.MediaModel> media;
    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("admin_id")
    private Integer adminId;
    @SerializedName("user_news_id")
    private Integer userNewsId;
    @SerializedName("active")
    private Integer active;
    @SerializedName("hot")
    private Integer hot;

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getUserNewsId() {
        return userNewsId;
    }

    public void setUserNewsId(Integer userNewsId) {
        this.userNewsId = userNewsId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<AdminNewsStoreRequest.MediaModel> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<AdminNewsStoreRequest.MediaModel> media) {
        this.media = media;
    }
}
