package ir.tildaweb.news.ui.app_comments_list.model;

import com.google.gson.annotations.SerializedName;

public class AppCommentsListRequest {

    @SerializedName("page")
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
