package ir.tildaweb.news.ui.admin_categories_index.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_categories_index.model.AdminCategoriesDeleteRequest;
import ir.tildaweb.news.ui.admin_categories_index.model.AdminCategoriesDeleteResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;


public class AdminCategoriesActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminCategoriesActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesIndex() {
        String url = Endpoints.BASE_URL_CATEGORIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CategoriesResponse categoriesResponse = DataParser.fromJson(response, CategoriesResponse.class);
            view.onResponseAdminCategoriesIndex(categoriesResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesIndex(error);
        });
        volleyRequestController.start();
    }

    public void requestCategoriesDelete(int categoryId) {
        String url = Endpoints.BASE_URL_ADMIN_CATEGORIES_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminCategoriesDeleteResponse adminCategoriesDeleteResponse = DataParser.fromJson(response, AdminCategoriesDeleteResponse.class);
            view.onResponseAdminCategoriesDelete(adminCategoriesDeleteResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesDelete(error);
        });

        AdminCategoriesDeleteRequest adminCategoriesDeleteRequest = new AdminCategoriesDeleteRequest();
        adminCategoriesDeleteRequest.setCategoryId(categoryId);
        volleyRequestController.setBody(adminCategoriesDeleteRequest);
        volleyRequestController.start();
    }


    public interface View {
        void onResponseAdminCategoriesIndex(CategoriesResponse categoriesResponse);

        void onErrorResponseAdminCategoriesIndex(VolleyError volleyError);

        void onResponseAdminCategoriesDelete(AdminCategoriesDeleteResponse adminCategoriesDeleteResponse);

        void onErrorResponseAdminCategoriesDelete(VolleyError volleyError);
    }

}