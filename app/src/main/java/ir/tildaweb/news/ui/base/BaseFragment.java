package ir.tildaweb.news.ui.base;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import java.util.Stack;

import ir.tildaweb.news.R;
import ir.tildaweb.news.app.CacheManager;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.utils.ToastUtils;


public class BaseFragment extends Fragment {

    private ViewGroup loadingViewGroup;
    private Stack<View> loadingViewsStack;
    private AppPreferencesHelper appPreferencesHelper;
    private CacheManager cacheManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingViewsStack = new Stack<>();

    }


    public static void hideKeyboard(Activity activity, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showLoadingFullPage() {
        loadingViewGroup = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
        View loadingView = getLayoutInflater().inflate(R.layout.dialog_base_loading, null);
        CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loadingView.setLayoutParams(layoutParams);
        loadingViewsStack.push(loadingView);
        loadingViewGroup.addView(loadingView);
    }

    protected void dismissLoading() {
        if (!loadingViewsStack.empty()) {
            for (View view : loadingViewsStack) {
                loadingViewGroup.removeView(view);
            }
            loadingViewsStack.clear();
        }
    }

    protected AppPreferencesHelper getAppPreferencesHelper() {
        this.appPreferencesHelper = new AppPreferencesHelper(getContext());
        return this.appPreferencesHelper;
    }

    protected CacheManager getCacheManager() {
        this.cacheManager = new CacheManager(getContext());
        return this.cacheManager;
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    protected void toast(String message) {
        if (getContext() != null) {
            ToastUtils.toast(getContext(), message, ToastUtils.ToastType.NONE);
        }
    }

}
