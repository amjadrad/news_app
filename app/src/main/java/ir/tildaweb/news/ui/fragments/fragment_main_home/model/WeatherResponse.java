package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class WeatherResponse implements Serializable {

    private static double kelvinDif = 273.15;
    @SerializedName("cod")
    private Integer status;
    @SerializedName("list")
    private ArrayList<Weather> weather;
    @SerializedName("city")
    private CityClass city;

    public class CityClass {
        @SerializedName("name")
        private String name;
        @SerializedName("population")
        private Integer population;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getPopulation() {
            return population;
        }

        public void setPopulation(Integer population) {
            this.population = population;
        }
    }

    public class Weather {
        @SerializedName("main")
        private MainInfo mainInfo;
        @SerializedName("weather")
        private ArrayList<WeatherInfo> weatherInfo;
        @SerializedName("dt_txt")
        private String date;

        public class MainInfo {
            //Kelvin , cel = k-273.15
            @SerializedName("temp")
            private Double temp;
            @SerializedName("temp_min")
            private Double tempMin;
            @SerializedName("temp_max")
            private Double tempMax;
            @SerializedName("humidity")
            private Integer humidity;

            public Double getTemp() {
                return temp - kelvinDif;
            }

            public void setTemp(Double temp) {
                this.temp = temp;
            }

            public Double getTempMin() {
                return tempMin - kelvinDif;
            }

            public void setTempMin(Double tempMin) {
                this.tempMin = tempMin;
            }

            public Double getTempMax() {
                return tempMax - kelvinDif;
            }

            public void setTempMax(Double tempMax) {
                this.tempMax = tempMax;
            }

            public Integer getHumidity() {
                return humidity;
            }

            public void setHumidity(Integer humidity) {
                this.humidity = humidity;
            }
        }

        public class WeatherInfo {
            //range of thunderstorm, drizzle, rain, snow, clouds, atmosphere including extreme conditions like tornado, hurricane etc.
            @SerializedName("main")
            private String main;
            @SerializedName("description")
            private String description;
            @SerializedName("icon")
            private String icon;

            public String getMain() {
                return main;
            }

            public void setMain(String main) {
                this.main = main;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }
        }

        public MainInfo getMainInfo() {
            return mainInfo;
        }

        public void setMainInfo(MainInfo mainInfo) {
            this.mainInfo = mainInfo;
        }

        public ArrayList<WeatherInfo> getWeatherInfo() {
            return weatherInfo;
        }

        public void setWeatherInfo(ArrayList<WeatherInfo> weatherInfo) {
            this.weatherInfo = weatherInfo;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

    public static double getKelvinDif() {
        return kelvinDif;
    }

    public static void setKelvinDif(double kelvinDif) {
        WeatherResponse.kelvinDif = kelvinDif;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public CityClass getCity() {
        return city;
    }

    public void setCity(CityClass city) {
        this.city = city;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}