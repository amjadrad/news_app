package ir.tildaweb.news.ui.contact_us.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.contact_us.model.ContactUsStoreRequest;
import ir.tildaweb.news.ui.contact_us.model.ContactUsStoreResponse;
import ir.tildaweb.news.ui.sigin.model.SigninRequest;
import ir.tildaweb.news.ui.sigin.model.SigninResponse;


public class ContactUsActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public ContactUsActivityPresenter(View view) {
        this.view = view;
    }


    public void requestContactUsStore(String title, String description, int userId) {
        String url = Endpoints.BASE_URL_CONTACT_US;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            ContactUsStoreResponse contactUsStoreResponse = DataParser.fromJson(response, ContactUsStoreResponse.class);
            view.onResponseContactUsStore(contactUsStoreResponse);
        }
                , error -> {
            view.onErrorResponseContactUsStore(error);
        });

        ContactUsStoreRequest request = new ContactUsStoreRequest();
        request.setTitle(title);
        request.setDescription(description);
        request.setUserId(userId);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseContactUsStore(ContactUsStoreResponse response);

        void onErrorResponseContactUsStore(VolleyError volleyError);

    }


}