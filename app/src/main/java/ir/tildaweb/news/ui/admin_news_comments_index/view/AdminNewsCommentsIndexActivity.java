package ir.tildaweb.news.ui.admin_news_comments_index.view;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminNews;
import ir.tildaweb.news.adapter.AdapterAdminNewsComments;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityAdminNewsCommentsIndexBinding;
import ir.tildaweb.news.databinding.ActivityAdminNewsIndexBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.listeners.OnAdminNewsCommentItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.admin_news_comments_index.model.AdminNewsCommentsActiveResponse;
import ir.tildaweb.news.ui.admin_news_comments_index.model.AdminNewsCommentsResponse;
import ir.tildaweb.news.ui.admin_news_comments_index.presenter.AdminNewsCommentsIndexActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminNewsCommentsIndexActivity extends BaseActivity implements View.OnClickListener, AdminNewsCommentsIndexActivityPresenter.View, OnLoadMoreListener, OnAdminNewsCommentItemClickListener {

    private String TAG = this.getClass().getName();
    private ActivityAdminNewsCommentsIndexBinding binding;
    private AdminNewsCommentsIndexActivityPresenter presenter;
    private AdapterAdminNewsComments adapterComments;
    private int nextPage = 1;
    private int page = 1;
    private int newsId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminNewsCommentsIndexBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminNewsCommentsIndexActivityPresenter(this);
        newsId = getIntent().getIntExtra("news_id", -1);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterComments = new AdapterAdminNewsComments(this, binding.recyclerView, new ArrayList<>(), this, this);
        binding.recyclerView.setAdapter(adapterComments);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("مدیریت نظرات خبر");
        showLoadingFullPage();
        presenter.requestComments(newsId, page);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
        }

    }

    @Override
    public void onLoadMore() {
        if (nextPage > page) {
            page = nextPage;
            presenter.requestComments(newsId, nextPage);
        }
    }


    @Override
    public void onResponseAdminNewsComments(AdminNewsCommentsResponse response) {
        dismissLoading();
        adapterComments.addItems(response.getData().getComments());
        nextPage = VolleyRequestController.getNextPage(response.getData().getNextPageUrl());
    }

    @Override
    public void onErrorResponseAdminNewsComments(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در دریافت نظرات به وجود آمد. مجددا امتحان کنید.");
    }

    @Override
    public void onResponseAdminNewsCommentActive(AdminNewsCommentsActiveResponse response) {
        dismissLoading();
        if (response.getStatus() == 200) {
            adapterComments.changeActive(response.getNewsCommentId(), response.getActive());
        } else {
            toast("امکان تغییر وضعیت نمایش کامنت وجود ندارد. مجدد امتحان کنید.");
        }
    }

    @Override
    public void onErrorResponseAdminNewsCommentActive(VolleyError volleyError) {
        dismissLoading();
        toast("امکان تغییر وضعیت نمایش کامنت وجود ندارد. مجدد امتحان کنید.");
    }


    @Override
    public void onActiveChange(int newsCommentId, boolean isActive) {
        showLoadingFullPage();
        presenter.requestCommentActiveChange(newsCommentId, isActive);
    }

    @Override
    public void onCommentSeen(int newsCommentId) {
        presenter.requestCommentSeen(newsCommentId);
    }
}