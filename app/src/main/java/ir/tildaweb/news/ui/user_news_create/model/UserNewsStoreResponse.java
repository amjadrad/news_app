package ir.tildaweb.news.ui.user_news_create.model;

import com.google.gson.annotations.SerializedName;

public class UserNewsStoreResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("user_news_id")
    private Integer userNewsId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserNewsId() {
        return userNewsId;
    }

    public void setUserNewsId(Integer userNewsId) {
        this.userNewsId = userNewsId;
    }
}