package ir.tildaweb.news.ui.base;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;

import java.util.Stack;

import ir.tildaweb.news.R;
import ir.tildaweb.news.app.CacheManager;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.utils.ToastUtils;

public class BaseActivity extends AppCompatActivity {

    private AppPreferencesHelper appPreferencesHelper;
    private String TAG = this.getClass().getName();
    private Stack<View> loadingViewsStack;
    private ViewGroup loadingViewGroup;
    private CacheManager cacheManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        themeSettings();
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.animation_fade_in, R.anim.animation_fade_out);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        loadingViewsStack = new Stack<>();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);


    }

    protected void themeSettings() {
        int theme = getAppPreferencesHelper().getThemePref();
        int font = getAppPreferencesHelper().getFontPref();
        if (theme == 0) {
            setTheme(R.style.AppTheme);
        } else if (theme == 1) {
            setTheme(R.style.AppTheme1);
        } else if (theme == 2) {
            setTheme(R.style.AppTheme2);
        } else if (theme == 3) {
            setTheme(R.style.AppTheme3);
        } else if (theme == 4) {
            setTheme(R.style.AppTheme4);
        }
        switch (font) {
            case 1: {
                getTheme().applyStyle(R.style.fontIranYekan, true);
                break;
            }
            case 2: {
                getTheme().applyStyle(R.style.fontKalameh, true);
                break;
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void setFullScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void hideKeyboard(Activity activity, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismissLoading();
        overridePendingTransition(R.anim.animation_fade_in, R.anim.animation_fade_out);
    }

    protected void showLoadingFullPage() {
        loadingViewGroup = getWindow().getDecorView().findViewById(android.R.id.content);
        View loadingView = getLayoutInflater().inflate(R.layout.dialog_base_loading, null);
        CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        loadingView.setLayoutParams(layoutParams);
        loadingViewsStack.push(loadingView);
        loadingViewGroup.addView(loadingView);
    }

    protected void dismissLoading() {
        if (!loadingViewsStack.empty()) {
            for (View view : loadingViewsStack) {
                loadingViewGroup.removeView(view);
            }
            loadingViewsStack.clear();
        }
    }

    protected AppPreferencesHelper getAppPreferencesHelper() {
        this.appPreferencesHelper = new AppPreferencesHelper(this);
        return this.appPreferencesHelper;
    }

    protected CacheManager getCacheManager() {
        this.cacheManager = new CacheManager(this);
        return this.cacheManager;
    }

    protected boolean checkReadExternalPermission(Activity activity, int PICK_IMAGE) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE);
            return false;
        } else {
            return true;
        }
    }


    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    protected void toast(String message) {
        if (getApplicationContext() != null) {
            ToastUtils.toast(getApplicationContext(), message, ToastUtils.ToastType.NONE);
        }
    }

    public <T> boolean isNotNull(T t) {
        return t != null;
    }
}
