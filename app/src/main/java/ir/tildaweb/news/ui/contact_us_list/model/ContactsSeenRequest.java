package ir.tildaweb.news.ui.contact_us_list.model;

import com.google.gson.annotations.SerializedName;

public class ContactsSeenRequest {

    @SerializedName("contact_id")
    private Integer contactId;

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }
}
