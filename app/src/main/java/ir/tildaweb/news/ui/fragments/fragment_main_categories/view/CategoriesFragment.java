package ir.tildaweb.news.ui.fragments.fragment_main_categories.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.adapter.AdapterCategories;
import ir.tildaweb.news.databinding.FragmentMainCategoriesBinding;
import ir.tildaweb.news.ui.base.BaseFragment;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.presenter.CategoriesFragmentPresenter;


public class CategoriesFragment extends BaseFragment implements View.OnClickListener, CategoriesFragmentPresenter.View {

    private FragmentMainCategoriesBinding binding;
    private String TAG = this.getClass().getName();

    private CategoriesFragmentPresenter categoriesFragmentPresenter;
    private AdapterCategories adapterCategories;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = FragmentMainCategoriesBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        categoriesFragmentPresenter = new CategoriesFragmentPresenter(this);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        adapterCategories = new AdapterCategories(getContext(), new ArrayList<>());
        binding.recyclerView.setAdapter(adapterCategories);
        binding.swipeRefresh.setOnRefreshListener(() -> {
            categoriesFragmentPresenter.requestCategories(getAppPreferencesHelper().getUserId());
        });
        categoriesFragmentPresenter.requestCategories(getAppPreferencesHelper().getUserId());
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    @Override
    public void onResponseCategories(CategoriesResponse categoriesResponse) {
        adapterCategories.clearAll();
        binding.swipeRefresh.setRefreshing(false);
        adapterCategories.addItems(categoriesResponse.getCategories());
    }

    @Override
    public void onErrorResponseCategories(VolleyError volleyError) {
        toast("امکان دریافت لیست دسته بندی اخبار وجود ندارد.");
    }
}
