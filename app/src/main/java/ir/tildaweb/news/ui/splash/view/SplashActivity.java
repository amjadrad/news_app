package ir.tildaweb.news.ui.splash.view;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.VolleyError;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.app.App;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.databinding.ActivitySplashBinding;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditResponse;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.main.view.MainActivity;
import ir.tildaweb.news.ui.sigin.view.SignActivity;
import ir.tildaweb.news.ui.splash.model.AdminAccessRequest;
import ir.tildaweb.news.ui.splash.presenter.SplashActivityPresenter;

public class SplashActivity extends BaseActivity implements SplashActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivitySplashBinding binding;
    private SplashActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        presenter = new SplashActivityPresenter(this);
        setContentView(binding.getRoot());
        binding.tvVersion.setText(String.format("نسخه " + BuildConfig.VERSION_NAME));
        if (getAppPreferencesHelper().getLoginPref() && getAppPreferencesHelper().getUserIsAdminPref() && getAppPreferencesHelper().getAdminId() > 0) {
            AdminAccessRequest request = new AdminAccessRequest();
            request.setAdminId(getAppPreferencesHelper().getAdminId());
            presenter.requestAdminAccess(request);
        } else {
            gotoNextPage();
        }
    }

    private void gotoNextPage() {
        new Handler().postDelayed(() -> {
            if (getAppPreferencesHelper().getLoginPref()) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, SignActivity.class));
            }
            finish();
        }, 2500);
    }

    @Override
    public void onResponseUserInfo(AdminAccessEditResponse response) {

        AppPreferencesHelper appPreferencesHelper = getAppPreferencesHelper();
        for (AdminAccessEditResponse.AdminAccess adminAccess : response.getAdminAccess()) {
            if (adminAccess.getActions() != null) {
                String[] actions = adminAccess.getActions().split(",");
                switch (adminAccess.getType()) {
                    case "news":
                        for (String action : actions) {
                            switch (action) {
                                case "insert":
                                    appPreferencesHelper.setAdminAccessNewsInsert(true);
                                    break;
                                case "update":
                                    appPreferencesHelper.setAdminAccessNewsUpdate(true);
                                    break;
                                case "delete":
                                    appPreferencesHelper.setAdminAccessNewsDelete(true);
                                    break;
                                case "reviews":
                                    appPreferencesHelper.setAdminAccessNewsComments(true);
                                    break;
                            }
                        }
                        break;
                    case "categories":
                        for (String action : actions) {
                            switch (action) {
                                case "insert":
                                    appPreferencesHelper.setAdminAccessCategoriesInsert(true);
                                    break;
                                case "update":
                                    appPreferencesHelper.setAdminAccessCategoriesUpdate(true);
                                    break;
                                case "delete":
                                    appPreferencesHelper.setAdminAccessCategoriesDelete(true);
                                    break;
                            }
                        }
                        break;
                    case "user_news":
                        for (String action : actions) {
                            switch (action) {
                                case "update":
                                    appPreferencesHelper.setAdminAccessUserNewsUpdate(true);
                                    break;
                                case "delete":
                                    appPreferencesHelper.setAdminAccessUserNewsDelete(true);
                                    break;
                            }
                        }
                        break;
                    case "app_comments":
                        for (String action : actions) {
                            if ("all".equals(action)) {
                                appPreferencesHelper.setAdminAccessAppComments(true);
                            }
                        }
                        break;
                    case "user_contacts":
                        for (String action : actions) {
                            if ("all".equals(action)) {
                                appPreferencesHelper.setAdminAccessUserContacts(true);
                            }
                        }
                        break;
                }
            }
        }

        gotoNextPage();
    }

    @Override
    public void onErrorResponseUserInfo(VolleyError volleyError) {
        gotoNextPage();
    }
}