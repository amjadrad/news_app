package ir.tildaweb.news.ui.admin_user_news_index.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;

public class AdminNewsShowResponse {

    @SerializedName("news")
    private NewsResponse.NewsData.News news;

    public NewsResponse.NewsData.News getNews() {
        return news;
    }

    public void setNews(NewsResponse.NewsData.News news) {
        this.news = news;
    }
}