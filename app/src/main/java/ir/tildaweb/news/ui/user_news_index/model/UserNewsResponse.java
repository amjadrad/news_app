package ir.tildaweb.news.ui.user_news_index.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsResponse;

public class UserNewsResponse {

    @SerializedName("data")
    private NewsData data;

    public class NewsData {

        @SerializedName("data")
        private ArrayList<NewsResponse.NewsData.News> news;
        @SerializedName("next_page_url")
        private String nextPageUrl;

        public ArrayList<NewsResponse.NewsData.News> getNews() {
            return news;
        }

        public void setNews(ArrayList<NewsResponse.NewsData.News> news) {
            this.news = news;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }
    }

    public NewsData getData() {
        return data;
    }

    public void setData(NewsData data) {
        this.data = data;
    }

}
