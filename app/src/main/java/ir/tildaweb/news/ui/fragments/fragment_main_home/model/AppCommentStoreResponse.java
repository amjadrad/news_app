package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;


public class AppCommentStoreResponse {

    public AppCommentStoreResponse() {
        this.withMedia = true;
    }

    @SerializedName("search")
    private String search;
    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("page")
    private Integer page;
    @SerializedName("user_id")
    private Integer userId;
    @SerializedName("with_media")
    private Boolean withMedia;

    public Boolean getWithMedia() {
        return withMedia;
    }

    public void setWithMedia(Boolean withMedia) {
        this.withMedia = withMedia;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
