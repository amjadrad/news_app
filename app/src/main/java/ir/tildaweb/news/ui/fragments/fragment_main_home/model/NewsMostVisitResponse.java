package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewsMostVisitResponse {

    @SerializedName("data")
    private ArrayList<NewsResponse.NewsData.News> news;

    public ArrayList<NewsResponse.NewsData.News> getNews() {
        return news;
    }

    public void setNews(ArrayList<NewsResponse.NewsData.News> news) {
        this.news = news;
    }
}
