package ir.tildaweb.news.ui.admin_admins_create.model;

import com.google.gson.annotations.SerializedName;

public class AdminAdminsStoreRequest {

    @SerializedName("full_name")
    private String name;
    @SerializedName("phone")
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
