package ir.tildaweb.news.ui.admin_admins_access.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;

import java.util.HashSet;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.ActivityAdminAdminsAccessBinding;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditRequest;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditResponse;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessUpdateRequest;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessUpdateResponse;
import ir.tildaweb.news.ui.admin_admins_access.presenter.AdminAdminsAccessActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminAdminsAccessActivity extends BaseActivity implements View.OnClickListener, AdminAdminsAccessActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminAdminsAccessBinding binding;
    private AdminAdminsAccessActivityPresenter presenter;
    private String adminName;
    private Integer adminId;
    private boolean isNewsInsert, isNewsUpdate, isNewsDelete, isNewsComments;
    private boolean isUserNewsUpdate, isUserNewsDelete;
    private boolean isUserContacts, isAppComments;
    private boolean isCategoryInsert, isCategoryUpdate, isCategoryDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminAdminsAccessBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminAdminsAccessActivityPresenter(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("تعیین دسترسی مدیر");

        adminId = getIntent().getIntExtra("admin_id", -1);
        adminName = getIntent().getStringExtra("admin_fullname");
        binding.tvAdminName.setText(String.format("%s", adminName));

        AdminAccessEditRequest adminAccessEditRequest = new AdminAccessEditRequest();
        adminAccessEditRequest.setAdminId(adminId);
        presenter.requestAdminAccessEdit(adminAccessEditRequest);

        binding.toggleNewsInsert.setOnToggleChanged(on -> isNewsInsert = on);
        binding.toggleNewsUpdate.setOnToggleChanged(on -> isNewsUpdate = on);
        binding.toggleNewsDelete.setOnToggleChanged(on -> isNewsDelete = on);
        binding.toggleNewsComments.setOnToggleChanged(on -> isNewsComments = on);
        binding.toggleUserNewsUpdate.setOnToggleChanged(on -> isUserNewsUpdate = on);
        binding.toggleUserNewsDelete.setOnToggleChanged(on -> isUserNewsDelete = on);
        binding.toggleCategoryInsert.setOnToggleChanged(on -> isCategoryInsert = on);
        binding.toggleCategoryUpdate.setOnToggleChanged(on -> isCategoryUpdate = on);
        binding.toggleCategoryDelete.setOnToggleChanged(on -> isCategoryDelete = on);
        binding.toggleUserContacts.setOnToggleChanged(on -> isUserContacts = on);
        binding.toggleAppComments.setOnToggleChanged(on -> isAppComments = on);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.btnConfirm: {
                AdminAccessUpdateRequest request = new AdminAccessUpdateRequest();
                request.setAdminId(adminId);
                HashSet<String> setNews = new HashSet<>();
                if (isNewsInsert) {
                    setNews.add("insert");
                }
                if (isNewsUpdate) {
                    setNews.add("update");
                }
                if (isNewsDelete) {
                    setNews.add("delete");
                }
                if (isNewsComments) {
                    setNews.add("reviews");
                }
                request.setNewsActions(setNews.toString().replace("[", "").replace("]", "").replace(" ", ""));
                HashSet<String> setCategories = new HashSet<>();
                if (isCategoryInsert) {
                    setCategories.add("insert");
                }
                if (isCategoryUpdate) {
                    setCategories.add("update");
                }
                if (isCategoryDelete) {
                    setCategories.add("delete");
                }
                request.setCategoriesActions(setCategories.toString().replace("[", "").replace("]", "").replace(" ", ""));
                HashSet<String> setUserNews = new HashSet<>();
                if (isUserNewsUpdate) {
                    setUserNews.add("update");
                }
                if (isUserNewsDelete) {
                    setUserNews.add("delete");
                }
                request.setUserNewsActions(setUserNews.toString().replace("[", "").replace("]", "").replace(" ", ""));

                if (isUserContacts) {
                    request.setUserContactsActions("all");
                }
                if (isAppComments) {
                    request.setAppCommentsActions("all");
                }
                showLoadingFullPage();
                presenter.requestAdminAccessUpdate(request);
                break;
            }

        }

    }


    @Override
    public void onResponseAdminAccessEdit(AdminAccessEditResponse adminAccessEditResponse) {
        for (AdminAccessEditResponse.AdminAccess adminAccess : adminAccessEditResponse.getAdminAccess()) {
            if (adminAccess.getActions() != null) {
                String[] actions = adminAccess.getActions().split(",");
                if (adminAccess.getType().equals("news")) {
                    for (String action : actions) {
                        if (action.equals("insert")) {
                            isNewsInsert = true;
                            binding.toggleNewsInsert.setToggleOn();
                        } else if (action.equals("update")) {
                            isNewsUpdate = true;
                            binding.toggleNewsUpdate.setToggleOn();
                        } else if (action.equals("delete")) {
                            isNewsDelete = true;
                            binding.toggleNewsDelete.setToggleOn();
                        } else if (action.equals("reviews")) {
                            isNewsComments = true;
                            binding.toggleNewsComments.setToggleOn();
                        }
                    }
                } else if (adminAccess.getType().equals("categories")) {
                    for (String action : actions) {
                        if (action.equals("insert")) {
                            isCategoryInsert = true;
                            binding.toggleCategoryInsert.setToggleOn();
                        } else if (action.equals("update")) {
                            isCategoryUpdate = true;
                            binding.toggleCategoryUpdate.setToggleOn();
                        } else if (action.equals("delete")) {
                            isCategoryDelete = true;
                            binding.toggleCategoryDelete.setToggleOn();
                        }
                    }
                } else if (adminAccess.getType().equals("user_news")) {
                    for (String action : actions) {
                        if (action.equals("update")) {
                            isUserNewsUpdate = true;
                            binding.toggleUserNewsUpdate.setToggleOn();
                        } else if (action.equals("delete")) {
                            isUserNewsDelete = true;
                            binding.toggleUserNewsDelete.setToggleOn();
                        }
                    }
                } else if (adminAccess.getType().equals("app_comments")) {
                    for (String action : actions) {
                        if (action.equals("all")) {
                            isAppComments = true;
                            binding.toggleAppComments.setToggleOn();
                            break;
                        }
                    }
                } else if (adminAccess.getType().equals("user_contacts")) {
                    for (String action : actions) {
                        if (action.equals("all")) {
                            isUserContacts = true;
                            binding.toggleUserContacts.setToggleOn();
                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onErrorResponseAdminAccessEdit(VolleyError volleyError) {
        toast("امکان دریافت اطلاعات دسترسی وجود ندارد. دوباره امتحان کنید.");
    }

    @Override
    public void onResponseAdminAccessUpdate(AdminAccessUpdateResponse adminAccessUpdateResponse) {
        String title = "ویرایش دسترسی مدیر";
        String desc = "اطلاعات دسترسی مدیر با موفقیت ذخیره شد.";
        DialogMessage dialogMessage = new DialogMessage(AdminAdminsAccessActivity.this, title, desc);
        dialogMessage.setOnConfirmClickListener(view -> {
            dialogMessage.dismiss();
            finish();
        });
        dialogMessage.show();
    }

    @Override
    public void onErrorResponseAdminAccessUpdate(VolleyError volleyError) {
        toast("امکان ویرایش اطلاعات دسترسی وجود ندارد. دوباره امتحان کنید.");
    }
}