package ir.tildaweb.news.ui.profile_edit.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.profile_edit.model.ProfileEditRequest;
import ir.tildaweb.news.ui.profile_edit.model.ProfileEditResponse;
import ir.tildaweb.news.ui.profile_edit.model.ProfileUpdateRequest;
import ir.tildaweb.news.ui.profile_edit.model.ContactUsStoreResponse;
import ir.tildaweb.news.ui.profile_edit.model.ProfileUpdateResponse;


public class ProfileEditActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public ProfileEditActivityPresenter(View view) {
        this.view = view;
    }

    public void requestProfileEdit(int userId) {
        String url = Endpoints.BASE_URL_USER_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            ProfileEditResponse profileEditResponse = DataParser.fromJson(response, ProfileEditResponse.class);
            view.onResponseProfileEdit(profileEditResponse);
        }
                , error -> {
            view.onErrorResponseProfileEdit(error);
        });
        ProfileEditRequest request = new ProfileEditRequest();
        request.setUserId(userId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }


    public void requestProfileUpdate(ProfileUpdateRequest request) {
        String url = Endpoints.BASE_URL_USER_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            ProfileUpdateResponse profileUpdateResponse = DataParser.fromJson(response, ProfileUpdateResponse.class);
            view.onResponseProfileUpdate(profileUpdateResponse);
        }
                , error -> {
            view.onErrorResponseProfileUpdate(error);
        });
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseProfileUpdate(ProfileUpdateResponse response);

        void onErrorResponseProfileUpdate(VolleyError volleyError);

        void onResponseProfileEdit(ProfileEditResponse response);

        void onErrorResponseProfileEdit(VolleyError volleyError);

    }


}