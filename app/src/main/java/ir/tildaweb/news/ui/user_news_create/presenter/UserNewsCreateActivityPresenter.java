package ir.tildaweb.news.ui.user_news_create.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.user_news_create.model.UserNewsStoreRequest;
import ir.tildaweb.news.ui.user_news_create.model.UserNewsStoreResponse;


public class UserNewsCreateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public UserNewsCreateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesIndex() {
        String url = Endpoints.BASE_URL_CATEGORIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CategoriesResponse categoriesResponse = DataParser.fromJson(response, CategoriesResponse.class);
            view.onResponseUserCategoriesIndex(categoriesResponse);
        }
                , error -> {
            view.onErrorResponseUserCategoriesIndex(error);
        });
        volleyRequestController.start();
    }

    public void requestNewsStore(UserNewsStoreRequest userNewsStoreRequest) {
        String url = Endpoints.BASE_URL_USER_NEWS_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            UserNewsStoreResponse userNewsStoreResponse = DataParser.fromJson(response, UserNewsStoreResponse.class);
            view.onResponseUserNewsStore(userNewsStoreResponse);
        }
                , error -> {
            view.onErrorResponseUserNewsStore(error);
        });

        volleyRequestController.setBody(userNewsStoreRequest);
        volleyRequestController.start();
    }


    public interface View {
        void onResponseUserCategoriesIndex(CategoriesResponse categoriesResponse);

        void onErrorResponseUserCategoriesIndex(VolleyError volleyError);

        void onResponseUserNewsStore(UserNewsStoreResponse userNewsStoreResponse);

        void onErrorResponseUserNewsStore(VolleyError volleyError);
    }

}