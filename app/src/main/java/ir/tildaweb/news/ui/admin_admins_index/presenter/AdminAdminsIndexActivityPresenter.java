package ir.tildaweb.news.ui.admin_admins_index.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminAdminsDeleteRequest;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminAdminsDeleteResponse;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminsResponse;


public class AdminAdminsIndexActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminAdminsIndexActivityPresenter(View view) {
        this.view = view;
    }


    public void requestAdminsIndex() {

        String url = Endpoints.BASE_URL_ADMIN_ADMINS_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminsResponse adminsResponse = DataParser.fromJson(response, AdminsResponse.class);
            view.onResponseAdminAdmins(adminsResponse);
        }
                , error -> {
            view.onErrorResponseAdminAdmins(error);
        });

        volleyRequestController.start();
    }

    public void requestAdminsDelete(int adminId) {

        String url = Endpoints.BASE_URL_ADMIN_ADMINS_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminAdminsDeleteResponse adminAdminsDeleteResponse = DataParser.fromJson(response, AdminAdminsDeleteResponse.class);
            view.onResponseAdminAdminsDelete(adminAdminsDeleteResponse);
        }
                , error -> {
            view.onErrorResponseAdminAdminsDelete(error);
        });

        AdminAdminsDeleteRequest adminAdminsDeleteRequest = new AdminAdminsDeleteRequest();
        adminAdminsDeleteRequest.setAdminId(adminId);
        volleyRequestController.setParameters(adminAdminsDeleteRequest);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseAdminAdmins(AdminsResponse adminsResponse);

        void onErrorResponseAdminAdmins(VolleyError volleyError);

        void onResponseAdminAdminsDelete(AdminAdminsDeleteResponse adminAdminsDeleteResponse);

        void onErrorResponseAdminAdminsDelete(VolleyError volleyError);

    }
}