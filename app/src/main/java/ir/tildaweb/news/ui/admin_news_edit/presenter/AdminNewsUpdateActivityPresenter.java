package ir.tildaweb.news.ui.admin_news_edit.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsEditResponse;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsMediaDeleteRequest;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsMediaDeleteResponse;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsUpdateRequest;
import ir.tildaweb.news.ui.admin_news_edit.model.AdminNewsUpdateResponse;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsShowRequest;
import ir.tildaweb.news.ui.admin_news_index.model.AdminNewsShowResponse;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;


public class AdminNewsUpdateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminNewsUpdateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesIndex() {
        String url = Endpoints.BASE_URL_CATEGORIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CategoriesResponse categoriesResponse = DataParser.fromJson(response, CategoriesResponse.class);
            view.onResponseAdminCategoriesIndex(categoriesResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesIndex(error);
        });
        volleyRequestController.start();
    }

    public void requestNewsEdit(int newsId) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminNewsEditResponse resp = DataParser.fromJson(response, AdminNewsEditResponse.class);
            view.onResponseAdminNewsEdit(resp);
        }
                , error -> {
            view.onErrorResponseAdminNewsEdit(error);
        });

        AdminNewsShowRequest request = new AdminNewsShowRequest();
        request.setNewsId(newsId);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }


    public void requestNewsUpdate(AdminNewsUpdateRequest adminNewsUpdateRequest) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_UPDATE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsUpdateResponse adminNewsUpdateResponse = DataParser.fromJson(response, AdminNewsUpdateResponse.class);
            view.onResponseAdminNewsUpdate(adminNewsUpdateResponse);
        }
                , error -> {
            view.onErrorResponseAdminNewsUpdate(error);
        });

        volleyRequestController.setBody(adminNewsUpdateRequest);
        volleyRequestController.start();
    }

    public void requestNewsMediaDelete(Integer mediaId) {
        String url = Endpoints.BASE_URL_ADMIN_NEWS_MEDIA_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminNewsMediaDeleteResponse adminNewsMediaDeleteResponse = DataParser.fromJson(response, AdminNewsMediaDeleteResponse.class);
            view.onResponseAdminNewsMediaDelete(adminNewsMediaDeleteResponse);
        }
                , error -> {
            view.onErrorResponseAdminNewsMediaDelete(error);
        });

        AdminNewsMediaDeleteRequest adminNewsMediaDeleteRequest = new AdminNewsMediaDeleteRequest();
        adminNewsMediaDeleteRequest.setMediaId(mediaId);
        volleyRequestController.setBody(adminNewsMediaDeleteRequest);
        volleyRequestController.start();
    }


    public interface View {
        void onResponseAdminCategoriesIndex(CategoriesResponse categoriesResponse);

        void onErrorResponseAdminCategoriesIndex(VolleyError volleyError);

        void onResponseAdminNewsEdit(AdminNewsEditResponse adminNewsEditResponse);

        void onErrorResponseAdminNewsEdit(VolleyError volleyError);

        void onResponseAdminNewsUpdate(AdminNewsUpdateResponse adminNewsUpdateResponse);

        void onErrorResponseAdminNewsUpdate(VolleyError volleyError);

        void onResponseAdminNewsMediaDelete(AdminNewsMediaDeleteResponse adminNewsMediaDeleteResponse);

        void onErrorResponseAdminNewsMediaDelete(VolleyError volleyError);
    }

}