package ir.tildaweb.news.ui.contact_us.view;

import android.os.Bundle;
import android.view.View;

import com.android.volley.VolleyError;

import ir.tildaweb.news.databinding.ActivityContactUsBinding;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.contact_us.model.ContactUsStoreResponse;
import ir.tildaweb.news.ui.contact_us.presenter.ContactUsActivityPresenter;

public class ContactUsActivity extends BaseActivity implements View.OnClickListener, ContactUsActivityPresenter.View {

    private ActivityContactUsBinding binding;
    private ContactUsActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityContactUsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tvToolbarTitle.setText(String.format("%s", "ارتباط با ما"));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        presenter = new ContactUsActivityPresenter(this);
        binding.btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.btnConfirm.getId()) {
            String title = binding.etTitle.getText().toString().trim();
            String description = binding.etDescription.getText().toString().trim();
            if (title.length() == 0) {
                toast("لطفا عنوان را وارد کنید.");
            } else if (description.length() == 0) {
                toast("لطفا توضیحات را وارد کنید.");
            } else {
                showLoadingFullPage();
                presenter.requestContactUsStore(title, description, getAppPreferencesHelper().getUserId());
            }
        } else if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        }
    }

    @Override
    public void onResponseContactUsStore(ContactUsStoreResponse response) {
        dismissLoading();
        DialogMessage dialogMessage = new DialogMessage(ContactUsActivity.this, "ارتباط با ما", "درخواست شما با موفقیت ثبت شد و توسط کارشناسان ما بررسی خواهد شد.");
        dialogMessage.setOnConfirmClickListener(view -> {
            dialogMessage.dismiss();
            finish();
        });
        dialogMessage.show();
    }

    @Override
    public void onErrorResponseContactUsStore(VolleyError volleyError) {
        dismissLoading();
        toast("امکان ثبت درخواست وجود ندارد. مجدد امتحان کنید.");

    }
}