package ir.tildaweb.news.ui.news_comments.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_home.model.NewsCommentStoreResponse;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentReplyRequest;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsRequest;
import ir.tildaweb.news.ui.news_comments.model.NewsCommentsResponse;

public class NewsCommentsIndexActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public NewsCommentsIndexActivityPresenter(View view) {
        this.view = view;
    }

    public void requestNewsComments(int page, int newsId) {

        String url = Endpoints.BASE_URL_NEWS_COMMENTS_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            NewsCommentsResponse resp = DataParser.fromJson(response, NewsCommentsResponse.class);
            view.onResponseNewsComments(resp);
        }
                , error -> {
            view.onErrorResponseNewsComments(error);
        });

        NewsCommentsRequest request = new NewsCommentsRequest();
        request.setNewsId(newsId);
        request.setPage(page);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestNewsCommentReply(int newsId, int replyId, int userId, String comment) {

        String url = Endpoints.BASE_URL_NEWS_COMMENTS_REPLY;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            NewsCommentStoreResponse newsCommentStoreResponse = DataParser.fromJson(response, NewsCommentStoreResponse.class);
            view.onResponseNewsCommentsReply(newsCommentStoreResponse);
        }
                , error -> {
            view.onErrorResponseNewsCommentsReply(error);
        });

        NewsCommentReplyRequest request = new NewsCommentReplyRequest();
        request.setNewsId(newsId);
        request.setReplyId(replyId);
        request.setUserId(userId);
        request.setComment(comment);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseNewsComments(NewsCommentsResponse newsCommentsResponse);

        void onErrorResponseNewsComments(VolleyError volleyError);

        void onResponseNewsCommentsReply(NewsCommentStoreResponse newsCommentStoreResponse);

        void onErrorResponseNewsCommentsReply(VolleyError volleyError);

    }
}