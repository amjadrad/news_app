package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationResponse;

public class ProfileUpdateResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("user")
    private SigninVerificationResponse.User user;

    public SigninVerificationResponse.User getUser() {
        return user;
    }

    public void setUser(SigninVerificationResponse.User user) {
        this.user = user;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}