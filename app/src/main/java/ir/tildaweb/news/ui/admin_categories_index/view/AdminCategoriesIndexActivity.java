package ir.tildaweb.news.ui.admin_categories_index.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminCategories;
import ir.tildaweb.news.databinding.ActivityAdminCategoriesIndexBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.listeners.OnAdminNewsItemClickListener;
import ir.tildaweb.news.ui.admin_categories_create.view.AdminCategoriesCreateActivity;
import ir.tildaweb.news.ui.admin_categories_edit.view.AdminCategoriesUpdateActivity;
import ir.tildaweb.news.ui.admin_categories_index.presenter.AdminCategoriesActivityPresenter;
import ir.tildaweb.news.ui.admin_categories_index.model.AdminCategoriesDeleteResponse;
import ir.tildaweb.news.ui.admin_news_create.model.AdminCategoriesShowResponse;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;

public class AdminCategoriesIndexActivity extends BaseActivity implements View.OnClickListener, ItemClickListener, AdminCategoriesActivityPresenter.View {

    private ActivityAdminCategoriesIndexBinding binding;
    private AdminCategoriesActivityPresenter presenter;
    private AdapterAdminCategories adapterAdminCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminCategoriesIndexBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new AdminCategoriesActivityPresenter(this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterAdminCategories = new AdapterAdminCategories(this, new ArrayList<>(), this);
        binding.recyclerView.setAdapter(adapterAdminCategories);

        binding.linearNewsCreate.setOnClickListener(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("مدیریت دسته بندی ها");

    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoadingFullPage();
        presenter.requestCategoriesIndex();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.linearNewsCreate: {
                if (getAppPreferencesHelper().getAdminAccessCategoriesInsert() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
                    startActivity(new Intent(AdminCategoriesIndexActivity.this, AdminCategoriesCreateActivity.class));
                } else {
                    toast("شما دسترسی ایجاد دسته بندی ندارید.");
                }
                break;
            }
        }

    }


    @Override
    public void onEdit(int id) {
        if (getAppPreferencesHelper().getAdminAccessCategoriesUpdate() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            Intent intent = new Intent(AdminCategoriesIndexActivity.this, AdminCategoriesUpdateActivity.class);
            intent.putExtra("category_id", id);
            startActivity(intent);
        } else {
            toast("شما دسترسی ویرایش دسته بندی ندارید.");
        }
    }


    @Override
    public void onDelete(int id, String title) {
        if (getAppPreferencesHelper().getAdminAccessCategoriesDelete() || getAppPreferencesHelper().getUserAdminTypePref().equals("owner")) {
            String t = "حذف دسته بندی";
            String description = String.format("آیا میخواهید دسته بندی (%s) را حذف کنید؟ دقت کنید که تمام اخبار این دسته حذف خواهند شد.", title);
            DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(this, t, description);
            dialogConfirmMessage.setDanger();
            dialogConfirmMessage.setConfirmButtonText("حذف");
            dialogConfirmMessage.show();
            dialogConfirmMessage.setOnConfirmClickListener(view -> {
                dialogConfirmMessage.dismiss();
                showLoadingFullPage();
                presenter.requestCategoriesDelete(id);
            });
        } else {
            toast("شما دسترسی حذف دسته بندی ندارید.");
        }
    }

    @Override
    public void onResponseAdminCategoriesIndex(CategoriesResponse categoriesResponse) {
        dismissLoading();
        if (categoriesResponse.getCategories().size() == 0) {
            binding.linearNoItem.setVisibility(View.VISIBLE);
        } else {
            binding.linearNoItem.setVisibility(View.GONE);
            adapterAdminCategories.clearAll();
            adapterAdminCategories.addItems(categoriesResponse.getCategories());
        }
    }

    @Override
    public void onErrorResponseAdminCategoriesIndex(VolleyError volleyError) {
        toast("امکان دریافت لیست دسته بندی ها وجود ندارد.");
    }


    @Override
    public void onResponseAdminCategoriesDelete(AdminCategoriesDeleteResponse adminCategoriesDeleteResponse) {
        dismissLoading();
        adapterAdminCategories.deleteItem(adminCategoriesDeleteResponse.getCategoryId());
    }

    @Override
    public void onErrorResponseAdminCategoriesDelete(VolleyError volleyError) {
        dismissLoading();
        toast("حذف دسته بندی با مشکل مواجه شد. مجددا امتحان کنید.");
    }
}