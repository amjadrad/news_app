package ir.tildaweb.news.ui.app_comments_list.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsAverageRatingResponse;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsDeleteRequest;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsDeleteResponse;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsListRequest;
import ir.tildaweb.news.ui.app_comments_list.model.AppCommentsListResponse;


public class AppCommentsListActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AppCommentsListActivityPresenter(View view) {
        this.view = view;
    }


    public void requestAppCommentsList(int page) {
        String url = Endpoints.BASE_URL_APP_COMMENT_INDEX;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AppCommentsListResponse appCommentsListResponse = DataParser.fromJson(response, AppCommentsListResponse.class);
            view.onResponseAppCommentsList(appCommentsListResponse);
        }
                , error -> {
            view.onErrorResponseAppCommentsUsList(error);
        });

        AppCommentsListRequest request = new AppCommentsListRequest();
        request.setPage(page);
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }

    public void requestAppCommentsAverageRating() {
        String url = Endpoints.BASE_URL_APP_COMMENT_AVERAGE_RATING;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AppCommentsAverageRatingResponse appCommentsAverageRatingResponse = DataParser.fromJson(response, AppCommentsAverageRatingResponse.class);
            view.onResponseAppCommentsAverageRating(appCommentsAverageRatingResponse);
        }
                , error -> {
            view.onErrorResponseAppCommentsAverageRating(error);
        });

        volleyRequestController.start();
    }

    public void requestAppCommentDelete(int id) {
        String url = Endpoints.BASE_URL_APP_COMMENT_DELETE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AppCommentsDeleteResponse appCommentsDeleteResponse = DataParser.fromJson(response, AppCommentsDeleteResponse.class);
            view.onResponseAppCommentsDelete(appCommentsDeleteResponse);
        }
                , error -> {
            view.onErrorResponseAppCommentsDelete(error);
        });

        AppCommentsDeleteRequest request = new AppCommentsDeleteRequest();
        request.setAppCommentId(id);
        volleyRequestController.setBody(request);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseAppCommentsList(AppCommentsListResponse response);

        void onErrorResponseAppCommentsUsList(VolleyError volleyError);

        void onResponseAppCommentsAverageRating(AppCommentsAverageRatingResponse response);

        void onErrorResponseAppCommentsAverageRating(VolleyError volleyError);

        void onResponseAppCommentsDelete(AppCommentsDeleteResponse response);

        void onErrorResponseAppCommentsDelete(VolleyError volleyError);
    }


}