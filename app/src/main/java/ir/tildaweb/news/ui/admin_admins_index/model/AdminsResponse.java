package ir.tildaweb.news.ui.admin_admins_index.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AdminsResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("admins")
    private ArrayList<Admin> admins;

    public class Admin {
        @SerializedName("id")
        private Integer id;
        @SerializedName("full_name")
        private String name;
        @SerializedName("phone")
        private String phone;
        @SerializedName("type")
        private String type;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<Admin> getAdmins() {
        return admins;
    }

    public void setAdmins(ArrayList<Admin> admins) {
        this.admins = admins;
    }
}
