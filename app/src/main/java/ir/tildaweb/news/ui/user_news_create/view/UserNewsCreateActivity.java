package ir.tildaweb.news.ui.user_news_create.view;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminNewsImages;
import ir.tildaweb.news.data.network.FileUploader;
import ir.tildaweb.news.databinding.ActivityUserNewsCreateBinding;
import ir.tildaweb.news.dialogs.DialogBottomSheetSelect;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.ui.admin_news_create.model.AdminNewsStoreRequest;
import ir.tildaweb.news.ui.base.BaseActivity;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.user_news_create.model.UserNewsStoreRequest;
import ir.tildaweb.news.ui.user_news_create.model.UserNewsStoreResponse;
import ir.tildaweb.news.ui.user_news_create.presenter.UserNewsCreateActivityPresenter;
import ir.tildaweb.tilda_filepicker.TildaFilePicker;
import ir.tildaweb.tilda_filepicker.enums.FileMimeType;
import ir.tildaweb.tilda_filepicker.enums.FileType;
import ir.tildaweb.tilda_filepicker.models.FileModel;

public class UserNewsCreateActivity extends BaseActivity implements View.OnClickListener, ItemClickListener, UserNewsCreateActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityUserNewsCreateBinding binding;
    private UserNewsCreateActivityPresenter presenter;
    private CategoriesResponse categoriesResponse;
    private Integer categoryId;
    private Integer PICK_FILE_PERMISSION_CODE = 1001;
    private Integer uploadFileRequestId = 0;
    private AdapterAdminNewsImages adapterAdminNewsImages;
    private ArrayList<AdminNewsStoreRequest.MediaModel> mediaModels;
    private Integer uploadedFilesCount = 0;
    private FileUploader fileUploader;
    private ArrayList<DialogBottomSheetSelect.SelectObject> listSelectCategories;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserNewsCreateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new UserNewsCreateActivityPresenter(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.coordinatorSelectFile.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.coordinatorSelectCategory.setOnClickListener(this);
        listSelectCategories = new ArrayList<>();

        binding.toolbar.tvToolbarTitle.setText("ایجاد خبر جدید");
        fileUploader = new FileUploader();
        adapterAdminNewsImages = new AdapterAdminNewsImages(this, new ArrayList<>(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerViewImages.setLayoutManager(linearLayoutManager);
        binding.recyclerViewImages.setAdapter(adapterAdminNewsImages);
        presenter.requestCategoriesIndex();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.coordinatorSelectFile: {
                if (checkReadExternalPermission(UserNewsCreateActivity.this, PICK_FILE_PERMISSION_CODE)) {
                    TildaFilePicker tildaFilePicker = new TildaFilePicker(UserNewsCreateActivity.this, new FileType[]{FileType.FILE_TYPE_IMAGE});
                    tildaFilePicker.setOnTildaFileSelectListener(list -> {
                        for (FileModel model : list) {
                            if (model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_IMAGE || model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_VIDEO) {
                                adapterAdminNewsImages.addItem(model);
                                binding.recyclerViewImages.scrollToPosition(0);
                            } else {
                                toast("فقط مجاز به انتخاب عکس و ویدیو هستید.");
                            }
                        }
                    });
                    tildaFilePicker.show(getSupportFragmentManager());
                }
                break;
            }
            case R.id.btnConfirm: {

                hideKeyboard(UserNewsCreateActivity.this, binding.etTitle);
                hideKeyboard(UserNewsCreateActivity.this, binding.etDescription);
                if (checkValidation()) {
                    showLoadingFullPage();
                    if (adapterAdminNewsImages.getList().size() > 0) {
                        mediaModels = new ArrayList<>();
                        toast("بر اساس حجم عکس و فیلم ها ممکن است مدتی طول بکشد...");

                        for (FileModel model : adapterAdminNewsImages.getList()) {

                            uploadFileRequestId++;
                            AdminNewsStoreRequest.MediaModel mediaModel = new AdminNewsStoreRequest().new MediaModel();
                            mediaModel.setId(uploadFileRequestId);
                            mediaModel.setType(model.getFileMimeType() == FileMimeType.FILE_MIME_TYPE_IMAGE ? "picture" : "video");
                            mediaModels.add(mediaModel);

                            fileUploader.upload(model.getPath(), uploadFileRequestId, new FileUploader.OnFileUploaderListener() {
                                @Override
                                public void onFileUploadProgress(int requestId, int percent) {
                                }

                                @Override
                                public void onFileUploaded(String fileName, int requestId) {
                                    uploadedFilesCount++;
                                    if (fileName != null) {
                                        for (AdminNewsStoreRequest.MediaModel mediaModel : mediaModels) {
                                            if (mediaModel.getId() == requestId) {
                                                mediaModel.setPath(fileName);
                                                break;
                                            }
                                        }
                                    }
                                    if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                        sendFinalStoreRequest();
                                    }
                                }

                                @Override
                                public void onFileUploadError(String error) {
                                    toast("امکان ارسال فایل وجود ندارد.");
                                    uploadedFilesCount++;
                                    if (uploadedFilesCount == adapterAdminNewsImages.getItemCount()) {
                                        sendFinalStoreRequest();
                                    }
                                }
                            });
                        }
                    } else {
                        sendFinalStoreRequest();

                    }
                }
                break;
            }
            case R.id.coordinatorSelectCategory: {
                String title = "دسته بندی مورد نظر را انتخاب کنید.";
                String searchHint = "جستجو دسته بندی:";
                DialogBottomSheetSelect dialog = new DialogBottomSheetSelect(title, searchHint, listSelectCategories);
                dialog.setClickListener(id -> {
                    for (CategoriesResponse.Category item : categoriesResponse.getCategories())
                        if (item.getId() == id) {
                            categoryId = id;
                            binding.tvSelectedCategoryName.setText(String.valueOf(item.getTitle()));
                            dialog.dismiss();
                            break;
                        }
                });
                dialog.show(getSupportFragmentManager(), null);
                break;
            }
        }
    }

    private void sendFinalStoreRequest() {
        UserNewsStoreRequest userNewsStoreRequest = new UserNewsStoreRequest();
        userNewsStoreRequest.setTitle(binding.etTitle.getText().toString().trim());
        userNewsStoreRequest.setCategoryId(categoryId);
        userNewsStoreRequest.setUserId(getAppPreferencesHelper().getUserId());
        userNewsStoreRequest.setDescription(binding.etDescription.getText().toString().trim());
        userNewsStoreRequest.setMedia(mediaModels);
        presenter.requestNewsStore(userNewsStoreRequest);
    }

    @Override
    public void onResponseUserCategoriesIndex(CategoriesResponse categoriesResponse) {
        dismissLoading();
        this.categoriesResponse = categoriesResponse;
        listSelectCategories.clear();
        Drawable icon = ContextCompat.getDrawable(this, R.drawable.ic_list);
        for (CategoriesResponse.Category item : categoriesResponse.getCategories()) {
            DialogBottomSheetSelect.SelectObject selectObject = new DialogBottomSheetSelect.SelectObject();
            selectObject.setTitle(item.getTitle());
            selectObject.setId(item.getId());
            selectObject.setIcon(icon);
            selectObject.setSelected(false);
            if (categoryId != null) {
                if (categoryId.intValue() == item.getId()) {
                    selectObject.setSelected(true);
                    categoryId = item.getId();
                    binding.tvSelectedCategoryName.setText(String.valueOf(item.getTitle()));
                }
            }
            listSelectCategories.add(selectObject);
        }
    }

    @Override
    public void onErrorResponseUserCategoriesIndex(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت دسته بندی ها وجود ندارد.");
    }

    @Override
    public void onResponseUserNewsStore(UserNewsStoreResponse userNewsStoreResponse) {
        dismissLoading();
        toast("خبر با موفقیت ثبت شد و توسط کارشناسان ما بررسی خواهد شد.");
        Intent intent = new Intent();
        intent.putExtra("user_news_id", userNewsStoreResponse.getUserNewsId());
        setResult(1, intent);
        finish();
    }

    @Override
    public void onErrorResponseUserNewsStore(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در ذخیره خبر به وجود آمد.");
    }


    private boolean checkValidation() {
        if (binding.etTitle.getText().toString().trim().length() == 0) {
            toast("لطفا عنوان خبر را وارد کنید.");
            return false;
        } else if (binding.etDescription.getText().toString().trim().length() == 0) {
            toast("لطفا توضیحات خبر را وارد کنید.");
            return false;
        } else if (categoryId == null) {
            toast("لطفا دسته بندی را انتخاب کنید.");
            return false;
        }
        return true;
    }


    @Override
    public void onEdit(int id) {

    }

    @Override
    public void onDelete(int id, String title) {

    }
}