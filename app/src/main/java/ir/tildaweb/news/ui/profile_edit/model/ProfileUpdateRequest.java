package ir.tildaweb.news.ui.profile_edit.model;

import com.google.gson.annotations.SerializedName;

public class ProfileUpdateRequest {

    @SerializedName("full_name")
    private String name;
    @SerializedName("user_id")
    private Integer userId;
    @SerializedName("city_id")
    private Integer cityId;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
