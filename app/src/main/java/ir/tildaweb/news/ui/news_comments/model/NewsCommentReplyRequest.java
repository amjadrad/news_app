package ir.tildaweb.news.ui.news_comments.model;

import com.google.gson.annotations.SerializedName;


public class NewsCommentReplyRequest {

    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("reply_id")
    private Integer replyId;
    @SerializedName("comment")
    private String comment;
    @SerializedName("user_id")
    private Integer userId;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
