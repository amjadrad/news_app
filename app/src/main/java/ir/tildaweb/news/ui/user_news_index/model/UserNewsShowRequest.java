package ir.tildaweb.news.ui.user_news_index.model;

import com.google.gson.annotations.SerializedName;

public class UserNewsShowRequest {

    @SerializedName("user_news_id")
    private Integer userNewsId;

    public Integer getUserNewsId() {
        return userNewsId;
    }

    public void setUserNewsId(Integer userNewsId) {
        this.userNewsId = userNewsId;
    }
}