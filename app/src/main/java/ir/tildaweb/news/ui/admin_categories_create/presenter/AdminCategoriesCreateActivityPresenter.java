package ir.tildaweb.news.ui.admin_categories_create.presenter;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateRequest;
import ir.tildaweb.news.ui.admin_categories_edit.model.AdminCategoriesUpdateResponse;


public class AdminCategoriesCreateActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public AdminCategoriesCreateActivityPresenter(View view) {
        this.view = view;
    }

    public void requestCategoriesStore(AdminCategoriesUpdateRequest adminCategoriesUpdateRequest) {
        String url = Endpoints.BASE_URL_ADMIN_CATEGORIES_STORE;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            AdminCategoriesUpdateResponse adminCategoriesUpdateResponse = DataParser.fromJson(response, AdminCategoriesUpdateResponse.class);
            view.onResponseAdminCategoriesStore(adminCategoriesUpdateResponse);
        }
                , error -> {
            view.onErrorResponseAdminCategoriesStore(error);
        });
        volleyRequestController.setBody(adminCategoriesUpdateRequest);
        volleyRequestController.start();
    }

    public interface View {
        void onResponseAdminCategoriesStore(AdminCategoriesUpdateResponse adminCategoriesUpdateResponse);

        void onErrorResponseAdminCategoriesStore(VolleyError volleyError);
    }

}