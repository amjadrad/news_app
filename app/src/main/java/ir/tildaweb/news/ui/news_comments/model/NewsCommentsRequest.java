package ir.tildaweb.news.ui.news_comments.model;

import com.google.gson.annotations.SerializedName;

public class NewsCommentsRequest {

    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("page")
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
