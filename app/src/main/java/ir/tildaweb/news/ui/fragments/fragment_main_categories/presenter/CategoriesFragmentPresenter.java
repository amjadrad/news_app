package ir.tildaweb.news.ui.fragments.fragment_main_categories.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesRequest;
import ir.tildaweb.news.ui.fragments.fragment_main_categories.model.CategoriesResponse;
import ir.tildaweb.news.ui.sigin.model.SigninRequest;
import ir.tildaweb.news.ui.sigin.model.SigninResponse;


public class CategoriesFragmentPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public CategoriesFragmentPresenter(View view) {
        this.view = view;
    }


    public void requestCategories(int userId) {
        String url = Endpoints.BASE_URL_CATEGORIES;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            CategoriesResponse categoriesResponse = DataParser.fromJson(response, CategoriesResponse.class);
            view.onResponseCategories(categoriesResponse);
        }
                , error -> {
            view.onErrorResponseCategories(error);
        });
        CategoriesRequest categoriesRequest = new CategoriesRequest();
        categoriesRequest.setUserId(userId);
        volleyRequestController.setParameters(categoriesRequest);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseCategories(CategoriesResponse categoriesResponse);

        void onErrorResponseCategories(VolleyError volleyError);

    }


}