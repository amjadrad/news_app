package ir.tildaweb.news.ui.sigin.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.sigin.model.SigninRequest;
import ir.tildaweb.news.ui.sigin.model.SigninResponse;


public class SignActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public SignActivityPresenter(View view) {
        this.view = view;
    }


    public void requestSignin(String phone) {
        String url = Endpoints.BASE_URL_SIGN_IN;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            SigninResponse signinResponse = DataParser.fromJson(response, SigninResponse.class);
            view.onResponseSignIn(signinResponse);
        }
                , error -> {
            view.onErrorResponseSignIn(error);
        });

        SigninRequest signinRequest = new SigninRequest();
        signinRequest.setPhone(phone);
        volleyRequestController.setBody(signinRequest);
        volleyRequestController.start();
    }


    public interface View {

        void onResponseSignIn(SigninResponse signinResponse);

        void onErrorResponseSignIn(VolleyError volleyError);

    }


}