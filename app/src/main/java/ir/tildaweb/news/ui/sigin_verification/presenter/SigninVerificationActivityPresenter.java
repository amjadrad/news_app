package ir.tildaweb.news.ui.sigin_verification.presenter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.data.network.Endpoints;
import ir.tildaweb.news.ui.admin_admins_access.model.AdminAccessEditResponse;
import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationRequest;
import ir.tildaweb.news.ui.sigin_verification.model.SigninVerificationResponse;
import ir.tildaweb.news.ui.splash.model.AdminAccessRequest;


public class SigninVerificationActivityPresenter {

    private String TAG = this.getClass().getName();

    private View view;

    public SigninVerificationActivityPresenter(View view) {
        this.view = view;
    }


    public void requestSigninVerification(String phone, String verificationCode) {
        String url = Endpoints.BASE_URL_VERIFY;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.POST, url, response -> {
            SigninVerificationResponse signinVerificationResponse = DataParser.fromJson(response, SigninVerificationResponse.class);
            view.onResponseSignInVerification(signinVerificationResponse);
        }
                , error -> {
            view.onErrorResponseSignInVerification(error);
        });

        SigninVerificationRequest signinVerificationRequest = new SigninVerificationRequest();
        signinVerificationRequest.setPhone(phone);
        signinVerificationRequest.setVerificationCode(verificationCode);
        volleyRequestController.setBody(signinVerificationRequest);
        volleyRequestController.start();
    }

    public void requestAdminAccess(AdminAccessRequest request) {
        String url = Endpoints.BASE_URL_ADMIN_ADMIN_ACCESS_EDIT;
        VolleyRequestController volleyRequestController = new VolleyRequestController(Request.Method.GET, url, response -> {
            AdminAccessEditResponse response1 = DataParser.fromJson(response, AdminAccessEditResponse.class);
            view.onResponseUserInfo(response1);
        }
                , error -> {
            view.onErrorResponseUserInfo(error);
        });
        volleyRequestController.setParameters(request);
        volleyRequestController.start();
    }



    public interface View {

        void onResponseSignInVerification(SigninVerificationResponse signinVerificationResponse);

        void onErrorResponseSignInVerification(VolleyError volleyError);

        void onResponseUserInfo(AdminAccessEditResponse response);

        void onErrorResponseUserInfo(VolleyError volleyError);


    }


}