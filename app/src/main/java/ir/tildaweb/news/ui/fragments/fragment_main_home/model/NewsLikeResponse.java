package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;


public class NewsLikeResponse {

    @SerializedName("status")
    private Integer status;
    @SerializedName("news_id")
    private Integer newsId;
    @SerializedName("likes_count")
    private Integer likesCount;
    @SerializedName("dislikes_count")
    private Integer dislikesCount;
    @SerializedName("user_like")
    private Integer userLike;

    public Integer getUserLike() {
        return userLike;
    }

    public void setUserLike(Integer userLike) {
        this.userLike = userLike;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Integer getDislikesCount() {
        return dislikesCount;
    }

    public void setDislikesCount(Integer dislikesCount) {
        this.dislikesCount = dislikesCount;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
