package ir.tildaweb.news.ui.admin_admins_index.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.R;
import ir.tildaweb.news.adapter.AdapterAdminAdmins;
import ir.tildaweb.news.adapter.AdapterAdminNews;
import ir.tildaweb.news.app.VolleyRequestController;
import ir.tildaweb.news.databinding.ActivityAdminAdminsIndexBinding;
import ir.tildaweb.news.databinding.ActivityAdminNewsIndexBinding;
import ir.tildaweb.news.dialogs.DialogConfirmMessage;
import ir.tildaweb.news.listeners.AdminItemClickListener;
import ir.tildaweb.news.listeners.ItemClickListener;
import ir.tildaweb.news.listeners.OnAdminNewsItemClickListener;
import ir.tildaweb.news.listeners.OnLoadMoreListener;
import ir.tildaweb.news.ui.admin_admins_access.view.AdminAdminsAccessActivity;
import ir.tildaweb.news.ui.admin_admins_create.view.AdminAdminsCreateActivity;
import ir.tildaweb.news.ui.admin_admins_edit.view.AdminAdminsEditActivity;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminAdminsDeleteResponse;
import ir.tildaweb.news.ui.admin_admins_index.model.AdminsResponse;
import ir.tildaweb.news.ui.admin_admins_index.presenter.AdminAdminsIndexActivityPresenter;
import ir.tildaweb.news.ui.admin_news_create.view.AdminNewsCreateActivity;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminAdminsIndexActivity extends BaseActivity implements View.OnClickListener, AdminAdminsIndexActivityPresenter.View, AdminItemClickListener {

    private ActivityAdminAdminsIndexBinding binding;
    private AdminAdminsIndexActivityPresenter adminAdminsIndexActivityPresenter;
    private AdapterAdminAdmins adapterAdminAdmins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminAdminsIndexBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        adminAdminsIndexActivityPresenter = new AdminAdminsIndexActivityPresenter(this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapterAdminAdmins = new AdapterAdminAdmins(this, new ArrayList<>(), this);
        binding.recyclerView.setAdapter(adapterAdminAdmins);

        binding.linearCreate.setOnClickListener(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("مدیریت مدیرها");
    }


    @Override
    protected void onResume() {
        super.onResume();
        showLoadingFullPage();
        adminAdminsIndexActivityPresenter.requestAdminsIndex();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.linearCreate: {
                startActivity(new Intent(AdminAdminsIndexActivity.this, AdminAdminsCreateActivity.class));
                break;
            }
        }

    }


    @Override
    public void onEdit(int id) {
        Intent intent = new Intent(AdminAdminsIndexActivity.this, AdminAdminsEditActivity.class);
        intent.putExtra("admin_id", id);
        startActivity(intent);
    }

    @Override
    public void onAccess(int id, String name) {
        Intent intent = new Intent(AdminAdminsIndexActivity.this, AdminAdminsAccessActivity.class);
        intent.putExtra("admin_id", id);
        intent.putExtra("admin_fullname", name);
        startActivity(intent);
    }

    @Override
    public void onDelete(int id, String title) {
        String t = "حذف خبر";
        String description = String.format("آیا میخواهید مدیر (%s) را حذف کنید؟", title);
        DialogConfirmMessage dialogConfirmMessage = new DialogConfirmMessage(this, t, description);
        dialogConfirmMessage.setDanger();
        dialogConfirmMessage.setConfirmButtonText("حذف");
        dialogConfirmMessage.show();
        dialogConfirmMessage.setOnConfirmClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogConfirmMessage.dismiss();
                showLoadingFullPage();
                adminAdminsIndexActivityPresenter.requestAdminsDelete(id);
            }
        });
    }

    @Override
    public void onResponseAdminAdmins(AdminsResponse adminsResponse) {
        dismissLoading();
        adapterAdminAdmins.clearAll();
        adapterAdminAdmins.addItems(adminsResponse.getAdmins());
    }

    @Override
    public void onErrorResponseAdminAdmins(VolleyError volleyError) {
        dismissLoading();
        toast("امکان دریافت لیست مدیرها وجود ندارد. مجدد امتحان کنید.");
    }

    @Override
    public void onResponseAdminAdminsDelete(AdminAdminsDeleteResponse adminAdminsDeleteResponse) {
        dismissLoading();
        adapterAdminAdmins.deleteItem(adminAdminsDeleteResponse.getAdminId());
    }

    @Override
    public void onErrorResponseAdminAdminsDelete(VolleyError volleyError) {
        dismissLoading();
        if (volleyError.networkResponse.statusCode == 401) {
            toast("امکان حذف مدیراصلی وجود ندارد.");
        } else if (volleyError.networkResponse.statusCode == 403) {
            toast("امکان حذف مدیر به علت ثبت خبر وجود ندارد. می توانید غیرفعال کنید.");
        }
    }
}