package ir.tildaweb.news.ui.admin_settings.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.VolleyError;

import java.util.ArrayList;

import ir.tildaweb.news.adapter.AdapterDropDown;
import ir.tildaweb.news.databinding.ActivityAdminSettingsBinding;
import ir.tildaweb.news.databinding.ActivityProfileUpdateBinding;
import ir.tildaweb.news.dialogs.DialogMessage;
import ir.tildaweb.news.ui.admin_news_edit.presenter.CityInfoPresenter;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsEditResponse;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsUpdateRequest;
import ir.tildaweb.news.ui.admin_settings.model.AdminSettingsUpdateResponse;
import ir.tildaweb.news.ui.admin_settings.presenter.AdminSettingsActivityPresenter;
import ir.tildaweb.news.ui.base.BaseActivity;

public class AdminSettingsActivity extends BaseActivity implements View.OnClickListener, AdminSettingsActivityPresenter.View {

    private String TAG = this.getClass().getName();
    private ActivityAdminSettingsBinding binding;
    private AdminSettingsActivityPresenter presenter;
    private boolean isShowUserCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAdminSettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tvToolbarTitle.setText(String.format("%s", "تنظیمات"));
        binding.toolbar.imageViewBack.setOnClickListener(this);
        presenter = new AdminSettingsActivityPresenter(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.toggleShowUsersCount.setOnToggleChanged(on -> isShowUserCount = on);

        showLoadingFullPage();
        presenter.requestAdminSettingsEdit(getAppPreferencesHelper().getAdminId());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.btnConfirm.getId()) {
            showLoadingFullPage();
            AdminSettingsUpdateRequest request = new AdminSettingsUpdateRequest();
            request.setShowUserCount(isShowUserCount);
            presenter.requestAdminSettingsUpdate(request);
        } else if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        }
    }

    @Override
    public void onResponseAdminSettingsEdit(AdminSettingsEditResponse response) {
        dismissLoading();
        if (response.getAdminSetting().getShowUserCount()) {
            binding.toggleShowUsersCount.setToggleOn();
        } else {
            binding.toggleShowUsersCount.setToggleOff();
        }
    }

    @Override
    public void onErrorResponseAdminSettingsEdit(VolleyError volleyError) {
        dismissLoading();
        toast("مشکلی در دریافت اطلاعات به وجود آمد.");
    }

    @Override
    public void onResponseAdminSettingsUpdate(AdminSettingsUpdateResponse response) {
        dismissLoading();
        getAppPreferencesHelper().setShowUsersCount(response.getAdminSetting().getShowUserCount());
        toast("با موفقیت ویرایش شد.");
        finish();
    }

    @Override
    public void onErrorResponseAdminSettingsUpdate(VolleyError volleyError) {
        dismissLoading();
        toast("ویرایش تنطیمات با مشکل مواجه شد.");
    }
}