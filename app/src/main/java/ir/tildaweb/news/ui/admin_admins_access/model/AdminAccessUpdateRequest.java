package ir.tildaweb.news.ui.admin_admins_access.model;

import com.google.gson.annotations.SerializedName;

public class AdminAccessUpdateRequest {

    @SerializedName("admin_id")
    private Integer adminId;
    @SerializedName("news_actions")
    private String newsActions;
    @SerializedName("categories_actions")
    private String categoriesActions;
    @SerializedName("user_news_actions")
    private String userNewsActions;
    @SerializedName("app_comments_actions")
    private String appCommentsActions;
    @SerializedName("user_contacts_actions")
    private String userContactsActions;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getNewsActions() {
        return newsActions;
    }

    public void setNewsActions(String newsActions) {
        this.newsActions = newsActions;
    }

    public String getCategoriesActions() {
        return categoriesActions;
    }

    public void setCategoriesActions(String categoriesActions) {
        this.categoriesActions = categoriesActions;
    }

    public String getUserNewsActions() {
        return userNewsActions;
    }

    public void setUserNewsActions(String userNewsActions) {
        this.userNewsActions = userNewsActions;
    }

    public String getAppCommentsActions() {
        return appCommentsActions;
    }

    public void setAppCommentsActions(String appCommentsActions) {
        this.appCommentsActions = appCommentsActions;
    }

    public String getUserContactsActions() {
        return userContactsActions;
    }

    public void setUserContactsActions(String userContactsActions) {
        this.userContactsActions = userContactsActions;
    }
}
