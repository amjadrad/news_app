package ir.tildaweb.news.ui.fragments.fragment_main_home.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewsPosterResponse {

    @SerializedName("data")
    private ArrayList<PosterNews> posterNews;

    public class PosterNews{
        @SerializedName("id")
        private Integer id;
        @SerializedName("news")
        private NewsResponse.NewsData.News news;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public NewsResponse.NewsData.News getNews() {
            return news;
        }

        public void setNews(NewsResponse.NewsData.News news) {
            this.news = news;
        }
    }

    public ArrayList<PosterNews> getPosterNews() {
        return posterNews;
    }

    public void setPosterNews(ArrayList<PosterNews> posterNews) {
        this.posterNews = posterNews;
    }
}
