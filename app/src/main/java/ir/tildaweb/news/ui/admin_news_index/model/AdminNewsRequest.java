package ir.tildaweb.news.ui.admin_news_index.model;

import com.google.gson.annotations.SerializedName;


public class AdminNewsRequest {

    @SerializedName("search")
    private String search;
    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("page")
    private Integer page;
    @SerializedName("admin_id")
    private Integer adminId;

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
