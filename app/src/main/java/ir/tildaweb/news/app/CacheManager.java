package ir.tildaweb.news.app;

import android.content.Context;

import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import ir.tildaweb.news.BuildConfig;


public class CacheManager {

    private DualCache<String> cache;
    public static String CACHE_TYPE_NAZMENOVIN = "cache_news";
    private final int MAX_RAM_SIZE = 10000000; // 10 MB
    private final int MAX_DISK_SIZE = 100000000; // 100 MB

    // Cache keys
    private static String CACHE_KEY_USER_SEARCH_HISTORY = "user_search_history";

    public CacheManager(Context context) {
        CacheSerializer<String> jsonSerializer = new JsonSerializer<>(String.class);
        cache = new Builder<String>(CACHE_TYPE_NAZMENOVIN, BuildConfig.APP_VERSION_ID)
                .enableLog()
                .useSerializerInRam(MAX_RAM_SIZE, jsonSerializer)
                .useSerializerInDisk(MAX_DISK_SIZE, true, jsonSerializer, context)
                .build();
    }

    public void setUserSearchHistory(String value) {
        cache.put(CACHE_KEY_USER_SEARCH_HISTORY, value);
    }

    public String getUserSearchHistory() {
        return cache.get(CACHE_KEY_USER_SEARCH_HISTORY);
    }

    public void invalidate() {
        cache.invalidate();
    }
}