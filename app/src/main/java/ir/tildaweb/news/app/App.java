package ir.tildaweb.news.app;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;

import java.util.HashMap;
import java.util.Map;

import ir.tildaweb.news.BuildConfig;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;

public class App extends Application {

    private static RequestQueue requestQueue;
    private static AppPreferencesHelper appPreferencesHelper;
    private static String hashCode = "";

    @Override
    public void onCreate() {
        super.onCreate();
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        AndroidNetworking.initialize(getApplicationContext());
        appPreferencesHelper = new AppPreferencesHelper(getApplicationContext());
        hashCode = new AppSignatureHelper(this).getAppSignatures().get(0);
    }

    public static String getHashCode() {
        return hashCode;
    }

    public static void addToQueue(StringRequest stringRequest) {
        requestQueue.add(stringRequest);
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Accept", "application/json");
        map.put("version_id", BuildConfig.APP_VERSION_ID + "");
        map.put("user_id", appPreferencesHelper.getUserId() + "");
        map.put("admin_id", appPreferencesHelper.getAdminId() + "");
//        try {
//            String TAG = "AppJAVA";
//            String iv = AESEncrypt.toBase64(AESEncrypt.generateRandomIv());
//            AESEncrypt AESEncrypt = new AESEncrypt(BuildConfig.API_KEY, 128, iv);
//            String api_token = AESEncrypt.encrypt(appPreferencesHelper.getUserApiToken());
//            String api_key = AESEncrypt.encrypt(appPreferencesHelper.getUserApiKey());
//            Log.d("AppHeader", "getHeaders: " + appPreferencesHelper.getUserDeviceId());
//            map.put("api-key", "" + api_key);
//            map.put("cipher-key", iv);

//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return map;
    }

}
