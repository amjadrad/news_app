package ir.tildaweb.news.app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.util.Map;


public class VolleyRequestController extends StringRequest {

    private String TAG = this.getClass().getName();

    public JSONObject customBody;
    private String parameters;

    public VolleyRequestController(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        customBody = new JSONObject();
        this.parameters = "";
    }


    @Override
    public String getUrl() {
        return super.getUrl() + parameters;
    }

    @Override
    protected void deliverResponse(String response) {
        // change response before send
        // this call after @parseNetworkResponse

//            String str = EasyAES.decryptString(response);

        int maxLogSize = 1000;
        if (response != null) {
            for (int i = 0; i <= response.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = Math.min(end, response.length());
                Log.d(TAG, response.substring(start, end));
            }
        }

        super.deliverResponse(response);
    }


    public void setBody(Object body) {
        try {
            customBody = new JSONObject(DataParser.toJson(body));
            Log.d(TAG, "setBody: " + customBody.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setParameters(Object urlParameters) {
        JacksonMapper jacksonMapper = JacksonMapper.getInstance();
        try {
            Map<String, String> map = jacksonMapper.toMap(DataParser.toJson(urlParameters));
            StringBuilder params = new StringBuilder();
            boolean firstItem = true;
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (firstItem) {
                    firstItem = false;
                    params.append("?");
                } else {
                    params.append("&");
                }
                Log.d(TAG, "setParameters: " + params.toString());
                params.append(key);
                params.append("=");
                params.append(URLEncoder.encode(value, "UTF-8"));
            }


            this.parameters = params.toString();
            Log.d(TAG, "setParameters: " + parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return customBody.toString().getBytes();
    }


    @Override
    public Map<String, String> getHeaders() {
        return App.getHeaders();
    }

    public void start() {
        this.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.addToQueue(this);
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        int maxLogSize = 1000;
        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
            String errorMessage = new String(volleyError.networkResponse.data);
            for (int i = 0; i <= errorMessage.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = Math.min(end, errorMessage.length());
                Log.d(TAG, errorMessage.substring(start, end));
            }
        }
        return super.parseNetworkError(volleyError);

    }


    public static String convertImageToBase64(File file) {
        String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(".") + 1);
        Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(file));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        String converted = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return converted + ":" + extension;
    }


    public static Integer getNextPage(String nextPageUrl) {
        if (nextPageUrl == null) {
            return -1;
        } else {
            return Integer.parseInt(nextPageUrl.substring(nextPageUrl.indexOf("page=") + 5));
        }
    }
}