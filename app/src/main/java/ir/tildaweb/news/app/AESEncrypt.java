package ir.tildaweb.news.app;

import android.util.Base64;

import java.security.Key;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncrypt {


    // ------------------------------------------------------------HOW TO USE
//    String message = "amjad";
//                try {
//        String iv = AESEncrypt.generateRandomIvBase64();
//        AESEncrypt amjiAES = new AESEncrypt(App.getMainKey(this), 128, iv);
//        String en = amjiAES.encrypt(message);
//        Log.d(TAG, "onClick: En " + en);
//        Log.d(TAG, "onClick: En " + amjiAES.decrypt("s4bqOhtZ5JW3fS9XFJ6VnQ=="));
//
//    } catch (Exception e) {
//        e.printStackTrace();
//    }


//    <?php
//    class amji{
//
//        private $iv = "1234567891234567";
//        private $key = "nDEhE62_&@YMZ_^uQz*vkmzjXxk4pg#gMz!em4mTAwzQrxP5PW6DemparvizwY&yh7_Q8uQz*vkKqND9N2ZNj7Sfp&3VJTAwzQrxU#H7fNcq9EA&8P%V%*cqaQ3auQz*vkvSr7ERQrZKQA@3SyQMLUQqND9N2Z";
//
//        public function __construct() {
//
//        }
//
//        public function en($str) {
//
//            return base64_encode(openssl_encrypt($str, 'AES-128-CBC', $this->key, OPENSSL_RAW_DATA, $this->iv)) . ":" . base64_encode($this->iv);
//
//        }
//
//        public function de($str) {
//            $parts = explode(":" , $str);
//            return openssl_decrypt(base64_decode($parts[0]), 'AES-128-CBC', $this->key, OPENSSL_RAW_DATA, base64_decode($parts[1]));
//        }
//
//    }


//    <?php
//    require "amji.php";
//    $amji = new amji();
//    echo $en = $amji->en("amjad");
//    echo "<br>";
//    echo $amji->de($en);

    // ------------------------------------------------------------END HOW TO USE

    // use it

//    String str = EasyAES.decryptString(response);


    private static final IvParameterSpec DEFAULT_IV = new IvParameterSpec(new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private Key key;
    private IvParameterSpec iv;
    private Cipher cipher;

    public AESEncrypt(final String key, final int bit) {
        this(key, bit, null);
    }

    public AESEncrypt(final String key, final int bit, final String iv) {
        String ivDe = new String(Base64.decode(iv, Base64.NO_WRAP));
        this.key = new SecretKeySpec(key.getBytes(), ALGORITHM);
        this.iv = new IvParameterSpec(ivDe.getBytes());
        init();
    }

    private void init() {
        try {
            cipher = Cipher.getInstance(TRANSFORMATION);
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public String encrypt(final String str) {
        try {
            return encrypt(str.getBytes());
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public String encrypt(final byte[] data) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            final byte[] encryptData = cipher.doFinal(data);
            return new String(Base64.encode(encryptData, Base64.NO_WRAP));
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }


    public String decrypt(final String part1) {
        try {
            return decrypt(Base64.decode(part1, Base64.NO_WRAP));
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }


    public String decrypt(final byte[] data) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            final byte[] decryptData = cipher.doFinal(data);
            return new String(decryptData);
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static String generateRandomIv() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        char tempChar;
        for (int i = 0; i < 16; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }


    public static String toBase64(String str) {
        return new String(Base64.encode(str.getBytes(), Base64.NO_WRAP));
    }


}