package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;

import ir.tildaweb.news.R;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.databinding.DialogProfileAdminBinding;


public class DialogProfileAdmin implements View.OnClickListener {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogProfileAdminBinding binding;
    private Context context;
    private AppPreferencesHelper appPreferencesHelper;
    private OnDialogProfileAdminClickListener onDialogProfileAdminClickListener;

    public interface OnDialogProfileAdminClickListener {

        void onClickNews();

        void onClickAdmins();

        void onClickCategories();

        void onClickContactUsList();

        void onUserNewsManagementClick();

        void onAppCommentsListClick();

        void onSettingsClick();

    }

    public void setonDialogProfileAdminClickListener(OnDialogProfileAdminClickListener onDialogProfileAdminClickListener) {
        this.onDialogProfileAdminClickListener = onDialogProfileAdminClickListener;
    }

    public DialogProfileAdmin(Context context) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogProfileAdminBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());
        setCancelable(true);
        binding.imageViewClose.setOnClickListener(view -> dismiss());
        appPreferencesHelper = new AppPreferencesHelper(context);

        if (appPreferencesHelper.getUserAdminTypePref() != null && appPreferencesHelper.getUserAdminTypePref().equals("owner")) {
            binding.linearAdmins.setVisibility(View.VISIBLE);
        } else {
            binding.linearAdmins.setVisibility(View.GONE);
        }

        binding.linearAdmins.setOnClickListener(this);
        binding.linearCategories.setOnClickListener(this);
        binding.linearNews.setOnClickListener(this);
        binding.linearContactUsList.setOnClickListener(this);
        binding.linearUserNews.setOnClickListener(this);
        binding.linearAppComment.setOnClickListener(this);
        binding.linearSettings.setOnClickListener(this);
    }


    public void setCancelable(boolean cancelable) {
        alertDialog.setCancelable(cancelable);
        alertDialog.setCanceledOnTouchOutside(cancelable);
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearAdmins: {
                onDialogProfileAdminClickListener.onClickAdmins();
                break;
            }
            case R.id.linearCategories: {
                onDialogProfileAdminClickListener.onClickCategories();
                break;
            }
            case R.id.linearNews: {
                onDialogProfileAdminClickListener.onClickNews();
                break;
            }

            case R.id.linearContactUsList: {
                onDialogProfileAdminClickListener.onClickContactUsList();
                break;
            }

            case R.id.linearUserNews: {
                onDialogProfileAdminClickListener.onUserNewsManagementClick();
                break;
            }
            case R.id.linearAppComment: {
                onDialogProfileAdminClickListener.onAppCommentsListClick();
                break;
            }
            case R.id.linearSettings: {
                onDialogProfileAdminClickListener.onSettingsClick();
                break;
            }
        }
    }


}
