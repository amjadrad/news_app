package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.robertlevonyan.views.chip.Chip;

import ir.tildaweb.news.R;
import ir.tildaweb.news.app.CacheManager;
import ir.tildaweb.news.app.DataParser;
import ir.tildaweb.news.data.cache.SearchHistoryModel;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.databinding.DialogSearchBinding;
import ir.tildaweb.news.utils.FontUtils;
import ir.tildaweb.news.utils.MathHelper;

public class DialogSearch {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogSearchBinding binding;
    private Context context;
    private CacheManager cacheManager;
    private AppPreferencesHelper appPreferencesHelper;

    public DialogSearch(Context context) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogSearchBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        binding.btnConfirm.setOnClickListener(view -> dismiss());
        cacheManager = new CacheManager(context);
        appPreferencesHelper = new AppPreferencesHelper(context);
        binding.linearDelete.setOnClickListener(view -> {
            cacheManager.setUserSearchHistory(null);
            binding.chipGroup.removeAllViews();
        });
        SearchHistoryModel searchHistoryModel = DataParser.fromJson(cacheManager.getUserSearchHistory(), SearchHistoryModel.class);
        if (searchHistoryModel != null && searchHistoryModel.getSearchs() != null) {
            for (String str : searchHistoryModel.getSearchs()) {
                AppCompatTextView textView = new AppCompatTextView(context);
                textView.setText(str);
                int dp8 = MathHelper.convertDipToPixels(context, 8);
                int dp4 = MathHelper.convertDipToPixels(context, 4);
                textView.setPadding(dp8, dp4, dp8, dp4);
                textView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_info_pill_default));
                textView.setTextColor(context.getResources().getColor(R.color.colorText));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12 * appPreferencesHelper.getTextSizePref());
                textView.setTypeface(FontUtils.getFont(context), Typeface.BOLD);
                textView.setOnClickListener(view -> {
                    String text = ((AppCompatTextView) view).getText().toString();
                    binding.etSearch.setText(text);
                    binding.etSearch.setSelection(text.length());
                });
                binding.chipGroup.addView(textView);
            }
        }


    }


    public AppCompatEditText getEtSearch() {
        return this.binding.etSearch;
    }

    public void setOnConfirmClickListener(View.OnClickListener onConfirmClickListener) {
        this.binding.btnConfirm.setOnClickListener(onConfirmClickListener);
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }

}