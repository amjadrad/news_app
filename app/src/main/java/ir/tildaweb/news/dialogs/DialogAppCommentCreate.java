package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import ir.tildaweb.news.databinding.DialogAppCommentCreateBinding;
import ir.tildaweb.news.databinding.DialogCommentCreateBinding;


public class DialogAppCommentCreate {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogAppCommentCreateBinding binding;
    private Context context;

    public DialogAppCommentCreate(Context context) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogAppCommentCreateBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());

        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }


    public DialogAppCommentCreate setCancelable(boolean cancelable) {
        alertDialog.setCancelable(cancelable);
        alertDialog.setCanceledOnTouchOutside(cancelable);
        return this;
    }

    public void setOnConfirmClickListener(View.OnClickListener onConfirmClickListener) {
        this.binding.btnConfirm.setOnClickListener(onConfirmClickListener);
    }

    public void setOnCancelClickListener(View.OnClickListener onCancelClickListener) {
        this.binding.btnCancel.setOnClickListener(onCancelClickListener);
    }

    public String getDescription() {
        return this.binding.etDescription.getText().toString().trim();
    }

    public EditText getEtDescription() {
        return this.binding.etDescription;
    }

    public float getRating() {
        return this.binding.ratingBar.getRating();
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }


}
