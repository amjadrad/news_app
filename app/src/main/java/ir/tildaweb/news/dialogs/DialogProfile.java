package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;

import ir.tildaweb.news.R;
import ir.tildaweb.news.data.pref.AppPreferencesHelper;
import ir.tildaweb.news.databinding.DialogProfileBinding;
import ir.tildaweb.news.utils.MathHelper;


public class DialogProfile implements View.OnClickListener {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogProfileBinding binding;
    private Context context;
    private AppPreferencesHelper appPreferencesHelper;
    private OnDialogProfileClickListener onDialogProfileClickListener;

    public interface OnDialogProfileClickListener {
        void onLogout();


        void onClickAdvertise();

        void onClickContactUs();


        void onClickEditProfile();

        void onClickSettings();

        void onClickBookmarks();

        void onUserNewsClick();

        void onSendToFriendsClick();

        void onAppCommentClick();

    }

    public void setOnDialogProfileClickListener(OnDialogProfileClickListener onDialogProfileClickListener) {
        this.onDialogProfileClickListener = onDialogProfileClickListener;
    }

    public DialogProfile(Context context) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogProfileBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());
        setCancelable(true);
        binding.imageViewClose.setOnClickListener(view -> dismiss());

        appPreferencesHelper = new AppPreferencesHelper(context);

        if (appPreferencesHelper.getUserFullNamePref() != null) {
            binding.tvFullname.setText(String.format("%s", appPreferencesHelper.getUserFullNamePref()));
        }
        if (appPreferencesHelper.getUserPhonePref() != null) {
            binding.tvPhone.setText(String.format("%s", appPreferencesHelper.getUserPhonePref()));
        }


        binding.linearAdvertise.setOnClickListener(this);
        binding.linearLogout.setOnClickListener(this);
        binding.linearContactUs.setOnClickListener(this);
        binding.tvEditProfile.setOnClickListener(this);
        binding.imageViewSettings.setOnClickListener(this);
        binding.linearBookmarks.setOnClickListener(this);
        binding.linearUserNews.setOnClickListener(this);
        binding.linearSendToFriends.setOnClickListener(this);
        binding.linearAppComment.setOnClickListener(this);
        if (appPreferencesHelper.getShowUsersCount()) {
            binding.tvUserCount.setText(String.format("%s", MathHelper.getView(appPreferencesHelper.getUsersCount())));
            binding.linearUsersCount.setVisibility(View.VISIBLE);
        } else {
            binding.linearUsersCount.setVisibility(View.GONE);
        }
    }


    public void setCancelable(boolean cancelable) {
        alertDialog.setCancelable(cancelable);
        alertDialog.setCanceledOnTouchOutside(cancelable);
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearAdvertise: {
                onDialogProfileClickListener.onClickAdvertise();
                break;
            }
            case R.id.linearContactUs: {
                onDialogProfileClickListener.onClickContactUs();
                break;
            }
            case R.id.tvEditProfile: {
                onDialogProfileClickListener.onClickEditProfile();
                break;
            }
            case R.id.linearLogout: {
                onDialogProfileClickListener.onLogout();
                break;
            }
            case R.id.imageViewSettings: {
                onDialogProfileClickListener.onClickSettings();
                break;
            }
            case R.id.linearBookmarks: {
                onDialogProfileClickListener.onClickBookmarks();
                break;
            }
            case R.id.linearUserNews: {
                onDialogProfileClickListener.onUserNewsClick();
                break;
            }
            case R.id.linearSendToFriends: {
                onDialogProfileClickListener.onSendToFriendsClick();
                break;
            }
            case R.id.linearAppComment: {
                onDialogProfileClickListener.onAppCommentClick();
                break;
            }
        }
    }


}
