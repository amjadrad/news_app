package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.DialogConfirmMessageBinding;
import ir.tildaweb.news.databinding.DialogShareAppBinding;


public class DialogShareApp {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogShareAppBinding binding;
    private Context context;

    public DialogShareApp(Context context) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogShareAppBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());

    }

    public void setOnShareApkClickListener(View.OnClickListener onConfirmClickListener) {
        this.binding.linearShareApk.setOnClickListener(onConfirmClickListener);
    }

    public void setOnShareLinkClickListener(View.OnClickListener onCancelClickListener) {
        this.binding.linearShareLink.setOnClickListener(onCancelClickListener);
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }
}