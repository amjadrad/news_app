package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.DialogConfirmMessageBinding;


public class DialogConfirmMessage {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogConfirmMessageBinding binding;
    private Context context;

    public DialogConfirmMessage(Context context, String title, String description) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = binding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());
        binding.tvTitle.setText(String.format("%s", title));
        binding.tvDescription.setText(String.format("%s", description));
        binding.btnCancel.setOnClickListener(view -> dismiss());
    }

    public void setDanger() {
        binding.btnConfirm.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_danger_pill_default));
    }

    public DialogConfirmMessage setCancelable(boolean cancelable) {
        alertDialog.setCancelable(cancelable);
        alertDialog.setCanceledOnTouchOutside(cancelable);
        return this;
    }

    public void setOnConfirmClickListener(View.OnClickListener onConfirmClickListener) {
        this.binding.btnConfirm.setOnClickListener(onConfirmClickListener);
    }

    public void setOnCancelClickListener(View.OnClickListener onCancelClickListener) {
        this.binding.btnCancel.setOnClickListener(onCancelClickListener);
    }

    public void setConfirmButtonText(String text) {
        this.binding.btnConfirm.setText(String.format("%s", text));
    }

    public void setCancelButtonText(String text) {
        this.binding.btnCancel.setText(String.format("%s", text));
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }


}
