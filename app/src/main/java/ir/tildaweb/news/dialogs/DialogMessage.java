package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;

import ir.tildaweb.news.databinding.DialogMessageBinding;

public class DialogMessage {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogMessageBinding binding;
    private Context context;

    public DialogMessage(Context context, String title, String description) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogMessageBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        binding.tvTitle.setText(String.format("%s", title));
        binding.tvDescription.setText(String.format("%s", description));
        binding.btnConfirm.setOnClickListener(view -> dismiss());
    }

    public void setOnConfirmClickListener(View.OnClickListener onConfirmClickListener) {
        this.binding.btnConfirm.setOnClickListener(onConfirmClickListener);
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }

}