package ir.tildaweb.news.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import ir.tildaweb.news.R;
import ir.tildaweb.news.databinding.DialogCommentCreateBinding;
import ir.tildaweb.news.databinding.DialogConfirmMessageBinding;


public class DialogNewsCommentCreate {

    private String TAG = this.getClass().getName();
    private AlertDialog alertDialog;
    private DialogCommentCreateBinding binding;
    private Context context;

    public DialogNewsCommentCreate(Context context) {
        this.context = context;
        this.alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        binding = DialogCommentCreateBinding.inflate(LayoutInflater.from(context));
        alertDialog.setView(binding.getRoot());

        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }


    public DialogNewsCommentCreate setCancelable(boolean cancelable) {
        alertDialog.setCancelable(cancelable);
        alertDialog.setCanceledOnTouchOutside(cancelable);
        return this;
    }

    public void setOnConfirmClickListener(View.OnClickListener onConfirmClickListener) {
        this.binding.btnConfirm.setOnClickListener(onConfirmClickListener);
    }

    public void setOnCancelClickListener(View.OnClickListener onCancelClickListener) {
        this.binding.btnCancel.setOnClickListener(onCancelClickListener);
    }

    public String getDescription() {
        return this.binding.etDescription.getText().toString().trim();
    }

    public EditText getEtDescription() {
        return this.binding.etDescription;
    }

    public void show() {
        this.alertDialog.show();
    }

    public void dismiss() {
        this.alertDialog.dismiss();
    }


}
