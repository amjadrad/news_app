package ir.tildaweb.news.data.network;


import ir.tildaweb.news.BuildConfig;

public class Endpoints {

    private static String BASE_URL = BuildConfig.BASE_URL;

    public static String BASE_URL_PROVINCES = BASE_URL + "provinces";
    public static String BASE_URL_CITIES = BASE_URL + "cities";
    public static String BASE_URL_WEATHER = "https://api.openweathermap.org/data/2.5/forecast?appid=a0c431932b8dc1be0f223cdab53969e6&lang=fa&";
    public static String BASE_URL_UPLOAD_FILE = BASE_URL + "upload_file";
    public static String BASE_URL_SIGN_IN = BASE_URL + "signin";
    public static String BASE_URL_USER_EDIT = BASE_URL + "user/edit";
    public static String BASE_URL_USER_UPDATE = BASE_URL + "user/update";
    public static String BASE_URL_USER_COUNT = BASE_URL + "user/count";

    public static String BASE_URL_APP_COMMENT_STORE = BASE_URL + "app/comment/store";
    public static String BASE_URL_APP_COMMENT_DELETE = BASE_URL + "admin/app/comment/delete";
    public static String BASE_URL_APP_COMMENT_INDEX = BASE_URL + "admin/app/comment/index";
    public static String BASE_URL_APP_COMMENT_AVERAGE_RATING = BASE_URL + "admin/app/comment/average_rating";

    public static String BASE_URL_CONTACT_US = BASE_URL + "contacts/store";
    public static String BASE_URL_ADMIN_CONTACT_US_INDEX = BASE_URL + "admin/contacts/index";
    public static String BASE_URL_ADMIN_CONTACT_US_SEEN = BASE_URL + "admin/contacts/seen";
    public static String BASE_URL_ADMIN_CONTACT_US_DELETE = BASE_URL + "admin/contacts/delete";
    public static String BASE_URL_VERIFY = BASE_URL + "verify";
    public static String BASE_URL_CATEGORIES = BASE_URL + "categories/index";
    public static String BASE_URL_NEWS_SEARCH = BASE_URL + "news/search";
    public static String BASE_URL_NEWS_HOT = BASE_URL + "news/hot";
    public static String BASE_URL_NEWS_MOST_VISIT = BASE_URL + "news/most_visit";
    public static String BASE_URL_NEWS_POSTER_MAIN = BASE_URL + "news/posters/main";
    public static String BASE_URL_NEWS_LIKE = BASE_URL + "news/like";
    public static String BASE_URL_NEWS_VISIT = BASE_URL + "news/visit";
    public static String BASE_URL_NEWS_SHOW = BASE_URL + "news/show";
    //Comments
    public static String BASE_URL_NEWS_COMMENTS_INDEX = BASE_URL + "news/comments/index";
    public static String BASE_URL_NEWS_COMMENTS_STORE = BASE_URL + "news/comments/store";
    public static String BASE_URL_NEWS_COMMENTS_REPLY = BASE_URL + "news/comments/reply";
    //User news favorites
    public static String BASE_URL_USER_NEWS_FAVORITES_INDEX = BASE_URL + "user_news_favorites/index";
    public static String BASE_URL_USER_NEWS_FAVORITES_STORE = BASE_URL + "user_news_favorites/store";
    //Admins
    public static String BASE_URL_ADMIN_ADMINS_INDEX = BASE_URL + "admin/admins/index";
    public static String BASE_URL_ADMIN_ADMINS_DELETE = BASE_URL + "admin/admins/delete";
    public static String BASE_URL_ADMIN_ADMINS_UPDATE = BASE_URL + "admin/admins/update";
    public static String BASE_URL_ADMIN_ADMINS_STORE = BASE_URL + "admin/admins/store";
    public static String BASE_URL_ADMIN_ADMINS_SHOW = BASE_URL + "admin/admins/show";
    //Admin access
    public static String BASE_URL_ADMIN_ADMIN_ACCESS_EDIT = BASE_URL + "admin/admins/access/edit";
    public static String BASE_URL_ADMIN_ADMIN_ACCESS_UPDATE = BASE_URL + "admin/admins/access/update";
    //Admin Settings
    public static String BASE_URL_ADMIN_SETTINGS_EDIT = BASE_URL + "admin/settings/edit";
    public static String BASE_URL_ADMIN_SETTINGS_UPDATE = BASE_URL + "admin/settings/update";

    //News
    public static String BASE_URL_ADMIN_NEWS_SEARCH = BASE_URL + "admin/news/search";
    public static String BASE_URL_ADMIN_NEWS_DELETE = BASE_URL + "admin/news/delete";

    public static String BASE_URL_ADMIN_NEWS_SHOW = BASE_URL + "admin/news/show";
    public static String BASE_URL_ADMIN_NEWS_EDIT = BASE_URL + "admin/news/edit";
    public static String BASE_URL_ADMIN_NEWS_UPDATE = BASE_URL + "admin/news/update";
    public static String BASE_URL_ADMIN_NEWS_STORE = BASE_URL + "admin/news/store";
    public static String BASE_URL_ADMIN_NEWS_MEDIA_DELETE = BASE_URL + "admin/news/media/delete";
    public static String BASE_URL_ADMIN_NEWS_COMMENTS = BASE_URL + "admin/news/comments/index";
    public static String BASE_URL_ADMIN_NEWS_COMMENTS_ACTIVE = BASE_URL + "admin/news/comments/active";
    public static String BASE_URL_ADMIN_NEWS_COMMENTS_SEEN = BASE_URL + "admin/news/comments/seen";
    //Categories
    public static String BASE_URL_ADMIN_CATEGORIES_DELETE = BASE_URL + "admin/categories/delete";
    public static String BASE_URL_ADMIN_CATEGORIES_SHOW = BASE_URL + "admin/categories/show";
    public static String BASE_URL_ADMIN_CATEGORIES_UPDATE = BASE_URL + "admin/categories/update";
    public static String BASE_URL_ADMIN_CATEGORIES_STORE = BASE_URL + "admin/categories/store";
    //UserNews
    public static String BASE_URL_USER_NEWS_INDEX = BASE_URL + "user/news/index";
    public static String BASE_URL_USER_NEWS_SHOW = BASE_URL + "user/news/show";
    public static String BASE_URL_USER_NEWS_DELETE = BASE_URL + "user/news/delete";
    public static String BASE_URL_USER_NEWS_EDIT = BASE_URL + "user/news/edit";
    public static String BASE_URL_USER_NEWS_UPDATE = BASE_URL + "user/news/update";
    public static String BASE_URL_USER_NEWS_STORE = BASE_URL + "user/news/store";
    public static String BASE_URL_USER_NEWS_MEDIA_DELETE = BASE_URL + "user/news/media/delete";
    //AdminUserNews
    public static String BASE_URL_ADMIN_USER_NEWS_INDEX = BASE_URL + "admin/user/news/index";
    public static String BASE_URL_ADMIN_USER_NEWS_SHOW = BASE_URL + "admin/user/news/show";
    public static String BASE_URL_ADMIN_USER_NEWS_DELETE = BASE_URL + "admin/user/news/delete";
    public static String BASE_URL_ADMIN_USER_NEWS_REJECT = BASE_URL + "admin/user/news/reject";
    public static String BASE_URL_ADMIN_USER_NEWS_EDIT = BASE_URL + "admin/user/news/edit";
    public static String BASE_URL_ADMIN_USER_NEWS_UPDATE = BASE_URL + "admin/user/news/update";
    public static String BASE_URL_ADMIN_USER_NEWS_STORE = BASE_URL + "admin/user/news/store";
    public static String BASE_URL_ADMIN_USER_NEWS_MEDIA_DELETE = BASE_URL + "admin/user/news/media/delete";

}
