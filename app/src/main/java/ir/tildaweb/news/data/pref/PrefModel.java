package ir.tildaweb.news.data.pref;

public class PrefModel {


    public static String PREF_LOGIN = "PREF_LOGIN";
    public static String PREF_USER_IS_ADMIN = "PREF_USER_IS_ADMIN";
    public static String PREF_USER_ADMIN_TYPE = "PREF_USER_ADMIN_TYPE";
    public static String PREF_ADMIN_ID = "PREF_ADMIN_ID";
    public static String PREF_USER_ID = "PREF_USER_ID";
    public static String PREF_USER_FULL_NAME = "PREF_USER_FULL_NAME";
    public static String PREF_USER_PHONE = "PREF_USER_PHONE";
    public static String PREF_USER_API_TOKEN = "PREF_USER_API_TOKEN";
    public static String PREF_USER_CITY_ID = "PREF_USER_CITY_ID";
    public static String PREF_USER_CITY_TITLE = "PREF_USER_CITY_TITLE";
    public static String PREF_USER_CENTER_CITY_TITLE = "PREF_USER_CENTER_CITY_TITLE";
    public static String PREF_THEME = "PREF_THEME";
    public static String PREF_FONT = "PREF_FONT";
    public static String PREF_TEXT_SIZE = "PREF_TEXT_SIZE";
    public static String PREF_IS_SHOW_NEWS_GALLERY = "PREF_IS_SHOW_NEWS_GALLERY";
    public static String PREF_USERS_COUNT = "PREF_USERS_COUNT";
    public static String PREF_SHOW_USERS_COUNT = "PREF_SHOW_USERS_COUNT";

    //Admin Access
    public static String PREF_ADMIN_ACCESS_NEWS_INSERT = "PREF_ADMIN_ACCESS_NEWS_INSERT";
    public static String PREF_ADMIN_ACCESS_NEWS_UPDATE = "PREF_ADMIN_ACCESS_NEWS_UPDATE";
    public static String PREF_ADMIN_ACCESS_NEWS_DELETE = "PREF_ADMIN_ACCESS_NEWS_DELETE";
    public static String PREF_ADMIN_ACCESS_NEWS_COMMENTS = "PREF_ADMIN_ACCESS_NEWS_COMMENTS";
    public static String PREF_ADMIN_ACCESS_CATEGORIES_INSERT = "PREF_ADMIN_ACCESS_CATEGORIES_INSERT";
    public static String PREF_ADMIN_ACCESS_CATEGORIES_UPDATE = "PREF_ADMIN_ACCESS_CATEGORIES_UPDATE";
    public static String PREF_ADMIN_ACCESS_CATEGORIES_DELETE = "PREF_ADMIN_ACCESS_CATEGORIES_DELETE";
    public static String PREF_ADMIN_ACCESS_USER_NEWS_UPDATE = "PREF_ADMIN_ACCESS_USER_NEWS_UPDATE";
    public static String PREF_ADMIN_ACCESS_USER_NEWS_DELETE = "PREF_ADMIN_ACCESS_USER_NEWS_DELETE";
    public static String PREF_ADMIN_ACCESS_USER_CONTACTS = "PREF_ADMIN_ACCESS_USER_CONTACTS";
    public static String PREF_ADMIN_ACCESS_APP_COMMENTS = "PREF_ADMIN_ACCESS_APP_COMMENTS";


}
