package ir.tildaweb.news.data.pref;


import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferencesHelper implements PreferencesPresenter {


    private final String prefFileName = "main_pref";
    private final SharedPreferences mPrefs;


    /**
     * injects contex,prefFileName and also initialize the mPref object with given encryption and decryption password and
     * also the name of shared preference file name
     *
     * @param context
     * @author amjadrad
     */

    public AppPreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }


    @Override
    public void setLoginPref(boolean loginPref) {
        mPrefs.edit().putBoolean(PrefModel.PREF_LOGIN, loginPref).apply();
    }

    @Override
    public boolean getLoginPref() {
        return mPrefs.getBoolean(PrefModel.PREF_LOGIN, false);
    }


    @Override
    public void setUserId(int userId) {
        mPrefs.edit().putInt(PrefModel.PREF_USER_ID, userId).apply();
    }

    @Override
    public int getUserId() {
        return mPrefs.getInt(PrefModel.PREF_USER_ID, -1);
    }

    @Override
    public void setAdminId(int adminId) {
        mPrefs.edit().putInt(PrefModel.PREF_ADMIN_ID, adminId).apply();
    }

    @Override
    public int getAdminId() {
        return mPrefs.getInt(PrefModel.PREF_ADMIN_ID, -1);
    }


    @Override
    public void setUserFullNamePref(String userFullNamePref) {
        mPrefs.edit().putString(PrefModel.PREF_USER_FULL_NAME, userFullNamePref).apply();
    }

    @Override
    public String getUserFullNamePref() {
        return mPrefs.getString(PrefModel.PREF_USER_FULL_NAME, null);
    }

    @Override
    public void setUserApiTokenPref(String userApiTokenPref) {
        mPrefs.edit().putString(PrefModel.PREF_USER_API_TOKEN, userApiTokenPref).apply();
    }

    @Override
    public String getUserApiTokenPref() {
        return mPrefs.getString(PrefModel.PREF_USER_API_TOKEN, null);
    }

    @Override
    public void setUserPhonePref(String userPhonePref) {
        mPrefs.edit().putString(PrefModel.PREF_USER_PHONE, userPhonePref).apply();
    }

    @Override
    public String getUserPhonePref() {
        return mPrefs.getString(PrefModel.PREF_USER_PHONE, null);
    }

    @Override
    public void setUserIsAdminPref(boolean userIsAdminPref) {
        mPrefs.edit().putBoolean(PrefModel.PREF_USER_IS_ADMIN, userIsAdminPref).apply();
    }

    @Override
    public boolean getUserIsAdminPref() {
        return mPrefs.getBoolean(PrefModel.PREF_USER_IS_ADMIN, false);
    }

    @Override
    public void setUserAdminTypePref(String userAdminTypePref) {
        mPrefs.edit().putString(PrefModel.PREF_USER_ADMIN_TYPE, userAdminTypePref).apply();
    }

    @Override
    public String getUserAdminTypePref() {
        return mPrefs.getString(PrefModel.PREF_USER_ADMIN_TYPE, null);
    }

    @Override
    public void setThemePref(int themePref) {
        mPrefs.edit().putInt(PrefModel.PREF_THEME, themePref).apply();
    }

    @Override
    public int getThemePref() {
        return mPrefs.getInt(PrefModel.PREF_THEME, 0);
    }

    @Override
    public void setFontPref(int fontPref) {
        mPrefs.edit().putInt(PrefModel.PREF_FONT, fontPref).apply();
    }

    @Override
    public int getFontPref() {
        return mPrefs.getInt(PrefModel.PREF_FONT, 1);
    }

    @Override
    public void setIsShowNewsGalleryPref(boolean isShowNewsGalleryPref) {
        mPrefs.edit().putBoolean(PrefModel.PREF_IS_SHOW_NEWS_GALLERY, isShowNewsGalleryPref).apply();
    }

    @Override
    public boolean getIsShowNewsGalleryPref() {
        return mPrefs.getBoolean(PrefModel.PREF_IS_SHOW_NEWS_GALLERY, true);
    }

    @Override
    public void setUserCityIdPref(int userCityIdPref) {
        mPrefs.edit().putInt(PrefModel.PREF_USER_CITY_ID, userCityIdPref).apply();
    }

    @Override
    public int getUserCityIdPref() {
        return mPrefs.getInt(PrefModel.PREF_USER_CITY_ID, -1);
    }

    @Override
    public void setUserCityTitlePref(String userCityTitlePref) {
        mPrefs.edit().putString(PrefModel.PREF_USER_CITY_TITLE, userCityTitlePref).apply();
    }

    @Override
    public String getUserCityTitlePref() {
        return mPrefs.getString(PrefModel.PREF_USER_CITY_TITLE, "تهران");
    }

    @Override
    public void setUserCenterCityTitlePref(String userCenterCityTitlePref) {
        mPrefs.edit().putString(PrefModel.PREF_USER_CENTER_CITY_TITLE, userCenterCityTitlePref).apply();
    }

    @Override
    public String getUserCenterCityTitlePref() {
        return mPrefs.getString(PrefModel.PREF_USER_CENTER_CITY_TITLE, "تهران");
    }

    @Override
    public void setTextSizePref(float textSizePref) {
        mPrefs.edit().putFloat(PrefModel.PREF_TEXT_SIZE, textSizePref).apply();

    }

    @Override
    public float getTextSizePref() {
        return mPrefs.getFloat(PrefModel.PREF_TEXT_SIZE, 1);
    }

    @Override
    public void setUsersCount(int usersCount) {
        mPrefs.edit().putInt(PrefModel.PREF_USERS_COUNT, usersCount).apply();
    }

    @Override
    public int getUsersCount() {
        return mPrefs.getInt(PrefModel.PREF_USERS_COUNT, 0);
    }

    @Override
    public void setShowUsersCount(boolean showUsersCount) {
        mPrefs.edit().putBoolean(PrefModel.PREF_SHOW_USERS_COUNT, showUsersCount).apply();
    }

    @Override
    public boolean getShowUsersCount() {
        return mPrefs.getBoolean(PrefModel.PREF_SHOW_USERS_COUNT, true);
    }

    @Override
    public void setAdminAccessNewsInsert(boolean adminAccessNewsInsert) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_INSERT, adminAccessNewsInsert).apply();
    }

    @Override
    public boolean getAdminAccessNewsInsert() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_INSERT, false);
    }

    @Override
    public void setAdminAccessNewsUpdate(boolean adminAccessNewsUpdate) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_UPDATE, adminAccessNewsUpdate).apply();
    }

    @Override
    public boolean getAdminAccessNewsUpdate() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_UPDATE, false);
    }

    @Override
    public void setAdminAccessNewsDelete(boolean adminAccessNewsDelete) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_DELETE, adminAccessNewsDelete).apply();
    }

    @Override
    public boolean getAdminAccessNewsDelete() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_DELETE, false);
    }

    @Override
    public void setAdminAccessNewsComments(boolean adminAccessNewsComments) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_COMMENTS, adminAccessNewsComments).apply();
    }

    @Override
    public boolean getAdminAccessNewsComments() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_NEWS_COMMENTS, false);
    }

    @Override
    public void setAdminAccessCategoriesInsert(boolean adminAccessCategoriesInsert) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_CATEGORIES_INSERT, adminAccessCategoriesInsert).apply();
    }

    @Override
    public boolean getAdminAccessCategoriesInsert() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_CATEGORIES_INSERT, false);
    }

    @Override
    public void setAdminAccessCategoriesUpdate(boolean adminAccessCategoriesUpdate) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_CATEGORIES_UPDATE, adminAccessCategoriesUpdate).apply();
    }

    @Override
    public boolean getAdminAccessCategoriesUpdate() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_CATEGORIES_UPDATE, false);
    }

    @Override
    public void setAdminAccessCategoriesDelete(boolean adminAccessCategoriesDelete) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_CATEGORIES_DELETE, adminAccessCategoriesDelete).apply();
    }

    @Override
    public boolean getAdminAccessCategoriesDelete() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_CATEGORIES_DELETE, false);
    }

    @Override
    public void setAdminAccessUserNewsDelete(boolean adminAccessUserNewsDelete) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_USER_NEWS_DELETE, adminAccessUserNewsDelete).apply();
    }

    @Override
    public boolean getAdminAccessUserNewsDelete() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_USER_NEWS_DELETE, false);
    }

    @Override
    public void setAdminAccessUserNewsUpdate(boolean adminAccessUserNewsUpdate) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_USER_NEWS_UPDATE, adminAccessUserNewsUpdate).apply();
    }

    @Override
    public boolean getAdminAccessUserNewsUpdate() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_USER_NEWS_UPDATE, false);
    }

    @Override
    public void setAdminAccessUserContacts(boolean adminAccessUserContacts) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_USER_CONTACTS, adminAccessUserContacts).apply();
    }

    @Override
    public boolean getAdminAccessUserContacts() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_USER_CONTACTS, false);
    }

    @Override
    public void setAdminAccessAppComments(boolean adminAccessAppComments) {
        mPrefs.edit().putBoolean(PrefModel.PREF_ADMIN_ACCESS_APP_COMMENTS, adminAccessAppComments).apply();
    }

    @Override
    public boolean getAdminAccessAppComments() {
        return mPrefs.getBoolean(PrefModel.PREF_ADMIN_ACCESS_APP_COMMENTS, false);
    }

}