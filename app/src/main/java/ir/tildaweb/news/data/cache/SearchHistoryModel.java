package ir.tildaweb.news.data.cache;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchHistoryModel {
    @SerializedName("searchs")
    private ArrayList<String> searchs;

    public ArrayList<String> getSearchs() {
        return searchs;
    }

    public void setSearchs(ArrayList<String> searchs) {
        this.searchs = searchs;
    }
}
