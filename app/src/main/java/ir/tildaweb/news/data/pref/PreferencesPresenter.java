package ir.tildaweb.news.data.pref;

public interface PreferencesPresenter {


    void setLoginPref(boolean loginPref);

    boolean getLoginPref();

    void setUserId(int userId);

    int getUserId();

    void setAdminId(int adminId);

    int getAdminId();

    void setUserFullNamePref(String userFullNamePref);

    String getUserFullNamePref();

    void setUserApiTokenPref(String userApiTokenPref);

    String getUserApiTokenPref();

    void setUserPhonePref(String userPhonePref);

    String getUserPhonePref();

    void setUserIsAdminPref(boolean userIsAdminPref);

    boolean getUserIsAdminPref();

    void setUserAdminTypePref(String userAdminTypePref);

    String getUserAdminTypePref();

    void setThemePref(int themePref);

    int getThemePref();

    void setFontPref(int fontPref);

    int getFontPref();

    void setIsShowNewsGalleryPref(boolean isShowNewsGalleryPref);

    boolean getIsShowNewsGalleryPref();

    void setUserCityIdPref(int userCityIdPref);

    int getUserCityIdPref();

    void setUserCityTitlePref(String userCityTitlePref);

    String getUserCityTitlePref();

    void setUserCenterCityTitlePref(String userCenterCityTitlePref);

    String getUserCenterCityTitlePref();

    void setTextSizePref(float textSizePref);

    float getTextSizePref();

    void setUsersCount(int usersCount);

    int getUsersCount();

    void setShowUsersCount(boolean showUsersCount);

    boolean getShowUsersCount();

    //    ---------------------------------
//    ---------------------------------
//    ---------------------------------
//    ---------------------------------
    void setAdminAccessNewsInsert(boolean adminAccessNewsInsert);

    boolean getAdminAccessNewsInsert();

    void setAdminAccessNewsUpdate(boolean adminAccessNewsUpdate);

    boolean getAdminAccessNewsUpdate();

    void setAdminAccessNewsDelete(boolean adminAccessNewsDelete);

    boolean getAdminAccessNewsDelete();

    void setAdminAccessNewsComments(boolean adminAccessNewsComments);

    boolean getAdminAccessNewsComments();

    void setAdminAccessCategoriesInsert(boolean adminAccessCategoriesInsert);

    boolean getAdminAccessCategoriesInsert();

    void setAdminAccessCategoriesUpdate(boolean adminAccessCategoriesUpdate);

    boolean getAdminAccessCategoriesUpdate();

    void setAdminAccessCategoriesDelete(boolean adminAccessCategoriesDelete);

    boolean getAdminAccessCategoriesDelete();

    void setAdminAccessUserNewsDelete(boolean adminAccessUserNewsDelete);

    boolean getAdminAccessUserNewsDelete();

    void setAdminAccessUserNewsUpdate(boolean adminAccessUserNewsUpdate);

    boolean getAdminAccessUserNewsUpdate();

    void setAdminAccessUserContacts(boolean adminAccessUserContacts);

    boolean getAdminAccessUserContacts();

    void setAdminAccessAppComments(boolean adminAccessAppComments);

    boolean getAdminAccessAppComments();
}

