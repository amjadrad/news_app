package ir.tildaweb.news.data.network;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

import ir.tildaweb.news.utils.FileUtils;


public class FileUploader {

    private final String TAG = this.getClass().getName();

    public void upload(String filePath, int requestCode, OnFileUploaderListener onFileUploaderListener) {

        FileUtils.FileUtilObject fileUtilObject = FileUtils.saveBitmapToFile(FileUtils.convertImageToBitmap(filePath), null, null, null);
        if (fileUtilObject == null) {
            onFileUploaderListener.onFileUploadError("file util null line:21");
            return;
        }
        AndroidNetworking.upload(Endpoints.BASE_URL_UPLOAD_FILE)
                .addMultipartFile("upload_file", fileUtilObject.getFile())
                .setTag("uploader")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener((bytesUploaded, totalBytes) -> {
                    int percent = (int) ((bytesUploaded / totalBytes) * 100);
                    onFileUploaderListener.onFileUploadProgress(requestCode, percent);
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        onFileUploaderListener.onFileUploaded(fileUtilObject.getFileName(), requestCode);
                    }

                    @Override
                    public void onError(ANError error) {
                        onFileUploaderListener.onFileUploadError(error.getLocalizedMessage());
                    }
                });
    }


    public interface OnFileUploaderListener {
        void onFileUploadProgress(int percent, int requestId);

        void onFileUploaded(String fileName, int requestId);

        void onFileUploadError(String error);
    }

}



